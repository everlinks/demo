<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User_model extends CI_Model {

    public function __construct()
    {
        parent::__construct();
    }


    public function get_user_record($username)
    {
        $query = $this->db->select('id, profile_picture, username')
                          ->where('username', $username)
                          ->limit(1)
                          ->get('users');
        if ($query->num_rows() === 1)
        {
            return $query->row();
        } else {
            return false;
        }
    }

    public function get_links($id)
    {
        $query = $this->db->select('id, title, url, position, active')
                 ->where(array('user_id' => $id, 'active' => 1, 'title !=' => '', 'url !=' => '', 'deleted !=' => 1))
                 ->order_by('position asc')
                 ->order_by('id desc')
                 ->get('links');
        $result = $query->result_array();

        $count = count($result);

        if(empty($count)){
            return false;
        }
        else{
            return $result;
        }
    }
}
