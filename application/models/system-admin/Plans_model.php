<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Plans_model extends CI_Model {

    public function __construct()
    {
        parent::__construct();
    }

	public function plans()
	{

        return $this->db->get('plans')->result();
        
	}

	public function get_plans($id = NULL)
	{
		if (isset($id))
		{
		return $this->db->select('id, plan_name, amount, intervals')
			->where('plans.id', $id)
			->limit(1)
			->order_by('id')
			->get('plans')
			->row();
		}
		return false;
	}


    public function create_plans($name, $amount,$intervals)
	{
		

        $data = array(
            'plan_name' => $this->input->post('plans_name'),
            'amount' => $this->input->post('amount'),
            'intervals' => $this->input->post('intervals')

           
         );
         
         
		 $this->db->insert('plans', $data);
	
        }
    
	public function update_plans($id, $data)
	{
		$this->db->update('plans', $data, array('id' => $id));
        //  $this->db->update('categories', $data);
        }
	
	
	}


	
	
