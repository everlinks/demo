<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Features_model extends CI_Model {

    public function __construct()
    {
        parent::__construct();
    }

	public function features()
	{

        return $this->db->get('features')->result();
        
    }

	// public function get_category($id = NULL)
	// {
	// 	if (isset($id))
	// 	{
	// 	return $this->db->select('id, name, description')
	// 		->where('categories.id', $id)
	// 		->limit(1)
	// 		->order_by('id', 'description')
	// 		->get('categories')
	// 		->row();
	// 	}
	// 	return false;
	// }


    public function create_features($title = '' , $shortdescription = '', $fulldescription = '',$answers = '' , $additional_data = array())
	{
		

        $data = array(
            'category' => $this->input->post('features_category'),
            'title' => $this->input->post('features_name'),
            'shortdescription' => $this->input->post('features_description'),
            // 'fulldescription' => $this->input->post('features_descript'),
            'answers' => $this->input->post('features_answers')
           
           
         );
         
         
		 $this->db->insert('features', $data);
	
        }
    
	public function update_support($id, $data)
	{
		$this->db->update('categories', $data, array('id' => $id));
        //  $this->db->update('categories', $data);
        }
	
		public function getAllGroups(){
			$this->db->from('categories');
			$query = $this->db->get();
			foreach($query->result() as $row ){
				//this sets the key to equal the value so that
				//the pulldown array lists the same for each
				$array[$row->name] = $row->name;
			}
			return $array;
		}




		// public function getAllGroups()
		// {
			
		// 	$query = $this->db->query('SELECT name FROM categories');


		// 	return $this->db->query($query)->result();
		// }

	// public function submit_category()
	// {
	// 	$data = array(
	// 		'category' => $this->input->post('1'),
	// 		'Getting-Started' => $this->input->post('2'),
	// 		'FAQ' => $this->input->post('3')
	// 	);
	
	// 	return $this->db->insert('features', $data);
	// }

public function row_delete($id) {    
		$this->db->where('id', $id);   
		$this->db->delete('features');
	}
	
public function getCategory(){


		$categories = $this->db->get('categories');
	
		$data = array();	
		
		if($categories->num_rows() > 1){
		
		foreach ($categories->result_array() as $row) {
		
		  $data[$row['id']] = $row['name'];
		
		}
	
		return $data;
	
		}
		else {
			return false;
		}
	
	}



	public function edit_category($id = NULL)
	{
		if (isset($id))
		{
		
		return $this->db->select('id, title,options')
			->where('features.id', $id)
			->limit(1)
			->order_by('id', 'options')
			->get('features')
			->row();
		}
		return false;
	}

	public function update_features($id, $data)
	{
		$this->db->update('features', $data, array('id' => $id));
        //  $this->db->update('categories', $data);
        }
  }
