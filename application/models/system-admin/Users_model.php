<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Users_model extends CI_Model {

    public function __construct()
    {
        parent::__construct();
    }

    public function get_links($id)
    {
        $this->db->select('*')->order_by('position', 'asc')->order_by('id', 'desc');            
        $query = $this->db->get_where('links', array('user_id' => $id));
        $result = $query->result_array();

        $count = count($result);

        if(empty($count)){
            return false;
        }
        else{
            return $result;
        }
    }

    public function get_user_theme($user_id)
    {



        // nvn
        $query = $this->db->select('theme_id')
                          ->where('user_id', $user_id)
                          ->limit(1)
                          ->get('user_themes');




        if ($query->num_rows() === 1)
        {
          $result= $query->row()->theme_id;
        $query2 = $this->db->select('themes.name')
         ->from('themes')
         ->join('user_themes', 'themes.id='.$result)
         ->get();

         if ($query2->num_rows() === 1)
        {
         return $query2->row('name');
        }

        } else {
            return false;
        }
    }

}
