<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Dashboard_model extends CI_Model
	{
		public function get_links($id)
		{
			$this->db->select('id, title, url, position, active')->order_by('position', 'asc')->order_by('id', 'desc');            
	        $query = $this->db->get_where('links', array('user_id' => $id, 'deleted !=' => 1));
	        $result = $query->result_array();

	        $count = count($result);

	        if(empty($count)){
	            return false;
	        }
	        else{
	            return $result;
	        }
		}
		public function insert_link($data)
		{
		 	if($this->db->insert('links', $data))
			{
				$insert_id = $this->db->insert_id();
   				return $insert_id;
			}
			return false;
		}
		public function remove_link($data)
		{
			$this->db->where('id', $data['id']);
			$this->db->update('links', $data);
			return ($this->db->affected_rows() > 0);
		}
		// input-data-insert
		public function insert_link_data($data, $id)
		{
			if($id){
				$this->db->where('id', $id);
    			$this->db->update('links', $data);
				return ($this->db->affected_rows() > 0);
			}
		}

		public function update_link_position($data)
		{
			$this->db->update_batch('links', $data, 'id');
			return ($this->db->affected_rows() > 0);
		}


	}


