<?php
class upd_model extends CI_Model {
// model constructor function
function __construct() {
    parent::__construct(); // call parent constructor
    $this->load->database();
}
// ss
public function update_title($data)
{
   return $this->db->set($data)
         ->where('instagram_id',$data['instagram_id'])
        ->update('users', $data);
}
public function row_deactivate($data)
{
    return $this->db->set('active', 0)
         ->where('instagram_id',$data['instagram_id'])
        ->update('users');
}
public function update_color($data)
{
   return $this->db->set($data)
         ->where('instagram_id',$data['instagram_id'])
        ->insert('users', $data);
        
}
}

