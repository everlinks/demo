<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Template {

    protected $CI;

    public function __construct()
    {	
		$this->CI =& get_instance();
    }


    public function admin_render($content, $data = NULL)
    {
        if ( ! $content)
        {
            return NULL;
        }
        else
        {
            $this->template['header']          = $this->CI->load->view('system-admin/_templates/header', $data, TRUE);
            $this->template['main_header']     = $this->CI->load->view('system-admin/_templates/main_header', $data, TRUE);
            $this->template['main_sidebar']    = $this->CI->load->view('system-admin/_templates/main_sidebar', $data, TRUE);
            $this->template['content']         = $this->CI->load->view($content, $data, TRUE);
            $this->template['control_sidebar'] = $this->CI->load->view('system-admin/_templates/control_sidebar', $data, TRUE);
            $this->template['footer']          = $this->CI->load->view('system-admin/_templates/footer', $data, TRUE);

            return $this->CI->load->view('system-admin/_templates/template', $this->template);
        }
	}


    public function auth_render($content, $data = NULL)
    {
        if ( ! $content)
        {
            return NULL;
        }
        else
        {
            $this->template['header']  = $this->CI->load->view('auth/_templates/header', $data, TRUE);
            $this->template['content'] = $this->CI->load->view($content, $data, TRUE);
            $this->template['footer']  = $this->CI->load->view('auth/_templates/footer', $data, TRUE);

            return $this->CI->load->view('auth/_templates/template', $this->template);
        }
    }
    
    public function public_render($content, $data = NULL)
    {
        if ( ! $content)
        {
            return NULL;
        }
        else
        {
            $this->template['header']  = $this->CI->load->view('public/_templates/header', $data, TRUE);
            $this->template['content'] = $this->CI->load->view($content, $data, TRUE);
            $this->template['footer']  = $this->CI->load->view('public/_templates/footer', $data, TRUE);

            return $this->CI->load->view('public/_templates/template', $this->template);
        }
	}

    public function user_render($content, $data = NULL)
    {
        if ( ! $content)
        {
            return NULL;
        }
        else
        {
            $this->template['header']       = $this->CI->load->view('admin/_templates/header', $data, TRUE);
            $this->template['sidebar']      = $this->CI->load->view('admin/_templates/sidebar', $data, TRUE);
            if (isset($data['main_header']) && $data['main_header'] == true){
                $this->template['main_header'] = $this->CI->load->view('admin/_templates/main_header', $data, TRUE);
            }
            $this->template['content']      = $this->CI->load->view($content, $data, TRUE);
            if (isset($data['preview']) && $data['preview'] == true){
                $this->template['preview'] = $this->CI->load->view('admin/_templates/preview', $data, TRUE);
            }
            $this->template['footer']       = $this->CI->load->view('admin/_templates/footer', $data, TRUE);

            return $this->CI->load->view('admin/_templates/template', $this->template);
        }
    }
    public function support_render($content, $data = NULL)
    {
        if ( ! $content)
        {
            return NULL;
        }
        else
        {
            $this->template['header']  = $this->CI->load->view('support/templates/header', $data, TRUE);
            $this->template['content'] = $this->CI->load->view($content, $data, TRUE);
            $this->template['footer']  = $this->CI->load->view('support/templates/footer', $data, TRUE);

            return $this->CI->load->view('support/templates/template', $this->template);
        }
    }

}