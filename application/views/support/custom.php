<div class="container">
      <div class="content educate_content"><section class="section section__article">
      <div class="breadcrumb" dir="ltr">
  <div class="link__arrow o__ltr">
    <a href="/">All Collections</a>
  </div>

      <div class="link__arrow o__ltr">
        <a href="/features-how-to">Features How To</a>
      </div>

  <div class="link__arrow o__ltr">Add Thumbnails and custom avatar</div>
</div>

  <div class="paper paper__large">
  <div class="content content__narrow">
    <div class="article intercom-force-break">
      <div class="article__meta" dir="ltr">
        <h1 class="t__h1">​Add Thumbnails and custom avatar</h1>
        <div class="article__desc">
         "How you can add your own picture to your links and custom avatar to your Everlinks profile."
        
          
        </div>
        <div class="avatar">
  <div class="avatar__photo o__ltr">
  <img src="https://static.intercomassets.com/avatars/1952807/square_128/4eTy83ekQvKfdf0G3epUWA_thumb_604-1528252847.jpg?1528252847" alt="Cameron Mackinnon avatar" class="avatar__image">
  </div>
  <div class="avatar__info">
    <div>
      Written by <span class="c__darker"> Cameron Mackinnon</span>
        <br> Updated over a week ago
    </div>
  </div>
</div>

      </div>
      <article dir="ltr"><p class="intercom-align-left"><b>PLEASE NOTE:</b> these are PRO features only.&nbsp;</p>
      <p class="intercom-align-left">One of our most requested features is finally here! You can now add images to your link buttons. When adding an image, you can upload your own or choose from your Instagram images to add to you links!</p>
      <p class="intercom-align-left">To add your own thumbnail, simply go to your My Links section in your Admin Page and select the 'Add Thumbnail' button (the first button on the left).&nbsp;</p>

<div class="intercom-container intercom-align-left"><img src="https://downloads.intercomcdn.com/i/o/72190139/b9d76e0baae99bae4c476e2d/Screen+Shot+2018-08-16+at+12.48.36+pm.png"></div>
<p class="intercom-align-left">You can now also upload your own profile picture / avatar! You are no longer tied to having the exact same image as your Instagram profile.</p>
<p class="intercom-align-left">To upload your own avatar, go to the Appearance section under your Settings tab and press the 'Upload Avatar' button.&nbsp;</p>
<p class="intercom-align-left"></p>
<div class="intercom-container intercom-align-left"><img src="https://downloads.intercomcdn.com/i/o/72631323/e9b7530ceb86e72f1b8af77d/Screen+Shot+2018-08-20+at+3.15.54+pm.png"></div>
<p class="intercom-align-left">If you have any questions, please don't hesitate to contact our support staff.&nbsp;</p>
<p class="intercom-align-left">Enjoy!<br></p>
   <article>
    </div>
  </div>
  <div class="intercom-reaction-picker" dir="ltr">
  <div class="intercom-reaction-prompt">Did this answer your question?</div>

    <button class="intercom-reaction" data-reaction-text="disappointed" tabindex="0" aria-label="Disappointed Reaction">
      <span data-emoji="disappointed" title="Disappointed"><span style="display:inline-block;height:32px;width:32px;background-position:-960px -640px;" title="disappointed" class="intermoji-default-class"></span></span>
    </button>
    <button class="intercom-reaction" data-reaction-text="neutral_face" tabindex="0" aria-label="Neutral face Reaction">
      <span data-emoji="neutral_face" title="Neutral face"><span style="display:inline-block;height:32px;width:32px;background-position:-960px -192px;" title="neutral_face" class="intermoji-default-class"></span></span>
    </button>
    <button class="intercom-reaction" data-reaction-text="smiley" tabindex="0" aria-label="Smiley Reaction">
      <span data-emoji="smiley" title="Smiley"><span style="display:inline-block;height:32px;width:32px;background-position:-736px -928px;" title="smiley" class="intermoji-default-class"></span></span>
    </button>
</div>

</div>

</section>
</div>
    </div>
    <style>
.container {
    padding-left: 40px;
    padding-right: 40px;
}
.content {
    max-width: 100%;
    width: 900px;
    margin-left: auto;
    margin-right: auto;
}
.section {
    padding-top: 26px;
}
.breadcrumb {
    position: relative;
    z-index: 11;
}
.link__arrow {
    position: relative;
    font-size: 14px;
    text-decoration: none;
    display: inline-block;
    color: #8f919d;
    margin: 0 7px 7px 0;
    padding-right: 16px;
    top: -6px;
}
.link__arrow a {
    color: #4f5e6b;
    text-decoration: none;
}
.paper__large {
    padding: 60px;
}
.paper {
    background-color: white;
   
   
    z-index: 3;
    text-decoration: none;
    
    width: 100%;
    display: block;
    outline: none;
    border: 1px solid #d4dadf;
}
.content__narrow {
    width: 640px;
}
body {
    line-height: unset;
}
.content{
  max-width: 100%;
  margin-left: auto;
    margin-right: auto;
}
.t__h1 {
    margin: 0 0 17px;
    font-size: 33px;
    line-height: 1.24;
    color: #3a3c4c;
    font-weight: normal;

}
.avatar {
    font-size: 13px;
    color: #8f919d;
    margin-top: 4px;
}
.avatar__info, .avatar__photo {
    line-height: 1.4;
}
.avatar__photo {
    margin-right: 10px;
    float: left;
}
.avatar__image {
    width: 32px;
    height: 32px;
    vertical-align: bottom;
    border-radius: 50%;
    box-shadow: 0 0 0 2px white;
    position: relative;
    z-index: 4;
}
.c__darker {
    color: #4f5e6b;
}
.article__desc {
    font-size: 18px;
    line-height: 1.35;
    margin-bottom: 17px;
    color: #8F919D;
}
.avatar__info{
    line-height: 1.4;
}
article {
    font-size: 15px;
    color: #565867;
}
.article a, .c__primary {
    color: #1f8ded;
    text-decoration: underline;
}
img{
    vertical-align: middle;
}
.intercom-reaction-picker {
    backface-visibility: hidden;
    padding: 5px;
    text-align: center;
    color: #777;
    background-color: #f0f3f5;
    margin: 60px -60px -60px -60px;
}
.intercom-reaction-picker .intercom-reaction-prompt {
    padding-top: 9px;
    padding-bottom: 4px;
    text-align: center;
}
.intercom-reaction-picker .intercom-reaction {
    background: none;
    color: inherit;
    border: none;
    padding: 0;
    font: inherit;
    cursor: pointer;
    width: 50px;
    height: 100%;
    display: inline-block;
    text-align: center;
    font-size: 32px;
    transition: transform 0.16s cubic-bezier(0.65, 0.61, 0.18, 1.8) 0.02s,filter 0.32s linear,-webkit-transform 0.16s cubic-bezier(0.65, 0.61, 0.18, 1.8) 0.02s,-webkit-filter 0.32s linear;
}
p .intercom-align-left{
  font-size: 15px;


}
.intercom-align-left{
  margin-bottom: 17px;
}
.intercom-reaction-picker .intercom-reaction span {
    cursor: pointer;
    line-height: 55px;
}
.article__meta {
    margin-bottom: 30px;
}




    </style>