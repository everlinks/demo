<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.4.2/css/all.css" integrity="sha384-/rXc/GQVaYpyDdyxK+ecHPVYJSN9bmVFBvjA/9eOB+pb3F2w2N6fc5qB9Ew5yIns" crossorigin="anonymous">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
<form method="POST" action="<?php echo site_url('admin/smiley/smiley_save'); ?>" id="myform"> 
<div class="container">
      <div class="content educate_content"><section class="section section__article">
  <div class="breadcrumb" dir="ltr">
  <div class="link__arrow o__ltr">
    <a href="/">All Collections</a>
  </div>

      <div class="link__arrow o__ltr">
        <a href="/features-how-to">Features How To</a>
      </div>

  <div class="link__arrow o__ltr">​Priority Links</div>
</div>

  <div class="paper paper__large">
  <div class="content content__narrow">
    <div class="article intercom-force-break">
      <div class="article__meta" dir="ltr">
        <h1 class="t__h1">​Priority Links</h1>
        <div class="article__desc">
          
        </div>
        <div class="avatar">
  <div class="avatar__photo o__ltr">
      <img src="https://static.intercomassets.com/avatars/417990/square_128/IMG_2270-1463315014.JPG?1463315014" alt="Alex Zaccaria avatar" class="avatar__image">

  </div>
  <div class="avatar__info">
    <div>
      Written by <span class="c__darker"> Alex Zaccaria</span>
        <br> Updated over a week ago
    </div>
  </div>
</div>

      </div>
      <article dir="ltr"><p class="intercom-align-left">Note: This is a<a href="https://linktr.ee/pro" rel="nofollow noopener noreferrer" target="_blank"> Everlinks PRO</a> feature <br><br>With priority links, you can choose one of your links to draw more attention to your visitors than the rest of your links. <br><br>This is great for those really important links such as your call to actions or sales links. You can also use it to highlight the link the relates to your most recent post, to make it super easy for your visitors.</p>
<p class="intercom-align-left"><b>To use priority links</b><br>1 - If you're a PRO user you will notice a star icon on each of your links. Click the star<br>2 - Select which priority link style you would like</p>
<div class="intercom-container intercom-align-left"><img src="https://downloads.intercomcdn.com/i/o/68284543/5adadf2697774c5fbe08b033/image.png"></div><p class="intercom-align-left">It's that simple!<br><br><b>Only one priority link at a time<br></b>You can only select one priority link at a time, so choose wisely! The entire purpose of a priority link is to draw the attention of your visitors to that one link. <br><br></p></article>
    </div>
  </div>
  <div class="intercom-reaction-picker intercom-reaction-picker-reaction-selected" dir="ltr">
  <div class="intercom-reaction-prompt">Did this answer your question?</div>


<label class="theme-option theme-option--disappointed" name="reaction" for="reaction" data-active-highlight-for="#colourway-radio-disappointed" >
<div class="theme__icon" data-height-padder>
<i class="far fa-frown">
<input type="radio" id="disappointed" name="reaction" data-settings-setting=""  data-settings-setting-key="account[colourway]" data-settings-setting-value="disappointed" value="disappointed" setting="" style="
    visibility: visible;    margin-left: 16px; cursor:pointer;
" onclick="myFunction(this)" data-id="1">
</i>
</label>

<label class="theme-option theme-option--neutral_face" name="reaction" for="reaction" data-active-highlight-for="#colourway-radio-neutral_face" >
<div class="theme__icon" data-height-padder>
<i class="far fa-meh">

<input type="radio" id="neutral_face" name="reaction" data-settings-setting=""  data-settings-setting-key="account[colourway]" data-settings-setting-value="neutral_face" value="neutral_face" setting="" style="
    visibility: visible;    margin-left: 16px;  cursor:pointer;
" onclick="myFunction(this)" data-id="2">
</i></label>


<label class="theme-option theme-option--smile" name="reaction" for="reaction" data-active-highlight-for="#colourway-radio-smile" >
<div class="theme__icon" data-height-padder>
<i class="far fa-smile">
<input type="radio" id="smiley" name="reaction" data-settings-setting=""  data-settings-setting-key="account[colourway]" data-settings-setting-value="smile" value="smile" setting="" style="
    visibility: visible;    margin-left: 16px;  cursor:pointer;
" onclick="myFunction(this)" data-id="3">
</label></i>



    <!-- <button class="intercom-reaction" type="submit"  name="disappointed" for="disappointed" data-reaction-text="disappointed" aria-label="Disappointed Reaction">
      <span data-emoji="disappointed" title="Disappointed"  onclick="myFunction(this)" data-id="1"><span style="display:inline-block;height:32px;width:32px;background-position:-960px -640px;" title="disappointed" class="intermoji-default-class"></span></span>
    </button> -->
    <!-- <button class="intercom-reaction" type="submit" data-settings-setting=""  data-reaction-text="neutral_face" name="neutral_face" for="neutral_face" aria-label="Neutral face Reaction">
      <span data-emoji="neutral_face" title="Neutral face"   onclick="myFunction(this)" data-id="2"><span style="display:inline-block;height:32px;width:32px;background-position:-960px -192px;" title="neutral_face" class="intermoji-default-class"></span></span>
    </button>
    <button class="intercom-reaction" type="submit" data-settings-setting="" data-reaction-text="smile" name="	
    smile" for="smile" aria-label="Smiley Reaction" >
      <span data-emoji="smile" title="Smile"  onclick="myFunction(this)" data-id="3"><span style="display:inline-block;height:32px;width:32px;background-position:-736px -928px;" title="smile" class="intermoji-default-class"></span></span>
    </button> -->
</div>

    
</div>

</div>

</section>
</form>
<script>

function myFunction(elem)
{
    var id = $(elem).data("id");
    if (id === 1){
    
    document.getElementById("disappointed").style.background ="green";
  
   //  window.location.replace("theme/color_save");
 
    }

  if (id === 2){

   document.getElementById("neutral_face").style.background ="pink";
 
   // window.location.replace("theme/color_save");
   }


   if (id === 3){

    document.getElementById("smiley").style.background ="blue";

   //  window.location.replace("theme/color_save");
   }


   $.ajax({
       url: $('#myform').attr('action'),
       type: "POST",
       data: {id : id},
         success: function(data) {
                    alert(data+' Submitted');
        },
        failure: function(errMsg) {
            alert(errMsg);
        }
   });


}


    </script>
    <style>
.container {
    padding-left: 40px;
    padding-right: 40px;
}
.theme__icon{
    display: inline-flex;
}
.far{
    font-size: 44px;
    display: grid;
    margin-left: 10px;
    cursor: pointer;
}
.far:hover{
color: #1d8a8a;
}

.content {
    max-width: 100%;
    width: 900px;
    margin-left: auto;
    margin-right: auto;
}
.section {
    padding-top: 26px;
}
.breadcrumb {
    position: relative;
    z-index: 11;
}
.link__arrow {
    position: relative;
    font-size: 14px;
    text-decoration: none;
    display: inline-block;
    color: #8f919d;
    margin: 0 7px 7px 0;
    padding-right: 16px;
    top: -6px;
}
.link__arrow a {
    color: #4f5e6b;
    text-decoration: none;
}
.paper__large {
    padding: 60px;
}
.paper {
    background-color: white;
   
   
    z-index: 3;
    text-decoration: none;
    
    width: 100%;
    display: block;
    outline: none;
    border: 1px solid #d4dadf;
}
.content__narrow {
    width: 640px;
}
body {
    line-height: unset;
}
.content{
  max-width: 100%;
  margin-left: auto;
    margin-right: auto;
}
.t__h1 {
    margin: 0 0 17px;
    font-size: 33px;
    line-height: 1.24;
    color: #3a3c4c;
    font-weight: normal;

}
.avatar {
    font-size: 13px;
    color: #8f919d;
    margin-top: 4px;
}
.avatar__info, .avatar__photo {
    line-height: 1.4;
}
.avatar__photo {
    margin-right: 10px;
    float: left;
}
.avatar__image {
    width: 32px;
    height: 32px;
    vertical-align: bottom;
    border-radius: 50%;
    box-shadow: 0 0 0 2px white;
    position: relative;
    z-index: 4;
}
.c__darker {
    color: #4f5e6b;
}
.avatar__info{
    line-height: 1.4;
}
article {
    font-size: 15px;
    color: #565867;
}
.article a, .c__primary {
    color: #1f8ded;
    text-decoration: underline;
}
img{
    vertical-align: middle;
}
.intercom-reaction-picker {
    backface-visibility: hidden;
    padding: 5px;
    text-align: center;
    color: #777;
    background-color: #f0f3f5;
    margin: 60px -60px -60px -60px;
}
.intercom-reaction-picker .intercom-reaction-prompt {
    padding-top: 9px;
    padding-bottom: 4px;
    text-align: center;
}
.intercom-reaction-picker .intercom-reaction {
    background: none;
    color: inherit;
    border: none;
    padding: 0;
    font: inherit;
    cursor: pointer;
    width: 50px;
    height: 100%;
    display: inline-block;
    text-align: center;
    font-size: 32px;
    transition: transform 0.16s cubic-bezier(0.65, 0.61, 0.18, 1.8) 0.02s,filter 0.32s linear,-webkit-transform 0.16s cubic-bezier(0.65, 0.61, 0.18, 1.8) 0.02s,-webkit-filter 0.32s linear;
}
p .intercom-align-left{
  font-size: 15px;


}
.intercom-align-left{
  margin-bottom: 17px;
}
.intercom-reaction-picker .intercom-reaction span {
    cursor: pointer;
    line-height: 55px;
}
.article__meta {
    margin-bottom: 30px;
}
.intercom-reaction-picker{
    height: 153px;

}
.intercom-reaction:onclick{
    height: 69px;
}

    </style>