<head>
    <link href="https://fonts.intercomcdn.com" rel="preconnect" crossorigin="">

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>FAQ | Everlinks Help Center</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="intercom:trackingEvent" content="{&quot;name&quot;:&quot;Viewed Collection&quot;,&quot;metadata&quot;:{&quot;action&quot;:&quot;viewed&quot;,&quot;object&quot;:&quot;collection&quot;,&quot;place&quot;:&quot;help_center&quot;,&quot;owner&quot;:&quot;educate&quot;,&quot;collection&quot;:{&quot;id&quot;:&quot;2883397&quot;,&quot;url&quot;:&quot;https://support.linktr.ee/faq&quot;,&quot;value&quot;:&quot;FAQ&quot;}}}">

    <link rel="stylesheet" media="all" href="https://intercom.help/_assets/application-cf96f613358ad1f77fb9ea03098706a52f28f50d9c46df57184f74e53e1941ba.css">
    <link rel="canonical" href="https://support.linktr.ee/faq">

        <link href="https://downloads.intercomcdn.com/i/o/11263/ae0f4427109b4978103ff80c/ms-icon-310x310.png" rel="shortcut icon" type="image/png">
      <style>
        .header, .avatar__image-extra { background-color: #39E09B; }
        .article a, .c__primary { color: #39E09B; }
        .avatar__fallback { background-color: #39E09B; }
        article a.intercom-h2b-button { background-color: #39E09B; border: 0; }
      </style>

      <meta property="og:title" content="FAQ | Everlinks Help Center">
  <meta name="twitter:title" content="FAQ | Everlinks Help Center">


<meta property="og:type" content="website">
<meta property="og:image" content="">

<meta name="twitter:image" content="">

<body>
<header class="header">
  <div class="container header__container o__ltr" dir="ltr">
    <div class="content">
      <div class="mo o__centered o__reversed header__meta_wrapper">
        <div class="mo__body">
          <div class="header__logo">
            <a href="/">
<h>Everlinks</h>            </a>
          </div>
        </div>
        <div class="mo__aside">
          <div class="header__home__url">
          </div>
        </div>
      </div>
        <form action="/" autocomplete="off" class="header__form search">
          <input type="text" autocomplete="off" class="search__input js__search-input o__ltr" placeholder="Search for answers..." tabindex="1" name="q" value="">
          <div class="search_icons">
            <button type="submit" class="search__submit o__ltr"></button>
            <a class="search__clear-text__icon">
              <svg class="interface-icon" xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 16 16">
                <path d="M8.018 6.643L5.375 4 4 5.375l2.643 2.643L4 10.643 5.375 12l2.643-2.625L10.625 12 12 10.643 9.357 8.018 12 5.375 10.643 4z"></path>
              </svg>
            </a>
        
      </div></form>
    </div>
  </div>
</header>
</body>
<style>
.header, .avatar__image-extra {
    background-color:#1d8a8a;
}
h{
    font-size: 50px;
    line-height: 1.24;
    color: white;

}
</style>