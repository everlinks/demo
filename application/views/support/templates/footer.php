<footer class="footer">
  <div class="container">
    <div class="content">
      <div class="u__cf" dir="ltr">
        <div class="footer__logo">
          <a href="/">
<h2>EverLinks</h2>
          </a>
        </div>
        <div class="footer__advert logo">
          <img src="https://intercom.help/_assets/intercom-a6a6ac0f033657af1aebe2e9e15b94a3cd5eabf6ae8b9916df6ea49099a894d8.png" alt="Intercom">
          <a href="https://www.intercom.com/intercom-link?company=Everlinks&amp;solution=customer-support&amp;utm_campaign=intercom-link&amp;utm_content=We+run+on+Intercom&amp;utm_medium=help-center&amp;utm_referrer=https%3A%2F%2Fsupport.linktr.ee%2Ffaq&amp;utm_source=desktop-web">We run on Intercom</a>
        </div>
      </div>
    </div>
  </div>
</footer>
<style>
h2 {
    font-size: 41px;
    line-height: normal;
    color: grey;
}
</style>