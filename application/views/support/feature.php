<div class="container">
    <div class="content educate_content">
        <section class="content section">
            <div class="breadcrumb">
            <div class="link__arrow o__ltr">
    <a href="#">All Collections</a>
  </div>
  <div class="link__arrow o__ltr">Features How To</div>
</div>
<div class="section__bg">
<div class="paper g__space collection__headline">
  <div class="collection o__ltr">
    <div class="collection__photo">
      <svg role="img" viewBox="0 0 48 48"><g id="book-bookmark" stroke-width="2" fill="none" fill-rule="evenodd" stroke-linecap="round"><path d="M35 31l-6-6-6 6V7h12v24z"></path><path d="M35 9h6v38H11a4 4 0 0 1-4-4V5" stroke-linejoin="round"></path><path d="M39 9V1H11a4 4 0 0 0 0 8h12" stroke-linejoin="round"></path></g></svg>
    </div>
    <div class="collection__meta intercom-force-break" dir="ltr">
      <h2 class="t__h1">Features How To</h2>
      <p class="paper__preview"></p>
      <div class="avatar">
  <div class="avatar__photo avatars__images o__ltr">
        <img src="https://static.intercomassets.com/avatars/417990/square_128/IMG_2270-1463315014.JPG?1463315014" alt="Alex Zaccaria avatar" class="avatar__image">

        <img src="https://static.intercomassets.com/avatars/1952807/square_128/4eTy83ekQvKfdf0G3epUWA_thumb_604-1528252847.jpg?1528252847" alt="Cameron Mackinnon avatar" class="avatar__image">

  </div>
  <div class="avatar__info">
    <div>
      <span class="c__darker">
        11 articles in this collection
      </span>
      <br>
      Written by <span class="c__darker"> Alex Zaccaria</span> and <span class="c__darker"> Cameron Mackinnon</span>
    </div>
  </div>
</div>

    </div>
  </div>
</div>
<div class="g__space">
    <a href="<?php echo site_url('support/feature/priority'); ?>" class="t__no-und paper paper__article-preview">
      <div class="article__preview intercom-force-break" dir="ltr">
  <h2 class="t__h3"><span class="c__primary">​Priority Links</span></h2>
  <span class="paper__preview c__body"></span>
  <div class="avatar">
  <div class="avatar__photo o__ltr">
      <img src="https://static.intercomassets.com/avatars/417990/square_128/IMG_2270-1463315014.JPG?1463315014" alt="Alex Zaccaria avatar" class="avatar__image">

  </div>
  <div class="avatar__info">
    <div>
      Written by <span class="c__darker"> Alex Zaccaria</span>
        <br> Updated over a week ago
    </div>
  </div>
</div>

</div>

    </a>
    <a href="<?php echo site_url('support/feature/custom'); ?>" class="t__no-und paper paper__article-preview">
      <div class="article__preview intercom-force-break" dir="ltr">
  <h2 class="t__h3"><span class="c__primary" >Add Thumbnails and custom avatar</span></h2>
  <span class="paper__preview c__body">How you can add your own picture to your links and custom avatar to your Everlinks profile.</span>
  <div class="avatar">
  <div class="avatar__photo o__ltr">
      <img src="https://static.intercomassets.com/avatars/1952807/square_128/4eTy83ekQvKfdf0G3epUWA_thumb_604-1528252847.jpg?1528252847" alt="Cameron Mackinnon avatar" class="avatar__image">

  </div>
  <div class="avatar__info">
    <div>
      Written by <span class="c__darker"> Cameron Mackinnon</span>
        <br> Updated over a week ago
    </div>
  </div>
</div>

</div>

    </a>
    <a href="<?php echo site_url('support/email/email'); ?>" class="t__no-und paper paper__article-preview">
      <div class="article__preview intercom-force-break" dir="ltr">
  <h2 class="t__h3"><span class="c__primary">Email Signup Integration</span></h2>
  <span class="paper__preview c__body">Configuring your email signup button for Mailchimp, Zapier and Google Sheets</span>
  <div class="avatar">
  <div class="avatar__photo o__ltr">
      <img src="https://static.intercomassets.com/avatars/417990/square_128/IMG_2270-1463315014.JPG?1463315014" alt="Alex Zaccaria avatar" class="avatar__image">

  </div>
  <div class="avatar__info">
    <div>
      Written by <span class="c__darker"> Alex Zaccaria</span>
        <br> Updated over a week ago
    </div>
  </div>
</div>

</div>

    </a>
    <a href="<?php echo site_url('support/statistics/statistics'); ?>" class="t__no-und paper paper__article-preview">
      <div class="article__preview intercom-force-break" dir="ltr">
  <h2 class="t__h3"><span class="c__primary">Daily link click statistics</span></h2>
  <span class="paper__preview c__body">How you can track your daily link traffic as a Pro user</span>
  <div class="avatar">
  <div class="avatar__photo o__ltr">
      <img src="https://static.intercomassets.com/avatars/1952807/square_128/4eTy83ekQvKfdf0G3epUWA_thumb_604-1528252847.jpg?1528252847" alt="Cameron Mackinnon avatar" class="avatar__image">

  </div>
  <div class="avatar__info">
    <div>
      Written by <span class="c__darker"> Cameron Mackinnon</span>
        <br> Updated over a week ago
    </div>
  </div>
</div>

</div>

    </a>
    <a href="/features-how-to/picture-backgrounds" class="t__no-und paper paper__article-preview">
      <div class="article__preview intercom-force-break" dir="ltr">
  <h2 class="t__h3"><span class="c__primary">Picture backgrounds</span></h2>
  <span class="paper__preview c__body">How to add picture backgrounds to your Everlinks.</span>
  <div class="avatar">
  <div class="avatar__photo o__ltr">
      <img src="https://static.intercomassets.com/avatars/417990/square_128/IMG_2270-1463315014.JPG?1463315014" alt="Alex Zaccaria avatar" class="avatar__image">

  </div>
  <div class="avatar__info">
    <div>
      Written by <span class="c__darker"> Alex Zaccaria</span>
        <br> Updated over a week ago
    </div>
  </div>
</div>

</div>

    </a>
    <a href="/features-how-to/managing-your-links" class="t__no-und paper paper__article-preview">
      <div class="article__preview intercom-force-break" dir="ltr">
  <h2 class="t__h3"><span class="c__primary">Managing Your Links</span></h2>
  <span class="paper__preview c__body">Explanation of how to manage your links on Everlinks</span>
  <div class="avatar">
  <div class="avatar__photo o__ltr">
      <img src="https://static.intercomassets.com/avatars/417990/square_128/IMG_2270-1463315014.JPG?1463315014" alt="Alex Zaccaria avatar" class="avatar__image">

  </div>
  <div class="avatar__info">
    <div>
      Written by <span class="c__darker"> Alex Zaccaria</span>
        <br> Updated over a week ago
    </div>
  </div>
</div>

</div>

    </a>
</div>
<div class="g__space">
    <div class="section__headline" id="Everlinks-pro-features" dir="ltr">
      <div class="section__content intercom-force-break">Everlinks PRO Features</div>
    </div>

      <a href="/features-how-to/Everlinks-pro-features/Everlinks-pro-features-overview" class="t__no-und paper paper__article-preview">
        <div class="article__preview intercom-force-break" dir="ltr">
  <h2 class="t__h3"><span class="c__primary">Everlinks Pro Features Overview</span></h2>
  <span class="paper__preview c__body">A quick walk through of our new PRO features</span>
  <div class="avatar">
  <div class="avatar__photo o__ltr">
      <img src="https://static.intercomassets.com/avatars/417990/square_128/IMG_2270-1463315014.JPG?1463315014" alt="Alex Zaccaria avatar" class="avatar__image">

  </div>
  <div class="avatar__info">
    <div>
      Written by <span class="c__darker"> Alex Zaccaria</span>
        <br> Updated over a week ago
    </div>
  </div>
</div>

</div>

      </a>
      <a href="/features-how-to/Everlinks-pro-features/changing-the-title-of-your-Everlinks" class="t__no-und paper paper__article-preview">
        <div class="article__preview intercom-force-break" dir="ltr">
  <h2 class="t__h3"><span class="c__primary">Changing the title of your Everlinks</span></h2>
  <span class="paper__preview c__body">How Pro users can change the title on their Everlinks</span>
  <div class="avatar">
  <div class="avatar__photo o__ltr">
      <img src="https://static.intercomassets.com/avatars/1952807/square_128/4eTy83ekQvKfdf0G3epUWA_thumb_604-1528252847.jpg?1528252847" alt="Cameron Mackinnon avatar" class="avatar__image">

  </div>
  <div class="avatar__info">
    <div>
      Written by <span class="c__darker"> Cameron Mackinnon</span>
        <br> Updated over a week ago
    </div>
  </div>
</div>

</div>

      </a>
      <a href="/features-how-to/Everlinks-pro-features/utm-parameters" class="t__no-und paper paper__article-preview">
        <div class="article__preview intercom-force-break" dir="ltr">
  <h2 class="t__h3"><span class="c__primary">UTM Parameters</span></h2>
  <span class="paper__preview c__body">How custom UTM parameters work and how they affect your analytics</span>
  <div class="avatar">
  <div class="avatar__photo o__ltr">
      <img src="https://static.intercomassets.com/avatars/417990/square_128/IMG_2270-1463315014.JPG?1463315014" alt="Alex Zaccaria avatar" class="avatar__image">

  </div>
  <div class="avatar__info">
    <div>
      Written by <span class="c__darker"> Alex Zaccaria</span>
        <br> Updated over a week ago
    </div>
  </div>
</div>

</div>

      </a>
      <a href="/features-how-to/Everlinks-pro-features/facebook-pixel-integration" class="t__no-und paper paper__article-preview">
        <div class="article__preview intercom-force-break" dir="ltr">
  <h2 class="t__h3"><span class="c__primary">Facebook Pixel Integration</span></h2>
  <span class="paper__preview c__body">How to integrate Facebook and Everlinks</span>
  <div class="avatar">
  <div class="avatar__photo o__ltr">
      <img src="https://static.intercomassets.com/avatars/417990/square_128/IMG_2270-1463315014.JPG?1463315014" alt="Alex Zaccaria avatar" class="avatar__image">

  </div>
  <div class="avatar__info">
    <div>
      Written by <span class="c__darker"> Alex Zaccaria</span>
        <br> Updated over a week ago
    </div>
  </div>
</div>

</div>

      </a>
      <a href="/features-how-to/Everlinks-pro-features/schedule-your-links" class="t__no-und paper paper__article-preview">
        <div class="article__preview intercom-force-break" dir="ltr">
  <h2 class="t__h3"><span class="c__primary">Schedule your links</span></h2>
  <span class="paper__preview c__body">Here's how you can schedule your links and why it's a great idea to plan ahead</span>
  <div class="avatar">
  <div class="avatar__photo o__ltr">
      <img src="https://static.intercomassets.com/avatars/1952807/square_128/4eTy83ekQvKfdf0G3epUWA_thumb_604-1528252847.jpg?1528252847" alt="Cameron Mackinnon avatar" class="avatar__image">

  </div>
  <div class="avatar__info">
    <div>
      Written by <span class="c__darker"> Cameron Mackinnon</span>
        <br> Updated over a week ago
    </div>
  </div>
</div>

</div>

      </a>
  </div>
</div>
</section>
</div>
</div>
<style>
.content {
    max-width: 100%;
    width: 900px;
    margin-left: auto;
    margin-right: auto;
}
.section {
    padding-top: 26px;
}
.content {
    max-width: 100%;
    width: 900px;
    margin-left: auto;
    margin-right: auto;
}

.link__arrow {
  
    font-size: 14px;
    text-decoration: none;
    display: inline-block;
    color: #8f919d;
    margin: 0 7px 7px 0;
    padding-right: 16px;
    top: -6px;
}
.link__arrow a {
    color: #4f5e6b;
    text-decoration: none;
}
.section__bg {
    padding-top: 0;
    padding-bottom: 26px;
    position: relative;
    background-color: #e9ecef;
}
.collection__headline{
    background: none;
    box-shadow: none;
    border: 1px solid transparent;
    border-radius: 0;
}
.breadcrumb {
    background-color: transparent;

}
.paper {
    padding: 30px;
    position: relative;
    z-index: 3;
    text-decoration: none;
    overflow: hidden;
    width: 100%;
    display: block;
    outline: none;
}
.collection {
    position: relative;
    padding-left: 150px;
}
.collection__photo {
    position: absolute;
    left: 0;
    /* top: 0; */
    width: 120px;
    height: 100%;
    transition: all .3s linear;
}
.collection__photo svg {
    stroke: #818a97;
    position: relative;
    width: 48px;
    height: 48px;
    transition: stroke .1s linear;
    top: 50%;
    left: 50%;
    margin: -24px 0 0 -24px;
}
.t__h1 {
    margin: 0 0 17px;
    font-size: 33px;
    text-align: left;
    font-weight: normal;
    line-height: 1.24;
    color: #3a3c4c;
}
.paper__preview {
    margin: 5px 0 11px;
    text-decoration: none;
    display: block;
    max-width: 630px;
    line-height: 1.4;
}
.avatar {
    font-size: 13px;
    color: #8f919d;
    margin-top: 4px;
}
.avatar__info, .avatar__photo {
    line-height: 1.4;
    margin-right: 10px;
    float: left;
}
.avatar__image {
    box-shadow: 0 0 0 2px #ebeef1;
    width: 32px;
    height: 32px;
    vertical-align: bottom;
    border-radius: 50%;
    position: relative;
    z-index: 4;
}
.avatar__info {
    line-height: 1.4;
}
.c__darker {
    color: #4f5e6b;
}
.g__space+.g__space {
    margin-top: 16px;
}
.paper__article-preview {
    box-shadow: 0 2px 6px 0 rgba(0,0,0,0.05);
    border: 1px solid #d4dadf;
    margin-left: 30px;
    background-color: white;
    margin-right: 30px;
    width: calc(100% - 60px);
    opacity: 1;
    transition: opacity .3s;

}
.t__h3 {
    margin: -5px 0 2px;
    font-size: 18px;
    text-align: left;

}
.paper .c__primary {
    transition: -webkit-filter .15s;
}
.article a, .c__primary {
    color: #1d8a8a;
}
.section__headline {
    padding: 16px 30px 11px;
    margin: -5px 0;
    color: #3a3c4c;
    font-weight: 600;
    letter-spacing: .02em;
    font-size: 18px;
}
.paper__preview {
    margin: 5px 0 11px;
    text-decoration: none;
    display: block;
    max-width: 630px;
    line-height: 1.4;
}
.avatar__photo {
    margin-right: 10px;
    float: left;
    line-height: 1.4;
}
#leftPanel{
    background-color: #f3f5f7;
    height: fit-content;
}



    </style>