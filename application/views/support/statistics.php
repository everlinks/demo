

<div class="container">
      <div class="content educate_content"><section class="section section__article">
      <div class="breadcrumb" dir="ltr">
  <div class="link__arrow o__ltr">
    <a href="/">All Collections</a>
  </div>

      <div class="link__arrow o__ltr">
        <a href="/features-how-to">Features How To</a>
      </div>

  <div class="link__arrow o__ltr">Daily link click statistics</div>
</div>

  <div class="paper paper__large">
  <div class="content content__narrow">
    <div class="article intercom-force-break">
      <div class="article__meta" dir="ltr">
        <h1 class="t__h1">​Daily link click statistics</h1>
        <div class="article__desc">
          
        How you can track your daily link traffic as a Pro user
        
        </div>
        <div class="avatar">
  <div class="avatar__photo o__ltr">
  <img src="https://static.intercomassets.com/avatars/1952807/square_128/4eTy83ekQvKfdf0G3epUWA_thumb_604-1528252847.jpg?1528252847" alt="Cameron Mackinnon avatar" class="avatar__image">
  </div>
  <div class="avatar__info">
    <div>
      Written by <span class="c__darker">  Cameron Mackinnon</span>
        <br> Updated over a week ago
    </div>
  </div>
</div>

      </div>
      <article dir="ltr"><p class="intercom-align-left"><b>Please note:</b>" This is a" <a href="https://linktr.ee/pro" rel="nofollow noopener noreferrer" target="_blank"> Everlinks PRO</a>  feature.&nbsp;</p>
      <p class="intercom-align-left">In addition to lifetime clicks, Everlinks Pro users have the ability to access a day-by-day breakdown of link traffic as well as seven day click reports.&nbsp;</p>
      <p class="intercom-align-left">Follow these steps:</p>
      <ol>
<li>Login to your <a href="https://linktr.ee/admin/account" rel="nofollow noopener noreferrer" target="_blank">Everlinks admin page</a>
</li>
<li>On each button, you’ll see a small icon of a graph. Click this.</li>
</ol>
<div class="intercom-container intercom-align-left"><img src="https://downloads.intercomcdn.com/i/o/67663079/f1af70d1e4b8fbb3d0389529/Yo2QNuCnGQTx9ZvqFvb88lzwikWlvrwgGtjio4pR39NwwXYhsMxBXS7Xp4gKb1U6oOl_Doci14yvkzj9K7Iu8Y-qVY7Ni-XROmnsH0ao6j4T-jKRdhte9h06MOZGv9gk27VuIy8"></div>
<p class="intercom-align-left">This will show you the total lifetime clicks for that link, as well as a seven day chart to show you when your link was accessed over the past week and day-by-day link statistics.&nbsp;</p>
<p class="intercom-align-left">Find out more about our Everlinks Pro features <a href="https://support.linktr.ee/features-how-to/Everlinks-pro-features/Everlinks-pro-features-overview" rel="nofollow noopener noreferrer" target="_blank">here</a>.</p>
   </article>
    </div>
  </div>
  <div class="intercom-reaction-picker intercom-reaction-picker-reaction-selected" dir="ltr">
  <div class="intercom-reaction-prompt">Did this answer your question?</div>

    <button class="intercom-reaction" data-reaction-text="disappointed" tabindex="0" aria-label="Disappointed Reaction">
      <span data-emoji="disappointed" title="Disappointed"><span style="display:inline-block;height:32px;width:32px;background-position:-960px -640px;" title="disappointed" class="intermoji-default-class"></span></span>
    </button>
    <button class="intercom-reaction intercom-reaction-selected" data-reaction-text="neutral_face" tabindex="0" aria-label="Neutral face Reaction">
      <span data-emoji="neutral_face" title="Neutral face"><span style="display:inline-block;height:32px;width:32px;background-position:-960px -192px;" title="neutral_face" class="intermoji-default-class"></span></span>
    </button>
    <button class="intercom-reaction" data-reaction-text="smiley" tabindex="0" aria-label="Smiley Reaction">
      <span data-emoji="smiley" title="Smiley"><span style="display:inline-block;height:32px;width:32px;background-position:-736px -928px;" title="smiley" class="intermoji-default-class"></span></span>
    </button>
</div>

</div>

</section>
</div>
    </div>
    <style>
.container {
    padding-left: 40px;
    padding-right: 40px;
}
.content {
    max-width: 100%;
    width: 900px;
    margin-left: auto;
    margin-right: auto;
}
.section {
    padding-top: 26px;
}
.breadcrumb {
    position: relative;
    z-index: 11;
}
.article__desc {
    font-size: 20px;
    line-height: 1.35;
    margin-bottom: 17px;
    color: #8F919D;
}
.link__arrow {
    position: relative;
    font-size: 14px;
    text-decoration: none;
    display: inline-block;
    color: #8f919d;
    margin: 0 7px 7px 0;
    padding-right: 16px;
    top: -6px;
}
.link__arrow a {
    color: #4f5e6b;
    text-decoration: none;
}
.paper__large {
    padding: 60px;
}
li {
    list-style: circle;
}
ol{
    margin-bottom: 17px;
}
.paper {
    background-color: white;
   
   
    z-index: 3;
    text-decoration: none;
    
    width: 100%;
    display: block;
    outline: none;
    border: 1px solid #d4dadf;
}
.content__narrow {
    width: 640px;
}
body {
    line-height: unset;
}
.content{
  max-width: 100%;
  margin-left: auto;
    margin-right: auto;
}
.t__h1 {
    margin: 0 0 17px;
    font-size: 33px;
    line-height: 1.24;
    color: #3a3c4c;
    font-weight: normal;

}
.intercom-reaction-picker{
    height: 147px;
}

.avatar {
    font-size: 13px;
    color: #8f919d;
    margin-top: 4px;
}
.avatar__info, .avatar__photo {
    line-height: 1.4;
}
.avatar__photo {
    margin-right: 10px;
    float: left;
}
.avatar__image {
    width: 32px;
    height: 32px;
    vertical-align: bottom;
    border-radius: 50%;
    box-shadow: 0 0 0 2px white;
    position: relative;
    z-index: 4;
}
.c__darker {
    color: #4f5e6b;
}
.avatar__info{
    line-height: 1.4;
}
article {
    font-size: 15px;
    color: #565867;
}
.article a, .c__primary {
    color: #1f8ded;
    text-decoration: underline;
}
img{
    vertical-align: middle;
}
.intercom-reaction-picker {
    backface-visibility: hidden;
    padding: 5px;
    text-align: center;
    color: #777;
    background-color: #f0f3f5;
    margin: 60px -60px -60px -60px;
}
.intercom-reaction-picker .intercom-reaction-prompt {
    padding-top: 9px;
    padding-bottom: 4px;
    text-align: center;
}
.intercom-reaction-picker .intercom-reaction {
    background: none;
    color: inherit;
    border: none;
    padding: 0;
    font: inherit;
    cursor: pointer;
    width: 50px;
    height: 100%;
    display: inline-block;
    text-align: center;
    font-size: 32px;
    transition: transform 0.16s cubic-bezier(0.65, 0.61, 0.18, 1.8) 0.02s,filter 0.32s linear,-webkit-transform 0.16s cubic-bezier(0.65, 0.61, 0.18, 1.8) 0.02s,-webkit-filter 0.32s linear;
}
p .intercom-align-left{
  font-size: 15px;


}
.intercom-align-left{
  margin-bottom: 17px;
}
.intercom-reaction-picker .intercom-reaction span {
    cursor: pointer;
    line-height: 55px;
}
.article__meta {
    margin-bottom: 30px;
}




    </style>