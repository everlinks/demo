

<div class="container">
      <div class="content educate_content"><section class="section section__article">
      <div class="breadcrumb" dir="ltr">
  <div class="link__arrow o__ltr">
    <a href="/">All Collections</a>
  </div>

      <div class="link__arrow o__ltr">
        <a href="/features-how-to">Features How To</a>
      </div>

  <div class="link__arrow o__ltr">Email Signup Integration</div>
</div>

  <div class="paper paper__large">
  <div class="content content__narrow">
    <div class="article intercom-force-break">
      <div class="article__meta" dir="ltr">
        <h1 class="t__h1">​Email Signup Integration</h1>
        <div class="article__desc">
         
          Configuring your email signup button for Mailchimp, Zapier and Google Sheets
        
        
          
        </div>
        <div class="avatar">
  <div class="avatar__photo o__ltr">
  <img src="https://static.intercomassets.com/avatars/417990/square_128/IMG_2270-1463315014.JPG?1463315014" alt="Alex Zaccaria avatar" class="avatar__image">  </div>
  <div class="avatar__info">
    <div>
      Written by <span class="c__darker"> Alex Zaccaria</span>
        <br> Updated over a week ago
    </div>
  </div>
</div>

      </div>
      <article dir="ltr"><p class="intercom-align-left">Note: This is a Everlinks PRO Feature
      <br>
      <br>
      You can add an email signup field to your Everlinks, so you visitors can easily signup to your Mailchimp lists in just 2 clicks. (Using Google Sheets or Zapier is coming very soon too!)
<br>
<br>
The signup field is displayed as a button, that looks just like your other links, but when your visitors click it, instead of sending them off to another page, it turns into an email input field!
<br>
</p>
<h4 class="intercom-align-left" data-post-processed="true">To set up your email signup field, follow these steps!</h4>
<ol>
<li>Login to your Everlinks dashboard</li>
<li>Go to your <a href="https://linktr.ee/admin/settings" rel="nofollow noopener noreferrer" target="_blank">Settings</a>
</li>
<li>Scroll down to 'Integrations'</li>
<li>You will see a heading called 'Email Signup'. Click the toggle button to enable the feature</li>
<li>The settings are split into two sections. 'Display' and 'Storage'. <br><b>Display:</b> Is what is text you want your visitors to see<br><b>Storage: </b>Is where you're storing the signups. By default, Mialchimp is selected but you will soon be able to use other tools such as Google Sheets and Zapier</li>
</ol>
<p class="intercom-align-left"><b>Display Settings</b></p>
<ol>
<li>Set the 'Button Text' field. This is the text that is on the button for visitors to click. Make it catchy!</li>
<li>Set the 'Success Message' field. This is the message that visitors are shown after they successfully submit their email address.</li>
</ol>
<p class="intercom-align-left"><b>Storage Settings - Mailchimp<br><br></b>For all your email signups to flow straight into your Mailchimp account, you need to connect it via the Mailchimp API</p>
<ol>
<li>Login to mailchimp</li>
<li>Navigate to the <a href="https://admin.mailchimp.com/account/api/" rel="nofollow noopener noreferrer" target="_blank">API keys screen</a>, by clicking on your username in the top right corner and select 'account'. Then click 'Extras' and select API Keys. (Click <a href="https://admin.mailchimp.com/account/api/" rel="nofollow noopener noreferrer" target="_blank">here</a> for a shortcut to this screen)</li>
<li>Now scroll down and press 'Create a Key'</li>
<li>Copy the newly generated Key, and paste it into the 'API' field in Everlinks. That's all for this part!</li>
<li>Select the list you would like your new email signups to go into, from the drop down list that appears.</li>
</ol>
<p class="intercom-align-left">You will now see the button enabled on your profile. Test it out and have fun! Remember to contact us via the chat icon in the bottom right corner of your dashboard any time you need help! <br><br></p>
</article>
</div>
</div>
</div>
</div>
</div>
    <style>
.container {
    padding-left: 40px;
    padding-right: 40px;
}
.section{
    margin-bottom: 100px;

}
article h4 {
    font-size: 18px;
    margin: 0 0 15px;
    font-weight: 600;
    line-height: 1.24;
}
article ul, article ol {
    padding-left: 16px;
    margin-bottom: 27px;
}
article li {
    margin-left: 15px;
    margin-bottom: 5px;
}
.content {
    max-width: 100%;
    width: 900px;
    margin-left: auto;
    margin-right: auto;
}
.section {
    padding-top: 26px;
}
.breadcrumb {
    position: relative;
    z-index: 11;
}
.link__arrow {
    position: relative;
    font-size: 14px;
    text-decoration: none;
    display: inline-block;
    color: #8f919d;
    margin: 0 7px 7px 0;
    padding-right: 16px;
    top: -6px;
}
.link__arrow a {
    color: #4f5e6b;
    text-decoration: none;
}
.paper__large {
    padding: 60px;
}
.paper {
    background-color: white;
   
   
    z-index: 3;
    text-decoration: none;
    
    width: 100%;
    display: block;
    outline: none;
    border: 1px solid #d4dadf;
}
.content__narrow {
    width: 640px;
}
body {
    line-height: unset;
}
.content{
  max-width: 100%;
  margin-left: auto;
    margin-right: auto;
}
.t__h1 {
    margin: 0 0 17px;
    font-size: 33px;
    line-height: 1.24;
    color: #3a3c4c;
    font-weight: normal;

}
.avatar {
    font-size: 13px;
    color: #8f919d;
    margin-top: 4px;
}
.avatar__info, .avatar__photo {
    line-height: 1.4;
}
.avatar__photo {
    margin-right: 10px;
    float: left;
}
.avatar__image {
    width: 32px;
    height: 32px;
    vertical-align: bottom;
    border-radius: 50%;
    box-shadow: 0 0 0 2px white;
    position: relative;
    z-index: 4;
}
.c__darker {
    color: #4f5e6b;
}
.article__desc {
    font-size: 18px;
    line-height: 1.35;
    margin-bottom: 17px;
    color: #8F919D;
}
.avatar__info{
    line-height: 1.4;
}
article {
    font-size: 15px;
    color: #565867;
}
.article a, .c__primary {
    color: #1f8ded;
    text-decoration: underline;
}
img{
    vertical-align: middle;
}
.intercom-reaction-picker {
    backface-visibility: hidden;
    padding: 5px;
    text-align: center;
    color: #777;
    background-color: #f0f3f5;
    margin: 60px -60px -60px -60px;
}
.intercom-reaction-picker .intercom-reaction-prompt {
    padding-top: 9px;
    padding-bottom: 4px;
    text-align: center;
}
.intercom-reaction-picker .intercom-reaction {
    background: none;
    color: inherit;
    border: none;
    padding: 0;
    font: inherit;
    cursor: pointer;
    width: 50px;
    height: 100%;
    display: inline-block;
    text-align: center;
    font-size: 32px;
    transition: transform 0.16s cubic-bezier(0.65, 0.61, 0.18, 1.8) 0.02s,filter 0.32s linear,-webkit-transform 0.16s cubic-bezier(0.65, 0.61, 0.18, 1.8) 0.02s,-webkit-filter 0.32s linear;
}
p .intercom-align-left{
  font-size: 15px;


}
.intercom-align-left{
  margin-bottom: 17px;
}
.intercom-reaction-picker .intercom-reaction span {
    cursor: pointer;
    line-height: 55px;
}
.article__meta {
    margin-bottom: 30px;
}
li {
    list-style: circle;
}



    </style>