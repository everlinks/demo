<?php
defined('BASEPATH') OR exit('No direct script access allowed');

?>

            <div class="content-wrapper">
                <section class="content-header">
                    <?php echo $pagetitle; ?>
                    <?php echo $breadcrumb; ?>
                </section>

                <section class="content">
                    <div class="row">
                        <div class="col-md-12">
                             <div class="box">
                                <div class="box-header with-border">
                                    <h3 class="box-title"><?php echo anchor('system-admin/features/create', '<i class="fa fa-plus"></i> '. lang('features_create'), array('class' => 'btn btn-block btn-primary btn-flat')); ?></h3>
                                </div>
                                <div class="box-body">
                                    <table class="table table-striped table-hover">
                                        <thead>
                                            <tr>
                                            <th><?php echo lang('features_category');?></th>
                                                <th><?php echo lang('features_name');?></th>
                                                <th><?php echo lang('features_options');?></th>
                                            </tr>
                                        </thead>
                                        <tbody>
<?php foreach ($features as $values):?>
                                            <tr>
                                                <td><?php echo htmlspecialchars($values->category, ENT_QUOTES, 'UTF-8'); ?></td>
                                                <td><?php echo htmlspecialchars($values->title, ENT_QUOTES, 'UTF-8'); ?></td>
                                                <td><?php echo htmlspecialchars($values->options, ENT_QUOTES, 'UTF-8'); ?></td>
                                                <td><?php echo anchor("system-admin/features/delete/".$values->id, 'DELETE'); ?></td>
                                                <td><?php echo anchor("system-admin/features/edit/".$values->id, 'EDIT'); ?></td>
                                            </tr>
<?php endforeach;?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                         </div>
                    </div>
                </section>
            </div>
