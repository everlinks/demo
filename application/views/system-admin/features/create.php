<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

?>

            <div class="content-wrapper">
                <section class="content-header">
                    <?php echo $pagetitle; ?>
                    <?php echo $breadcrumb; ?>
                </section>

                <section class="content">
                    <div class="row">
                        <div class="col-md-12">
                             <div class="box">




<div id="result"></div>
<script>

//  $('#categories').on('change',function(){
//     if(this.value){
//         localStorage.setItem('categories',this.value);
//         this.form.submit();
//     }
//  });
//  localStorage.getItem('categories') && 
//  $('#categories').val(localStorage.getItem('categories'))
// });

// $('#category').on('change',function(){
//     $('#result').html($(this).val());   
// });
</script>

                                <div class="box-body">
                                    <?php echo $message;?>

                                    <?php echo form_open(current_url(), array('class' => 'form-horizontal', 'id' => 'form-create_group')); ?>
                               
                                    <div class="form-group">
                                            <?php echo lang('features_category', 'features_category', array('class' => 'col-sm-2 control-label')); ?>
                                            <div class="col-sm-10">
                                            <?php echo form_dropdown($features_category); ?>


                                              
                                                <!-- //  (isset($_POST['categories']) ? $_POST['categories'] : ''),
                                                //  'id="id" onchange="this.form.submit()"'); ?> -->
                                            </div>
                                       </div>
                                      
                                        <div class="form-group">
                                            <?php echo lang('features_name', 'features_name', array('class' => 'col-sm-2 control-label')); ?>
                                            <div class="col-sm-10">
                                                <?php echo form_input($features_name);?>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <?php echo lang('features_description', 'features_description', array('class' => 'col-sm-2 control-label')); ?>
                                            <div class="col-sm-10">
                                                <?php echo form_input($features_description);?>
                                            </div>
                                        </div>



                                        
 <div class="form-group">
                                            <?php echo lang('features_answers', 'features_answers', array('class' => 'col-sm-2 control-label')); ?>
                                            <div class="col-sm-10">
                                                <?php echo form_input($features_answers);?>
                                            </div>
                                        </div>


                                        <div class="form-group">
                                            <div class="col-sm-offset-2 col-sm-10">
                                                <div class="btn-group">
                                                    <?php echo form_button(array('type' => 'submit', 'class' => 'btn btn-primary btn-flat', 'content' => lang('actions_submit'))); ?>
                                                    <?php echo form_button(array('type' => 'reset', 'class' => 'btn btn-warning btn-flat', 'content' => lang('actions_reset'))); ?>
                                                    <?php echo anchor('system-admin/features', lang('actions_cancel'), array('class' => 'btn btn-default btn-flat')); ?>
                                                </div>
                                            </div>
                                        </div>
                                    <?php echo form_close();?>
                                </div>
                            </div>
                         </div>
                    </div>
                </section>
            </div>
          
