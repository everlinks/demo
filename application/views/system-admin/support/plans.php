<?php
defined('BASEPATH') OR exit('No direct script access allowed');

?>

            <div class="content-wrapper">
                <section class="content-header">
                    <?php echo $pagetitle; ?>
                    <?php echo $breadcrumb; ?>
                </section>

                <section class="content">
                    <div class="row">
                        <div class="col-md-12">
                             <div class="box">
                             <div class="box-header with-border">
                                    <h3 class="box-title"><?php echo anchor('system-admin/plans/create', '<i class="fa fa-plus"></i> '. lang('plans_create'), array('class' => 'btn btn-block btn-primary btn-flat')); ?></h3>
                                </div>
                                <div class="box-body">
                                    <table class="table table-striped table-hover">
                                        <thead>
                                            <tr>
                                                <th>Plans Name</th>
                                                <th>Amount</th>
                                                <th>Interval</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
<?php foreach ($categories as $values):?>
                                            <tr>
                                                <td><?php echo htmlspecialchars($values->plan_name, ENT_QUOTES, 'UTF-8'); ?></td>
                                                <td><?php echo htmlspecialchars($values->amount, ENT_QUOTES, 'UTF-8'); ?></td>
                                                <td><?php echo htmlspecialchars($values->intervals, ENT_QUOTES, 'UTF-8'); ?></td>
                                                <td><?php echo anchor("system-admin/support/edit/".$values->id, lang('actions_edit')); ?></td>
                                            </tr>
<?php endforeach;?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                         </div>
                    </div>
                </section>
            </div>
