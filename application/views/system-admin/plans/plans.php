<?php
defined('BASEPATH') OR exit('No direct script access allowed');

?>

            <div class="content-wrapper">
                <section class="content-header">
                    <?php echo $pagetitle; ?>
                    <?php echo $breadcrumb; ?>
                </section>

                <section class="content">
                    <div class="row">
                        <div class="col-md-12">
                             <div class="box">
                             <div class="box-header with-border">
                                    <h3 class="box-title"><?php echo anchor('system-admin/plans/create', '<i class="fa fa-plus"></i> '. lang('plans_create'), array('class' => 'btn btn-block btn-primary btn-flat')); ?></h3>
                                </div>
                                <div class="box-body">
                                    <table class="table table-striped table-hover ">
                                        <thead>
                                            <tr>
                                                <th>Plans Name</th>
                                                <th>Amount</th>
                                                <th>Interval</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
<?php foreach ($categories as $values):?>
                                            <tr>
                                                <td><?php echo htmlspecialchars($values->plan_name, ENT_QUOTES, 'UTF-8'); ?></td>
                                                <td><?php echo htmlspecialchars($values->amount, ENT_QUOTES, 'UTF-8'); ?></td>
                                                <td><?php echo htmlspecialchars($values->intervals, ENT_QUOTES, 'UTF-8'); ?></td>
                                                <td><?php echo anchor("system-admin/plans/edit_plan/".$values->id, lang('actions_edit')); ?>
                                                
                                                &nbsp;&nbsp;
                                                <!-- modal-start -->
<a id="say" class=""  data-toggle="modal" data-target="#basicModal" href="#">Delete</a>
                         

                        <!-- basic modal -->
                        <div class="modal fade" id="basicModal" tabindex="-1" role="dialog" aria-labelledby="basicModal"
                            aria-hidden="true">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h4 class="modal-title" id="myModalLabel">Are you sure?</h4>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                        <p>This plan will delete from your database.</p>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                                       
                                       
                                           <button class="btn btn-default" ><?php echo anchor("system-admin/plans/delete_plan/".$values->id, lang('actions_delete'),array('class'=>'btn-default')); ?></button> 
                                    </div>
                                </div>
                            </div>
                        </div><!-- modal-end -->
                                            </tr>
<?php endforeach;?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                         </div>
                    </div>
                </section>
            </div>
