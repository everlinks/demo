<?php
defined('BASEPATH') OR exit('No direct script access allowed');

?>

<style type="text/css">
.user_profile img{
    height: 120px;
    width: 120px;
    border-radius: 50%;
    border: 5px solid white;
}
</style>

            <div class="content-wrapper">
                <section class="content-header">
                    <?php echo $pagetitle; ?>
                    <?php echo $breadcrumb; ?>
                </section>

                <section class="content">
                    <div class="row">
                        <div class="col-md-6">
                             <div class="box">
                                <!-- <div class="box-header with-border">
                                    <h3 class="box-title">xxxx</h3>
                                </div> -->
                                <div class="d-flex justify-content-center h-100">
                                     <div class="user_profile text-center">
                                         <img src="<?= $user_info[0]->profile_picture; ?>">
                                      </div>
                                 </div>
                                <div class="text-center">
                                   <h5>@<?= $user_info[0]->username; ?></h5>
                                </div>

                                <div class="box-body">
                                    <table class="table table-striped table-hover">
                                        <tbody>
<?php foreach ($user_info as $user):?>
                                            <tr>
                                                <th><?php echo lang('users_ip_address'); ?></th>
                                                <td><?php echo $user->ip_address; ?></td>
                                            </tr>
                                            <!-- <tr>
                                                <th><?php echo lang('users_firstname'); ?></th>
                                                <td><?php echo htmlspecialchars($user->first_name, ENT_QUOTES, 'UTF-8'); ?></td>
                                            </tr> -->
                                            <!-- <tr>
                                                <th><?php echo lang('users_lastname'); ?></th>
                                                <td><?php echo htmlspecialchars($user->last_name, ENT_QUOTES, 'UTF-8'); ?></td>
                                            </tr> -->
                                            <tr>
                                                <th><?php echo lang('users_username'); ?></th>
                                                <td><?php echo htmlspecialchars($user->username, ENT_QUOTES, 'UTF-8'); ?></td>
                                            </tr>
                                            <tr>
                                                <th><?php echo lang('users_email'); ?></th>
                                                <td><?php echo htmlspecialchars($user->email, ENT_QUOTES, 'UTF-8'); ?></td>
                                            </tr>
                                            <tr>
                                                <th><?php echo lang('users_created_on'); ?></th>
                                                <td><?php echo date('d-m-Y', $user->created_on); ?></td>
                                            </tr>
                                            <tr>
                                                <th><?php echo lang('users_last_login'); ?></th>
                                                <td><?php echo ( ! empty($user->last_login)) ? date('d-m-Y', $user->last_login) : NULL; ?></td>
                                            </tr>
                                            <tr>
                                                <th><?php echo lang('users_status'); ?></th>
                                                <td><?php echo ($user->active) ? '<span class="label label-success">'.lang('users_active').'</span>' : '<span class="label label-default">'.lang('users_inactive').'</span>'; ?></td>
                                            </tr>
                                            <!-- <tr>
                                                <th><?php echo lang('users_company'); ?></th>
                                                <td><?php echo htmlspecialchars($user->company, ENT_QUOTES, 'UTF-8'); ?></td>
                                            </tr> -->
                                            <!-- <tr>
                                                <th><?php echo lang('users_phone'); ?></th>
                                                <td><?php echo $user->phone; ?></td>
                                            </tr> -->
                                            <tr>
                                                <th><?php echo lang('users_groups'); ?></th>
                                                <td>
<?php foreach ($user->groups as $group):?>
                                                    <?php echo '<span class="label" style="background:'.$group->bgcolor.'">'.htmlspecialchars($group->name, ENT_QUOTES, 'UTF-8').'</span>'; ?>
<?php endforeach?>
                                                </td>
                                            </tr>


                                            <tr>
                                                <th>Theme Name</th>
                                   
                                                <td>
                                          <?php if (isset($user_theme) && !empty($user_theme)): ?>
                                                     <?= $user_theme; ?>
                                                        <?php endif; ?>  
                                                     </td>
                                            </tr>
                                   




<?php endforeach;?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                         </div>

                        <div class="col-md-6">
                             <div class="box">
                                <div class="box-header with-border">
                                    <h3 class="box-title">User Links</h3>
                                </div>
                                <div class="box-body">

                                    <!-- sdas -->
                                   
        <!-- <?php if (isset($links) && !empty($links)): ?>
            <?php foreach ($links as $key => $value): ?>
                
                    <div class="text-center">
                        <a href="<?= $value['url']; ?>" class="btn btn-dark btn-block" target="_blank"><?= $value['title']; ?>sdsf</a>
                    </div>
                
            <?php endforeach; ?>
        <?php endif; ?> -->
    

                                <table class="table table-striped table-hover">
                                        <tbody>
                                            <tr>
                                                <th>
                                                     <?php echo lang('users_links'); ?>
                                                </th>
                                                <th>
                                                     <?php echo lang('users_status'); ?>
                                                </th>
                                                <th>
                                                     <?php echo lang('users_thumbnail'); ?>
                                                </th>
                                                <th>
                                                     schedule_start_timezone
                                                </th>
                                            </tr>
<?php if (isset($links) && !empty($links)): ?>
<?php foreach ($links as $key => $value):?>
                        
                                            <tr>
                                                <td>
                                                    <a href="//<?= $value['url']; ?>" class="" target="_blank"><?= $value['title']; ?></a>
                                                </td>

                                                <td><?php echo ($value['active']) ? '<span class="label label-success">'.lang('links_active').'</span>' : '<span class="label label-default">'.lang('links_inactive').'</span>'; ?>
                                                </td>

                                                <td><? // $value['thumbnail']; ?></td>
                                                <td><? // $value['schedule_start_timezone']; ?></td>

                                            </tr>
                                            
<?php endforeach?>
<?php endif; ?>
                                            
                                            
                                        </tbody>
                                    </table>




                                </div>
                            </div>
                        </div>
                    </div>

                </section>
            </div>
