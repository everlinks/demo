<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
    <!-- START SLIDER - display none -->
    <div id="header-slider" class="header-slider color-gredient" style="display:none;">
        <ul class="seq-canvas">
            <!-- SLIDE 1 -->
            <li class="step1 slides">
                <!-- MAIN IMAGE -->
                <div class="bg-img" style="background-image: url(<?php echo base_url($frameworks_dir . '/everlink/images/header-slide-1.png'); ?>)"></div>
                <!-- Caption -->
                <div class="slide-caption">
                    <!-- H1 Heading -->
                    <h1>Smart</h1>
                    <!-- H2 Heading -->
                    <h2>Ready to showcase your app</h2>
                    <!-- Paragraph -->
                    <p>
                        Lorem Ipsum is simply dummy text of the printing and typesetting indus orem Ipsum has been the industry's standard dummy text ever since the when an own printer took a galley
                    </p>
                    <!-- Buttons -->
                    <a href="#" class="slide-button slide-button-active">Download</a>
                    <!-- Button -->
                    <a href="#" class="slide-button">Learn more</a>
                </div>
            </li>
            <!-- SLIDE 2 -->
            <li class="step2 slides">
                <!-- MAIN IMAGE -->
                <div class="bg-img" style="background-image: url(<?php echo base_url($frameworks_dir . '/everlink/images/header-slide-2.png'); ?>)"></div>
                <!-- Caption -->
                <div class="slide-caption">
                    <!-- H1 Heading -->
                    <h1>Smart</h1>
                    <!-- H2 Heading -->
                    <h2>Ready to showcase your app</h2>
                    <!-- Paragraph -->
                    <p>
                        Lorem Ipsum is simply dummy text of the printing and typesetting indus orem Ipsum has been the industry's standard dummy text ever since the when an own printer took a galley
                    </p>
                    <!-- Buttons -->
                    <a href="#" class="slide-button slide-button-active">Download </a>
                    <!-- Button -->
                    <a href="#" class="slide-button">Learn more</a>
                </div>
            </li>
            <!-- SLIDE 3 -->
            <li class="step3 slides">
                <!-- MAIN IMAGE -->
                <div class="bg-img" style="background-image: url(<?php echo base_url($frameworks_dir . '/everlink/images/header-slide-1.png'); ?>)"></div>
                <!-- Caption -->
                <div class="slide-caption">
                    <!-- H1 Heading -->
                    <h1>Smart</h1>
                    <!-- H2 Heading -->
                    <h2>Ready to showcase your app</h2>
                    <!-- Paragraph -->
                    <p>
                        Lorem Ipsum is simply dummy text of the printing and typesetting indus orem Ipsum has been the industry's standard dummy text ever since the when an own printer took a galley
                    </p>
                    <!-- Buttons -->
                    <a href="#" class="slide-button slide-button-active">Download </a>
                    <!-- Button -->
                    <a href="#" class="slide-button">Learn more</a>
                </div>
            </li>
            <!-- SLIDE 4 -->
            <li class="step4 slides">
                <!-- MAIN IMAGE -->
                <div class="bg-img" style="background-image: url(<?php echo base_url($frameworks_dir . '/everlink/images/header-slide-2.png'); ?>"></div>
                <!-- Caption -->
                <div class="slide-caption">
                    <!-- H1 Heading -->
                    <h1>Smart</h1>
                    <!-- H2 Heading -->
                    <h2>Ready to showcase your app</h2>
                    <!-- Paragraph -->
                    <p>
                        Lorem Ipsum is simply dummy text of the printing and typesetting indus orem Ipsum has been the industry's standard dummy text ever since the when an own printer took a galley
                    </p>
                    <!-- Buttons -->
                    <a href="#" class="slide-button slide-button-active">Download now</a>
                    <!-- Button -->
                    <a href="#" class="slide-button">Learn more</a>
                </div>
            </li>
        </ul>
        <!-- PAGINATION -->
        <ul class="seq-pagination">
            <li>01</li>
            <li>02</li>
            <li>03</li>
            <li>04</li>
        </ul>
        <!-- NAVIGATION -->
        <button type="button" class="seq-next"><span class="icon-play"></span></button>
        <button type="button" class="seq-prev"><span class="icon-play-flip"></span></button>
    </div>
    </header>
    <!-- END HEADER -->
    </section>
    <!-- LEFT CONTENT & RIGHT IMAGE -->
    <section id="left-content-section" class="site-amazing-features left-heading two-colom-section">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-6">
                    <div class="half-colom-left-section">
                        <!-- H1 HEADING -->
                        <div class="left-heading-icon wow fadeInLeft" data-wow-duration="2s"> <img src="<?php echo base_url($frameworks_dir . '/everlink/images/design-icon.png'); ?>" alt="designicon">
                        </div>
                        <h1>ONE URL & MULTIPLE LINKS </h1>
                        <div class="two-colom-content">
                            <p>
                                Offer multiple choices to your followers when they click on your Bio link. Share all your Social Media profiles and important links with just one url. Got a new sponsored campaign from a client? Just add their link and put it on top of the list. You can also choose to temporary disable all other links!
                            </p>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-6">
                    <div class="half-colom-right-section wow fadeInDown" data-wow-duration="2s">
                        <img src="<?php echo base_url($frameworks_dir . '/everlink/images/right-image1.png'); ?>" alt="rightimage">
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- LEFT IMAGE & RIGHT  CONTENT -->
    <section id="right-content-section" class="site-amazing-features left-heading two-colom-section">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-6 col-md-push-6">
                    <div class="half-colom-left-section half-colom-padding">
                        <!-- H1 HEADING -->
                        <div class="left-heading-icon wow fadeInRight" data-wow-duration="2s"> <img src="<?php echo base_url($frameworks_dir . '/everlink/images/perfomance-icon.png'); ?>" alt="perfomanceicon">
                        </div>
                        <h1> QUICK & EASY TO USE </h1>
                        <div class="two-colom-content">
                            <p>
                                Log in via Instagram, add your links and retrieve your unique URL. It literally takes 1 minute to get started. Open IG, go to "Edit your Instagram profile", paste your new URL and you're done. If you ever need to change your links, come back to LNK.bio and edit them without the need to change your URL on your IG profile.
                            </p>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-6 col-md-pull-6 ">
                    <div class="half-colom-right-section wow fadeInLeft" data-wow-duration="2s">
                        <img src="<?php echo base_url($frameworks_dir . '/everlink/images/left-image1.png'); ?>" alt="leftimage">
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- MORE FEATURES -->
    <div class="site-more-features section-blue color-gredient" id="site-more-features">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <!-- heading -->
                    <h1>FEATURES</h1>
                    <div class="col-xs-12 col-sm-12 col-md-2 col-lg-2 ">
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8 ">
                        <div class="heading-text">
                            <p>
                                a lot of amazing & cool features
                            </p>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-2 col-lg-2 ">
                    </div>
                </div>
                <!-- clearfix -->
                <div class="clearfix"></div>
                <div class="col-xs-12 col-sm-12 col-md-4 col-md-push-4">
                    <!-- mobile image -->
                    <figure class="featured-img wow fadeInDown" data-wow-duration="2s">
                        <img src="<?php echo base_url($frameworks_dir . '/everlink/images/feature-mobile.png'); ?>" alt="Image">
                    </figure>
                    <!-- end -->
                </div>
                <div class="col-xs-12 col-sm-12 col-md-4 col-md-pull-4">
                    <!-- feature 1 -->
                    <div class="feature align-right">
                        <h5>PERSONAL URL</h5>
                        <p>Create your personal Url and place it in your Instagram Bio.</p>
                        <figure>
                            <span><i class="fa fa-user"></i></span>
                        </figure>
                    </div>
                    <!-- feature 2 -->
                    <div class="feature align-right move">
                        <h5>UNLIMITED LINKS</h5>
                        <p>Add as many links as you wish, change them as often as you want.</p>
                        <figure>
                            <span><i class="fa fa-link"></i></span>
                        </figure>
                    </div>
                    <!-- feature 3 -->
                    <div class="feature align-right">
                        <h5>ELEGANT AND PERFECT</h5>
                        <p>With a cutting-edge interface, followers clicking on your Url will experience a great visual.</p>
                        <figure>
                            <span><i class="fa fa-trophy"></i></span>
                        </figure>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-4">
                    <!-- feature 1 -->
                    <div class="feature align-left">
                        <h5>SAFE</h5>
                        <p>We use the official Instagram API. We never ask for your password.</p>
                        <figure>
                            <span><i class="fa fa-lock"></i></span>
                        </figure>
                    </div>
                    <!-- feature 2 -->
                    <div class="feature align-left move">
                        <h5>SOCIAL FIRST</h5>
                        <p>Cross-link all your social profiles and optimize the engagement across your different channels.</p>
                        <figure>
                            <span><i class="fa fa-users"></i></span>
                        </figure>
                    </div>
                    <!-- feature 3 -->
                    <div class="feature align-left">
                        <h5>WEB-BASED</h5>
                        <p>No need to install anything, just access anytime via browser from any device.</p>
                        <figure>
                            <span><i class="fa fa-chrome"></i></span>
                        </figure>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- HOW IT WORKS -->
    <section class="site-how-it-works section-grey" id="how-it-works">
        <div class="container-fluid wide">
            <div class="row">
                <div class="col-xs-12">
                    <!-- H1 HEADING -->
                    <h1>How it Works?</h1>
                    <!-- START SLIDER -->
                    <!-- Slider main container -->
                    <div class="container">
                        <div class="row">
                            <div class="col-xs-12 col-sm-11 ">
                                <div class="swiper-container" id="how-it-works-slider">
                                    <!-- Additional required wrapper -->
                                    <ul class="swiper-wrapper">
                                        <!-- Slides -->
                                        <!-- SLIDE 1 -->
                                        <li class="swiper-slide">
                                            <div class="row">
                                                <div class="col-xs-12 col-sm-12 col-md-6">
                                                    <!-- H5 HEADING -->
                                                    <h3>First tab title - How to install ?</h3>
                                                    <!-- PARAGRAPH -->
                                                    <p>Lorem Ipsum is simply dummy text of the printing and typese ing industry Lorem Ipsum has been the industry's standard dummy text Lorem is simply dummy </p>
                                                    <!-- BOX -->
                                                    <div class="section first">
                                                        <!-- FIGURE -->
                                                        <figure><span class="icon-config"></span></figure>
                                                        <!-- H4 HEADING -->
                                                        <h4>Confing your mobile</h4>
                                                        <!-- PARAGRAPH -->
                                                        <p> Lorem Ipsum is simply dummy text of the printing and typing indus try Lorem Ipsum has been the indus. </p>
                                                    </div>
                                                    <!-- BOX -->
                                                    <div class="section">
                                                        <!-- FIGURE -->
                                                        <figure><span class="icon-refresh"></span></figure>
                                                        <!-- H4 HEADING -->
                                                        <h4>Refresh setup</h4>
                                                        <!-- PARAGRAPH -->
                                                        <p>Simply dummy text of the printing and typing indus try Lorem Ipsum has been the indus try's standard.</p>
                                                    </div>
                                                    <!-- BOX -->
                                                    <div class="section">
                                                        <!-- FIGURE -->
                                                        <figure><span class="icon-comment"></span></figure>
                                                        <!-- H4 HEADING -->
                                                        <h4>Chat with your love</h4>
                                                        <!-- PARAGRAPH -->
                                                        <p>Ipsum is simply dummy text of the printing and typing indus try Lorem Ipsum has been the indus standard.</p>
                                                    </div>
                                                </div>
                                                <div class="col-xs-12 col-sm-12 col-md-6">
                                                    <!-- DEVICE -->
                                                    <figure class="device">
                                                        <img src="<?php echo base_url($frameworks_dir . '/everlink/images/how-it-works-device.png'); ?>" alt="Device">
                                                    </figure>
                                                </div>
                                            </div>
                                        </li>
                                        <!-- SLIDE 2 -->
                                        <li class="swiper-slide">
                                            <div class="row">
                                                <div class="col-xs-12 col-sm-12 col-md-6">
                                                    <!-- H5 HEADING -->
                                                    <h3>First tab title - How to install ?</h3>
                                                    <!-- PARAGRAPH -->
                                                    <p>Lorem Ipsum is simply dummy text of the printing and typese ing industry Lorem Ipsum has been the industry's standard dummy text Lorem is simply dummy </p>
                                                    <!-- BOX -->
                                                    <div class="section first">
                                                        <!-- FIGURE -->
                                                        <figure><span class="icon-config"></span></figure>
                                                        <!-- H4 HEADING -->
                                                        <h4>Confing your mobile</h4>
                                                        <!-- PARAGRAPH -->
                                                        <p> Lorem Ipsum is simply dummy text of the printing and typing indus try Lorem Ipsum has been the indus. </p>
                                                    </div>
                                                    <!-- BOX -->
                                                    <div class="section">
                                                        <!-- FIGURE -->
                                                        <figure><span class="icon-refresh"></span></figure>
                                                        <!-- H4 HEADING -->
                                                        <h4>Refresh setup</h4>
                                                        <!-- PARAGRAPH -->
                                                        <p>Simply dummy text of the printing and typing indus try Lorem Ipsum has been the indus try's standard.</p>
                                                    </div>
                                                    <!-- BOX -->
                                                    <div class="section">
                                                        <!-- FIGURE -->
                                                        <figure><span class="icon-comment"></span></figure>
                                                        <!-- H4 HEADING -->
                                                        <h4>Chat with your love</h4>
                                                        <!-- PARAGRAPH -->
                                                        <p>Ipsum is simply dummy text of the printing and typing indus try Lorem Ipsum has been the indus standard.</p>
                                                    </div>
                                                </div>
                                                <div class="col-xs-12 col-sm-12 col-md-6">
                                                    <!-- DEVICE -->
                                                    <figure class="device">
                                                        <img src="<?php echo base_url($frameworks_dir . '/everlink/images/how-it-works-screen2.png'); ?>" alt="Device">
                                                    </figure>
                                                </div>
                                            </div>
                                        </li>
                                        <!-- SLIDE 3 -->
                                        <li class="swiper-slide">
                                            <div class="row">
                                                <div class="col-xs-12 col-sm-12 col-md-6">
                                                    <!-- H5 HEADING -->
                                                    <h3>First tab title - How to install ?</h3>
                                                    <!-- PARAGRAPH -->
                                                    <p>Lorem Ipsum is simply dummy text of the printing and typese ing industry Lorem Ipsum has been the industry's standard dummy text Lorem is simply dummy </p>
                                                    <!-- BOX -->
                                                    <div class="section first">
                                                        <!-- FIGURE -->
                                                        <figure><span class="icon-config"></span></figure>
                                                        <!-- H4 HEADING -->
                                                        <h4>Confing your mobile</h4>
                                                        <!-- PARAGRAPH -->
                                                        <p> Lorem Ipsum is simply dummy text of the printing and typing indus try Lorem Ipsum has been the indus. </p>
                                                    </div>
                                                    <!-- BOX -->
                                                    <div class="section">
                                                        <!-- FIGURE -->
                                                        <figure><span class="icon-refresh"></span></figure>
                                                        <!-- H4 HEADING -->
                                                        <h4>Refresh setup</h4>
                                                        <!-- PARAGRAPH -->
                                                        <p>Simply dummy text of the printing and typing indus try Lorem Ipsum has been the indus try's standard.</p>
                                                    </div>
                                                    <!-- BOX -->
                                                    <div class="section">
                                                        <!-- FIGURE -->
                                                        <figure><span class="icon-comment"></span></figure>
                                                        <!-- H4 HEADING -->
                                                        <h4>Chat with your love</h4>
                                                        <!-- PARAGRAPH -->
                                                        <p>Ipsum is simply dummy text of the printing and typing indus try Lorem Ipsum has been the indus standard.</p>
                                                    </div>
                                                </div>
                                                <div class="col-xs-12 col-sm-12 col-md-6">
                                                    <!-- DEVICE -->
                                                    <figure class="device">
                                                        <img src="<?php echo base_url($frameworks_dir . '/everlink/images/how-it-works-device.png'); ?>" alt="Device">
                                                    </figure>
                                                </div>
                                            </div>
                                        </li>
                                        <!-- SLIDE 4 -->
                                        <li class="swiper-slide">
                                            <div class="row">
                                                <div class="col-xs-12 col-sm-12 col-md-6">
                                                    <!-- H5 HEADING -->
                                                    <h3>First tab title - How to install ?</h3>
                                                    <!-- PARAGRAPH -->
                                                    <p>Lorem Ipsum is simply dummy text of the printing and typese ing industry Lorem Ipsum has been the industry's standard dummy text Lorem is simply dummy </p>
                                                    <!-- BOX -->
                                                    <div class="section first">
                                                        <!-- FIGURE -->
                                                        <figure><span class="icon-config"></span></figure>
                                                        <!-- H4 HEADING -->
                                                        <h4>Confing your mobile</h4>
                                                        <!-- PARAGRAPH -->
                                                        <p> Lorem Ipsum is simply dummy text of the printing and typing indus try Lorem Ipsum has been the indus. </p>
                                                    </div>
                                                    <!-- BOX -->
                                                    <div class="section">
                                                        <!-- FIGURE -->
                                                        <figure><span class="icon-refresh"></span></figure>
                                                        <!-- H4 HEADING -->
                                                        <h4>Refresh setup</h4>
                                                        <!-- PARAGRAPH -->
                                                        <p>Simply dummy text of the printing and typing indus try Lorem Ipsum has been the indus try's standard.</p>
                                                    </div>
                                                    <!-- BOX -->
                                                    <div class="section">
                                                        <!-- FIGURE -->
                                                        <figure><span class="icon-comment"></span></figure>
                                                        <!-- H4 HEADING -->
                                                        <h4>Chat with your love</h4>
                                                        <!-- PARAGRAPH -->
                                                        <p>Ipsum is simply dummy text of the printing and typing indus try Lorem Ipsum has been the indus standard.</p>
                                                    </div>
                                                </div>
                                                <div class="col-xs-12 col-sm-12 col-md-6">
                                                    <!-- DEVICE -->
                                                    <figure class="device">
                                                        <img src="<?php echo base_url($frameworks_dir . '/everlink/images/how-it-works-screen2.png'); ?>" alt="Device">
                                                    </figure>
                                                </div>
                                            </div>
                                        </li>
                                        <!-- SLIDE 5 -->
                                        <li class="swiper-slide">
                                            <div class="row">
                                                <div class="col-xs-12 col-sm-12 col-md-6">
                                                    <!-- H5 HEADING -->
                                                    <h3>First tab title - How to install ?</h3>
                                                    <!-- PARAGRAPH -->
                                                    <p>Lorem Ipsum is simply dummy text of the printing and typese ing industry Lorem Ipsum has been the industry's standard dummy text Lorem is simply dummy </p>
                                                    <!-- BOX -->
                                                    <div class="section first">
                                                        <!-- FIGURE -->
                                                        <figure><span class="icon-config"></span></figure>
                                                        <!-- H4 HEADING -->
                                                        <h4>Confing your mobile</h4>
                                                        <!-- PARAGRAPH -->
                                                        <p> Lorem Ipsum is simply dummy text of the printing and typing indus try Lorem Ipsum has been the indus. </p>
                                                    </div>
                                                    <!-- BOX -->
                                                    <div class="section">
                                                        <!-- FIGURE -->
                                                        <figure><span class="icon-refresh"></span></figure>
                                                        <!-- H4 HEADING -->
                                                        <h4>Refresh setup</h4>
                                                        <!-- PARAGRAPH -->
                                                        <p>Simply dummy text of the printing and typing indus try Lorem Ipsum has been the indus try's standard.</p>
                                                    </div>
                                                    <!-- BOX -->
                                                    <div class="section">
                                                        <!-- FIGURE -->
                                                        <figure><span class="icon-comment"></span></figure>
                                                        <!-- H4 HEADING -->
                                                        <h4>Chat with your love</h4>
                                                        <!-- PARAGRAPH -->
                                                        <p>Ipsum is simply dummy text of the printing and typing indus try Lorem Ipsum has been the indus standard.</p>
                                                    </div>
                                                </div>
                                                <div class="col-xs-12 col-sm-12 col-md-6">
                                                    <!-- DEVICE -->
                                                    <figure class="device">
                                                        <img src="<?php echo base_url($frameworks_dir . '/everlink/images/how-it-works-device.png'); ?>" alt="Device">
                                                    </figure>
                                                </div>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- If we need navigation buttons -->
                    <div id="how-it-works-prev" class="swiper-button-prev"><i class="fa fa-angle-up hidden-xs"></i><span class="icon-play-flip visible-xs"></span></div>
                    <div id="how-it-works-next" class="swiper-button-next"><i class="fa fa-angle-down hidden-xs"></i><span class="icon-play visible-xs"></span></div>
                    <!-- If we need pagination -->
                    <div id="how-it-works-paging" class="swiper-pagination" data-icons='[
                         "fa fa-mobile",
                         "icon-config",
                         "icon-help",
                         "fa fa-shopping-basket",
                         "icon-unlock",
                         "icon-shopbag"
                         ]'></div>
                </div>
            </div>
        </div>
    </section>
    <!-- DOWNLOAD -->
    <section id="site-download" class="site-download section-blue">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <!-- H1 HEADING -->
                    <h1>GET STARTED</h1>
                    <!-- DOWNLOAD ICONS -->
                    <section class="site-download-icons">
                        <div class="align-center">
                            <!-- BUTTON 2 -->
                            <a href="#" class="app-download-icons wow fadeInDown" data-wow-duration="2s">
                                <!-- FIGURE -->
                                <figure><i class="fa fa-instagram"></i></figure>
                                <!-- h6 heading -->
                                <!-- h5 -->
                                <h5>SIGN UP WITH INSTAGRAM</h5>
                            </a>
                        </div>
                    </section>
                </div>
            </div>
        </div>
    </section>
    <!-- QUICK VIEW -->
    <section class="site-quick-view section-white" id="site-quick-view">
        <div class="container-fluid wide">
            <div class="row">
                <div class="col-sm-12">
                    <!-- heading -->
                    <h1>Quick View</h1>
                    <!-- Slider main container -->
                    <div class="container">
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="swiper-container" id="quick-view-slider">
                                    <!-- Additional required wrapper -->
                                    <ul class="swiper-wrapper">
                                        <!-- slide 1 -->
                                        <li class="swiper-slide">
                                            <!-- box URL -->
                                            <a href="#" class="box">
                                                <!-- image -->
                                                <figure>
                                                    <img src="<?php echo base_url($frameworks_dir . '/everlink/images/quick-view-1.png'); ?>" alt="Image">
                                                </figure>
                                            </a>
                                        </li>
                                        <!-- slide 2 -->
                                        <li class="swiper-slide">
                                            <!-- box URL -->
                                            <a href="#" class="box">
                                                <!-- image -->
                                                <figure>
                                                    <img src="<?php echo base_url($frameworks_dir . '/everlink/images/quick-view-2.png'); ?>" alt="Image">
                                                </figure>
                                            </a>
                                        </li>
                                        <!-- slide 3 -->
                                        <li class="swiper-slide">
                                            <!-- box URL -->
                                            <a href="#" class="box">
                                                <!-- image -->
                                                <figure>
                                                    <img src="<?php echo base_url($frameworks_dir . '/everlink/images/quick-view-3.png'); ?>" alt="Image">
                                                </figure>
                                            </a>
                                        </li>
                                        <!-- slide 4 -->
                                        <li class="swiper-slide">
                                            <!-- box URL -->
                                            <a href="#" class="box">
                                                <!-- image -->
                                                <figure>
                                                    <img src="<?php echo base_url($frameworks_dir . '/everlink/images/quick-view-4.png'); ?>" alt="Image">
                                                </figure>
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- If we need navigation buttons -->
                    <div id="quick-view-prev" class="swiper-button-prev"><span class="icon-play-flip"></span></div>
                    <div id="quick-view-next" class="swiper-button-next"><span class="icon-play"></span></div>
                    <!-- If we need pagination -->
                    <div id="quick-view-paging" class="swiper-pagination visible-xs"></div>
                </div>
            </div>
        </div>
    </section>
    <!-- ACCORDION -->
    <section id="site-accordion" class="site-accordion section-grey left-heading ">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-md-6">
                    <div class="accordian-main">
                        <h1> FAQs</h1>
                        <!-- accordion -->
                        <div class="panel-group" id="accordion">
                            <!-- section 1 -->
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                        <a data-toggle="collapse" data-parent="#accordion" href="#collapse1" class="collapsed">
                                            Is the Free version really Free?
                                            <span>icon</span>
                                        </a>
                                    </h4>
                                </div>
                                <div id="collapse1" class="panel-collapse collapse">
                                    <div class="panel-body-container">
                                        <div class="panel-body">
                                            Yes, free and unlimited. Just log in via Instagram and have your unique Url. No fuss
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- section 2 -->
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title active">
                                        <a data-toggle="collapse" data-parent="#accordion" href="#collapse2">
                                            How can I update my Bio Link?
                                            <span>icon</span>
                                        </a>
                                    </h4>
                                </div>
                                <div id="collapse2" class="panel-collapse collapse in">
                                    <div class="panel-body-container">
                                        <div class="panel-body">
                                            Just open the IG app, go to your profile, edit and place the url we have generated in your link. It's that simple.
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- section 3 -->
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                        <a data-toggle="collapse" data-parent="#accordion" href="#collapse3" class="collapsed">
                                            What if I need help?
                                            <span>icon</span>
                                        </a>
                                    </h4>
                                </div>
                                <div id="collapse3" class="panel-collapse collapse">
                                    <div class="panel-body-container">
                                        <div class="panel-body">
                                            We do not guarantee support for the free version, but drop us an email to info@lnk.bio and we'll do our best.
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- section 4 -->
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                        <a data-toggle="collapse" data-parent="#accordion" href="#collapse4" class="collapsed">
                                          What payment methods do you accept?
                                            <span>icon</span>
                                        </a>
                                    </h4>
                                </div>
                                <div id="collapse4" class="panel-collapse collapse">
                                    <div class="panel-body-container">
                                        <div class="panel-body">
                                            We accept payment via Stripe. You can also pay with any credit card, without the need to signup via Stripe.
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- section 5 -->
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                        <a data-toggle="collapse" data-parent="#accordion" href="#collapse5" class="collapsed">
                                          Should I put this link also in my IG Posts?
                                            <span>icon</span>
                                        </a>
                                    </h4>
                                </div>
                                <div id="collapse5" class="panel-collapse collapse">
                                    <div class="panel-body-container">
                                        <div class="panel-body">
                                            No! That's against IG's T&C. It ruins your followers' experience and is useless since you can not "tap" on it.
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- section 6 -->
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                        <a data-toggle="collapse" data-parent="#accordion" href="#collapse5" class="collapsed">
                                          Can I remove links?
                                            <span>icon</span>
                                        </a>
                                    </h4>
                                </div>
                                <div id="collapse5" class="panel-collapse collapse">
                                    <div class="panel-body-container">
                                        <div class="panel-body">
                                            Of course! At any time just log in to lnk.bio and remove/edit/add everything you want!
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- end -->
                    </div>
                </div>
                <div class="col-xs-12 col-md-6">
                    <!-- mobile image -->
                    <figure class="accordion-img wow fadeIn" data-wow-duration="3s">
                        <img src="<?php echo base_url($frameworks_dir . '/everlink/images/01.png'); ?>" alt="Image">
                    </figure>
                    <!-- end -->
                </div>
            </div>
        </div>
    </section>
    <!-- PACKAGES -->
    <section class="site-packages section-blue " id="site-packages">
        <div class="container-fluid">
            <div class="row">
                <div class="color-gredient">
                </div>
            </div>
            <div class="site-packages-inner">
                <div class="row no-gutter">
                    <div class="col-sm-12">
                        <!-- h1 heading -->
                        <h1>Our Best Pricing</h1>
                    </div>
                    <div class="container">
                        <div class="col-xs-12 col-sm-6 col-md-push-6">
                            <!-- plan active -->
                            <div class="box active wow fadeInDown" data-wow-duration="2s">
                                <div class="price-box-header">
                                    <!-- heading -->
                                    <h4 class="heading">Pro</h4>
                                    <h4 class="">For users who want their links to work harder.</h4>
                                    <!-- price -->
                                    <div class="price"><span>$</span>6<span>/pm</span></div>
                                </div>
                                <!-- options -->
                                <ul class="options">
                                    <span>Includes everything in Free plus all this:</span>
                                    <li><span><i class="fa fa-check"></i>Get help quickly with priority support.</span></li>
                                    <li><span><i class="fa fa-check"></i>Capture your visitors email addresses with the email / newsletter signup integration.</span></li>
                                    <li><span><i class="fa fa-check"></i>More Linktree themes.</span></li>
                                    <li><span><i class="fa fa-check"></i>Remove Linktree branding.</span></li>
                                    <li><span><i class="fa fa-check"></i>See a day-by-day breakdown of link traffic.</span></li>
                                    <li><span><i class="fa fa-check"></i>Complete customization of your linktree colors, button styles and fonts.</span></li>
                                    <li><span><i class="fa fa-check"></i>Highlight your most important links with priority links.</span></li>
                                </ul>
                                <!-- button -->
                                <a href="#">Order Now</a>
                            </div>
                            <!-- end -->
                        </div>
                        <div class="col-xs-12 col-sm-6 col-md-pull-6">
                            <!-- plan -->
                            <div class="box left wow fadeInDown" data-wow-duration="1s">
                                <div class="price-box-header">
                                    <!-- heading -->
                                    <h4 class="heading">Free</h4>
                                    <h4 class="   ">All the basics, for as long as you like.</h4>
                                    <!-- price -->
                                    <div class="price"><span>$</span>0</div>
                                </div>
                                <!-- options -->
                                <ul class="options">
                                    <span>Free includes all of the basic features to get you started:</span>
                                    <li><span><i class="fa fa-check"></i>Get unlimited links on your linktree.</span></li>
                                    <li><span><i class="fa fa-check"></i>See the total number of times each link has been clicked.</span></li>
                                    <li><span><i class="fa fa-check"></i> Pick from a selection of linktree themes.</span></li>
                                </ul>
                                <!-- button -->
                                <a href="#">Order Now</a>
                            </div>
                            <!-- end -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- SUBSCRIBE -->
    <section id="site-subscribe" class="site-subscribe section-white">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 wow flipInX" data-wow-duration="2s">
                    <!-- H1 HEADING -->
                    <h1>Subscribe us</h1>
                    <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                        <div class="main-heading-text">
                            <p>Subscribe to our Newsletter to get daily updates by us!! Lorem Ipsum is simply dummy text of the printing and typeseing
                            </p>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
                    </div>
                    <!-- BOX -->
                    <div class="site-box">
                        <div class="row">
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-3">
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
                                <!-- FORM -->
                                <form action="#" method="post" class="site-form">
                                    <label>
                                        <input type="email" placeholder="enter your email id here" required="required">
                                    </label>
                                    <input type="submit" value="SUBMIT">
                                </form>
                                <!-- END FORM -->
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-3">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- TWITTER -->
    <section class="site-twitter section-blue" id="site-twitter" style="display:none">
        <div class="container-fluid wide">
            <div class="row">
                <div class="col-xs-12">
                    <!--  H1 HEADING-->
                    <h1 class="heading-inverse">Tweet @ <strong>Start</strong></h1>
                    <!-- Slider main container -->
                    <div class="container">
                        <div class="row">
                            <div class="col-xs-12 col-sm-10 col-sm-offset-1">
                                <div class="swiper-container" id="tweet-slider">
                                    <!-- Tweets -->
                                    <ul class="swiper-wrapper tweet"></ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- If we need navigation buttons -->
                    <div id="tweet-prev" class="swiper-button-prev hidden-lg"><span class="icon-play-flip"></span></div>
                    <div id="tweet-next" class="swiper-button-next hidden-lg"><span class="icon-play"></span></div>
                    <!-- If we need pagination -->
                    <div id="tweet-paging" class="swiper-pagination"></div>
                </div>
            </div>
        </div>
    </section>
    <!-- QUICK SUPPORT -->
    <section id="quick-support" class="site-quick-support section-white">
        <div class="container">
            <div class="contact-box color-gredient">
                <div class="col-xs-12">
                    <div class="box">
                        <!-- INFO -->
                        <div class="col-xs-12 col-sm-12 col-md-5 col-lg-5">
                            <div class="site-info">
                                <h5> Contact Info</h5>
                                <p>Lorem Ipsum is simply dummy text of the printing and typesetting indus orem Ipsum has been the industrys.
                                </p>
                                <!-- BOX -->
                                <a href="tel:+911234567890" class="site-box-row">
                                    <!-- ICON -->
                                    <h6><i class="fa fa-phone"></i> Call us </h6>
                                    <!-- PARAGRAPH -->
                                    <p>+91 123 456 7890</p>
                                </a>
                                <!-- BOX -->
                                <a href="mailto:support@gmail.com" class="site-box-row last">
                                    <!-- ICON -->
                                    <h6><i class="fa fa-envelope"></i> Email us</h6>
                                    <!-- Mail -->
                                    <p>support@gmail.com</p>
                                </a>
                                <!-- BOX -->
                                <a target="_blank" href="http://maps.google.com/?q=Location,125BusinessEvenue,Huston,USA" class="site-box-row">
                                    <!-- ICON -->
                                    <h6><i class="fa fa-map-marker"></i> Location</h6>
                                    <!-- ADDRESS -->
                                    <address>Location, 125 Business Evenue, Huston, USA</address>
                                </a>
                            </div>
                        </div>
                        <!-- CONTACT FORM -->
                        <div class="col-xs-12 col-sm-12 col-md-7 col-lg-7">
                            <div class="site-info form">
                                <h5> Send us message! </h5>
                                <form action="#" method="post" class="site-contact-form" id="myForm">
                                    <label>
                                        <input class="app-btn value-clear" type="text" name="app_name" placeholder="Name" required="required">
                                    </label>
                                    <label>
                                        <input class="app-btn value-clear" type="email" name="app_email" placeholder="Email" required="required">
                                    </label>
                                    <label>
                                        <input class="app-btn value-clear" type="tel" name="app_phone" placeholder="Phone" required="required">
                                    </label>
                                    <label>
                                        <input class="app-btn value-clear" type="url" name="app_website" placeholder="Website">
                                    </label>
                                    <label class="last">
                                        <textarea class="app-btn value-clear" name="app_message" placeholder="Message" required></textarea>
                                    </label>
                                    <label class="move">
                                        <button id="form-submit-btn" class="app-btn" type="submit">Submit <i class="fa fa-spin fa-spinner"></i></button>
                                    </label>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- SUBSCRIBE -->
    <section class="site-subscribe" style="display:none;">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <!-- BOX -->
                    <div class="site-box">
                        <div class="row">
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6 no-space">
                                <!-- PARAGRAPH -->
                                <p>Subscribe to our Newsletter to get first Gift voucher by Start</p>
                                <!-- END PARAGRAPH -->
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6 no-space">
                                <!-- FORM -->
                                <form action="#" method="post" class="site-form">
                                    <label>
                                        <input type="email" placeholder="" required="required">
                                    </label>
                                    <input type="submit" value="SUBMIT">
                                </form>
                                <!-- END FORM -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>