<!-- FOOTER -->
<footer class="site-footer section-blue">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">

                <!-- END LOGO -->

                <!-- SOCIAL ICONS -->
                <div class="site-social-icons">
                    <a href="#"><i class="fa fa-facebook"></i></a>
                    <a href="#"><i class="fa fa-twitter"></i></a>
                    <a href="#"><i class="fa fa-google-plus"></i></a>
                    <a href="#"><i class="fa fa-pinterest"></i></a>
                    <a href="#"><i class="fa fa-youtube"></i></a>
                </div>
                <!-- END SOCIAL ICONS -->

                <!-- COPYRIGHT -->
                <div class="site-copyright">
                    <p>© Copyright 2018</p>
                </div>

            </div>
        </div>
    </div>
</footer>

</div>
<!-- END WRAPPER -->

<!--SCRIPTS -->
<script src="<?php echo base_url($frameworks_dir . '/everlink/js/jquery-1.12.4.min.js'); ?>"></script>                         <!-- JQuery -->
<script src="<?php echo base_url($frameworks_dir . '/everlink/js/loadingoverlay.min.js'); ?>"></script>                        <!-- Preloader -->
<script src="<?php echo base_url($frameworks_dir . '/everlink/js/swiper.jquery.min.js'); ?>"></script>                         <!-- Carousel slider -->
<script src="<?php echo base_url($frameworks_dir . '/everlink/js/jquery.mCustomScrollbar.concat.min.js'); ?>"></script>        <!-- Custom scroll bar -->
<script src="<?php echo base_url($frameworks_dir . '/everlink/js/modernizr-custom.min.js'); ?>"></script>                      <!-- Modernizr -->
<script src="<?php echo base_url($frameworks_dir . '/everlink/js/imagesloaded.pkgd.min.js'); ?>"></script>                     <!-- Header Slider -->
<script src="<?php echo base_url($frameworks_dir . '/everlink/js/hammer.min.js'); ?>"></script>                                <!-- Header Slider -->
<script src="<?php echo base_url($frameworks_dir . '/everlink/js/sequence.min.js'); ?>"></script>                              <!-- Header Slider -->
<script src="<?php echo base_url($frameworks_dir . '/everlink/js/tweetie.min.js'); ?>"></script>                               <!-- Twitter Feed -->
<script src="<?php echo base_url($frameworks_dir . '/everlink/js/jquery.countimator.min.js'); ?>"></script>                    <!-- Counter -->
<script src="<?php echo base_url($frameworks_dir . '/everlink/js/bootstrap.min.js'); ?>"></script>                             <!-- Bootstrap -->
<script src="<?php echo base_url($frameworks_dir . '/everlink/js/jquery.sticky.min.js'); ?>"></script>                         <!-- Sticky Header -->
<script src="<?php echo base_url($frameworks_dir . '/everlink/js/jquery.scrollUp.min.js'); ?>"></script>                       <!-- scroll top -->
<script src="<?php echo base_url($frameworks_dir . '/everlink/js/style.js'); ?>"></script>                                     <!-- Template Changeable Plugin Options -->
<script src="<?php echo base_url($frameworks_dir . '/everlink/js/wow.min.js'); ?>"></script>									<!--Wow animation js -->

<!-- Header banner particle scripts -->
<script src="<?php echo base_url($frameworks_dir . '/everlink/js/particles.js'); ?>"></script>									<!-- Header banner particle js -->
<script src="<?php echo base_url($frameworks_dir . '/everlink/js/app.js'); ?>"></script>										<!-- Header banner particle js -->

</body>
</html>
