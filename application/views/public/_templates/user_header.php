<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="msapplication-tap-highlight" content="no"/>
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="Everlink">
    <meta name="author" content="">
    <title>@<?= $user->username; ?> | Everlink</title>

    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->

    <!--- STYLESHEETS -->
    <link rel="icon" href="<?php echo base_url('assets/frameworks/everlink/images/logo.png'); ?>">
    <!-- Browser Tab Icon -->
    <link href="<?php echo base_url('assets/frameworks/bootstrap/css/bootstrap.min.css'); ?>" rel="stylesheet">
    <!-- Bootstrap -->
    <link rel="stylesheet" href="<?php echo base_url('assets/frameworks//bootstrap/css/all.min.css'); ?>"> <!-- Font-Awesome Icons -->
    <link rel="stylesheet" href="<?php echo base_url('assets/frameworks/everlink/css/theme.css'); ?>">
	<!-- Google font (Poppins)  -->
</head>
<body style="height: 100vh;">