<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="<?php echo $charset; ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="msapplication-tap-highlight" content="no"/>
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="App Landing Page">
    <meta name="author" content="">
    <title>Everlinks - Boost your instagam Bio Link</title>

<?php if ($mobile === FALSE): ?>
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<?php else: ?>
    <meta name="HandheldFriendly" content="true">
<?php endif; ?>
<?php if ($mobile == TRUE && $mobile_ie == TRUE): ?>
    <meta http-equiv="cleartype" content="on">
<?php endif; ?>

    <!--- STYLESHEETS -->
    <link rel="icon" href="<?php echo base_url($frameworks_dir . '/everlink/images/logo.png'); ?>">                                <!-- Browser Tab Icon -->
    <link href="<?php echo base_url($frameworks_dir . '/everlink/css/bootstrap.min.css'); ?>" rel="stylesheet">                    <!-- Bootstrap -->
    <link rel="stylesheet" href="<?php echo base_url($frameworks_dir . '/everlink/css/font-awesome.min.css'); ?>">                 <!-- Font-Awesome Icons -->
    <link rel="stylesheet" href="<?php echo base_url($frameworks_dir . '/everlink/css/icomoon.min.css'); ?>">                      <!-- iconmoon Icons -->
    <link rel="stylesheet" href="<?php echo base_url($frameworks_dir . '/everlink/css/swiper.min.css'); ?>">                       <!-- Carousel slider -->
    <link rel="stylesheet" href="<?php echo base_url($frameworks_dir . '/everlink/css/style.css'); ?>">                            <!-- Template CSS -->
    <link rel="stylesheet" href="<?php echo base_url($frameworks_dir . '/everlink/css/animate.css'); ?>">							<!-- Wow Animation CSS -->
    <link rel="stylesheet" media="screen" href="<?php echo base_url($frameworks_dir . '/everlink/css/style-particle.css'); ?>">	<!-- ParticlesCSS -->

    <link rel="stylesheet" href="<?php echo base_url($frameworks_dir . '/everlink/css/google-fonts.css'); ?>">                     <!-- Google font (Poppins) font face -->
    <link href="//fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" rel="stylesheet">
    <link href="//fonts.googleapis.com/css?family=Roboto+Slab:100,300,400,700" rel="stylesheet">
    <link href="<?php echo base_url($frameworks_dir . '/everlink/css/all.css'); ?>">

	<!-- Google font (Poppins)  -->
</head>
<body>

<!-- STATIC BANNER -->
<!-- particles.js container -->
	<div id="particles-js">
    	<div class="container">
			<div class="static-banner ">
				<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                        <!-- Caption -->
                        <div class="banner-caption">

                            <!-- H1 Heading -->
                            <h1>Everlink</h1>

                            <!-- H2 Heading -->
                            <h2>Multiple Links for your Instagram Bio</h2>

                            <!-- Paragraph -->
                            <p>
                                You only get one chance to link in Instagram.Make it do more.
                            </p>

                            <!-- Buttons -->
                            <a href="<?php echo site_url('redirect/instagram'); ?>" class="slide-button slide-button-active">START NOW</a>

                            <!-- Button -->
                            <a href="#amazing-features" class="slide-button">MORE INFO</a>

					</div>
				</div>
                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                        <div class="static-banner-image">
                            <img src="<?php echo base_url($frameworks_dir . '/everlink/images/01.png'); ?>" class="mockup-back" alt="image">
                            <img src="<?php echo base_url($frameworks_dir . '/everlink/images/02.png'); ?>" class="mockup-back" alt="image">
                        </div>
                  </div>
			</div>
		</div>
	</div>

<!-- WRAPPER -->    <!-- Preloader -->
<div class="wrapper preloader" id="site-home">
    <!-- NAVIGATION AND SLIDER HOLDER -->
    <section class="site-holder" role="banner">

        <!-- HEADER -->
        <header class="site-header">

            <!-- STICKY HEADER -->
            <div class="sticky-header" id="sticky-header">
                <div class="container">
                    <div class="row">
                        <div class="col-xs-8 col-sm-3">

                            <!-- LOGO -->
                            <div class="site-logo">
                                <a href="index.html">
                                    <span>Everlink</span>
                                </a>
                            </div>
                            <!-- END LOGO -->

                        </div>
                        <div class="col-xs-4 col-sm-9">

                            <!-- NAVIGATION -->
                            <nav class="site-nav" id="site-nav" role="navigation">
                                <!-- MOBILE VIEW BUTTON -->
                                <div class="nav-mobile">
                                    <i class="fa fa-bars show"></i>
                                    <i class="fa fa-close hide"></i>
                                </div>
                                <!-- LINKS -->
                                <ul class="nav-off-canvas">
                                    <!-- ACTIVE ITEM -->
                                    <li class="active"><a href="#site-home">Home</a></li>
                                    <li><a href="#site-more-features">Features</a></li>
                                    <li><a href="#site-quick-view">Screens</a></li>
                                    <li><a href="#site-download">Download</a></li>
                                    <li><a href="#site-packages">Pricing</a></li>
                                    <li><a href="#how-it-works">How It Works</a></li>
                                    <li><a href="#quick-support">Contact us</a></li>

<?php if ($logout_link): ?>
                                    <li><a href="<?php echo site_url('auth/logout'); ?>">Logout</a></li>
<?php else: ?>
                                    <li><a href="<?php echo site_url('redirect/instagram'); ?>">Login</a></li>
<?php endif; ?>
<?php if ($admin_link): ?>
                                    <li><a href="<?php echo site_url('system-admin'); ?>">Admin</a></li>
<?php elseif($user_link): ?>
                                    <li><a href="<?php echo site_url('admin'); ?>">Admin</a></li>
<?php endif; ?>
                                </ul>
                            </nav>
                            <!-- END NAVIGATION -->

                        </div>
                    </div>
                </div>
            </div>