<div class="user-theme <?= $user_theme; ?>">
    <div class="container">
        <div class="d-flex justify-content-center">
            <div class="user_profile_container">
                <div class="green_icon"></div>
                <div class="user_profile">
                    <img src="<?= $user->profile_picture; ?>">
                </div>
            </div>
        </div>
        <div class="d-flex justify-content-center pt-1">
            <h5 class="d-flex align-items-end user_instagram"><i class="fab fa-instagram"></i><?= $user->username; ?></h5>
        </div>
        <div class="d-flex justify-content-center pt-1 row">
            <?php if (isset($links) && !empty($links)): ?>
                <?php foreach ($links as $key => $value): ?>
                    <div class="col-lg-12">
                        <div class="card-body text-center">
                            <a href="//<?= $value['url']; ?>" class="btn btn-lg btn-custom btn-block" target="_blank"><?= $value['title']; ?></a>
                        </div>
                    </div>
                <?php endforeach; ?>
            <?php endif; ?>
        </div>
    </div>
</div>

