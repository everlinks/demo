<style type="text/css">
.panel
{
    padding: 15px;
}
#terms:hover
{
   text-decoration: underline;
}
.btn-success
{
    background: #1d8a8a;
    border-color: #1d8a8a;
}
.btn-success:hover
{
    background: #34a1a1;
    border-color: #34a1a1;
}
.error
{
    color: red;

}
div.remember-error
{
  text-decoration: none !important;
}


</style>

<div class="main">
    <div class="container">
        <input type="hidden" name="store" id="store" value="<?php echo base_url('/admin/get_started/'); ?>">
        <div class="container">
    <div class="row">
        <div class="col-md-8 m-auto">
            <div class="panel panel-default mt-5" style="background: white;">
                <div class="panel-heading text-center">
                    <h3 class="panel-title text-dark">Get Started!</h3>
                    <p>We just need to confirm a few details and you'll be managing your links in no time!</p>
                </div>
                <div class="panel-body">
                   
                   <form id="form1">
                        <div class="form-group">
                            <input class="form-control" id placeholder="Username" name="Username" type="text" value="<?php echo $user ?>" >
                        </div>
                        <div class="form-group">
                            <input class="form-control" placeholder="Email" name="email" type="email" value="" id="email" >
                        </div>
                        <div class="checkbox mb-4">
                            
                                <input name="remember" type="checkbox" id="checkbox" > I agree to the Everlink <a href="#" id="terms">Terms & Conditions</a>
                            
                        </div>
                        <input class="btn btn-lg btn-success btn-block" type="submit" value="Save details" id="save_details">
                   <div class="text-center"> <a href="#"  id="terms" data-toggle="tooltip" title="Hooray!">Why?</a>


                    </div>
                   </form>
                    
                </div>
            </div>
        </div>
    </div>
</div>
</div>


<script type="text/javascript">




//     window.onload=function(){
// $("#save_details").click(function(){
//     var email=$("#email").val();
//     var store = $('#store').val();
//     var check_box= +$("#checkbox").is(':checked');



//     var dataString = 'email=' + email +'&check_box='+check_box;

    
     // $.ajax({
     //      type: 'POST',
     //      dataType: 'json',
     //      data: dataString,
     //      url: store+"update_mail",
     //      success: function(data) {
     //          console.log("success");
     //      },
     //      error: function(data){
     //         console.log(data);
     //      }
     //  });
//   });

// }


window.onload =(function(){
$("#form1").validate({
// Specify the validation rules
rules:
    {
        email: {
            required: true,
            email: true
        },
        remember: "required"
    },
    messages: {
   // email: "Please enter your Email", 
    remember: "Please accept our policy"
}, 
errorElement:'div',
errorPlacement: function (error, element) {
    if (element.attr("type") == "checkbox") {
        //error.appendTo ( element.next());
        error.insertAfter(element.next());
    } 
    else
    {
       error.appendTo( element.parent() );
    }
},
submitHandler: function(form) 
{   
    var email=$("#email").val();
    var store = $('#store').val();
    var dataString = 'email=' + email;
    $.ajax({
          type: 'POST',
          dataType: 'json',
          data: dataString,
          url: store+"update_mail",
          success: function(data) {
              console.log("success");
              window.location = data.redirect;
          },
          error: function(data){
             console.log(data);
          }
      }); }

})
});
</script>




