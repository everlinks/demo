<style type="text/css">
.main {
  padding: 20px 0;
}
.card {
    margin-top: 1em;
}
.person-card {
    margin-top: 5em;
    padding-top: 5em;
}
.person-card .card-title{
    text-align: center;
}
.person-card .person-img{
    width: 10em;
    position: absolute;
    top: -5em;
    left: 50%;
    margin-left: -5em;
    border-radius: 100%;
    overflow: hidden;
    background-color: white;
}
.invalid-feedback {
    display: block;
}
.card-title {
    margin-bottom: 1.05rem;
    margin-top: 0.75rem;
    font-size: 1.75rem;
}
.form-group-td{
    display: inline-flex;
}
.modal-body a{
    text-decoration: underline;
}
.text{
    text-align: center;
    font-size: 2.05rem;
}

.error
{
    color: red;

}
</style>

<script type="text/javascript">


 window.onload =(function(){
$("#form1").validate({
// Specify the validation rules
rules:
    {
        email: {
            required: true,
            email: true
        },
        
    },
    messages: {
   // email: "Please enter your Email", 
    //remember: "Please accept our policy"
}, 



})
});
</script>


<div class="main">
    <div class="container">
        <form method="POST" action="<?php echo site_url('admin/account'); ?>" id="form1">
            <!-- Sign up card -->
            <div class="card person-card">
                <div class="card-body">
                    <!-- Sex image -->
                    <img id="img_sex" class="person-img"
                    src="<?php echo $this->session->userdata['profile_picture']; ?>">
                    <h2 id="who_message" class="card-title">My information</h2>
                    <!-- First row (on medium screen) -->
                    <div class="row">
                       
                        <div class="form-group col-md-6">
                            <input id="real_name" type="text" name="real_name" class="form-control" placeholder="Real name" value="<?php echo $this->session->userdata['real_name']; ?>" required>
                            <div id="real_name_feedback" class="invalid-feedback">
                                <?php echo isset($message) ? $message : NULL ; ?>
                            </div>
                        </div>
                        <div class="form-group col-md-6">
                            <input type="email" name="email" class="form-control" id="email" placeholder="example@domain.com" value="<?php echo $this->session->userdata['email']; ?>" required>
                            <div id="email_feedback" class="invalid-feedback">
                            </div>
                        </div>
                   
                        <div class="col-md-2 mt-2">
                            <button type="submit" id="update" class="btn btn-gradient text-white oh-8 btn-block rounded-0">Update !</button>
                        </div>
                    </div>
                </div>
            </div>
            <!-- <pre>
                <!-- <?php print_r($this->session); ?> -->
            <!-- </pre> --> 
            <div class="row">
                <div class="col-md-6">
                    <div class="card">
                        <div class="card-body d-flex p-0">

                            <div class="px-3 py-2 w-100">
                            <h5 class="text">My trees</h5>
                           
                                <div class="text-1">
                                    <h5 class="text-dark"><?php echo $this->session->userdata['real_name']; ?></h5>
                                    <h6 class="text-warning">@<?php echo $this->session->userdata['username']; ?></h6>
                                </div>
                                <div class="text-2">
                                    <b class="text-dark">Plan</b>
                                    <h6 class="text-warning">Free</h6>
                                </div>
                                <div class="text-3">
                                    <b class="text-dark">Admins</b>
                                    <h6 class="text-warning">@<?php echo $this->session->userdata['username']; ?></h6>
                                </div>
                                <p>Upgrade this account to Everlink PRO to give other users access to manage it</p>
                                <div class="text-right m-0 mt-5 ">
                                    <a href="<?php echo site_url('admin'); ?>" class="btn btn-green text-white oh-8 rounded-0">Links</a>
                                    <a href="<?php echo site_url('admin/settings'); ?>" class="btn btn-green text-white oh-8 rounded-0">Settings</a>
                                    <a href="<?php echo site_url('admin/upgrade'); ?>" class="btn btn-gradient text-white oh-8 rounded-0"><i class="far fa-star"></i> Upgrade to Pro</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="card">
                        <div class="card-body" style="
    padding-bottom: 4rem;
">
                            <h5 class="card-title" style="
    margin-top: 0rem;
">Accounts Action</h5>
                            <div class="form-group-td">
                        <div class="action altsignin"><a class="btn btn-gradient text-white oh-8 rounded-0" data-toggle="modal" data-target="#basicModal">Sign in as a different user</a></div>
                        <div class="action logout"><a class="btn btn-green text-white oh-8 rounded-0" href="<?php echo site_url('auth/logout'); ?>">Logout</a></div>
                        
                        </div>
                        <h5 class="card-title">Delete account</h5>
                        <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#myModal">
                        Delete account
  </button>
  <div class="modal" id="myModal">
    <div class="modal-dialog">
      <div class="modal-content">
      
        <!-- Modal Header -->
        <div class="modal-header">
          <h4 class="modal-title">Are you sure?</h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        <div class="modal-body">
        <p>
                                    Deleting your account is irreversible. Please proceed with caution.
                                </p>
                                <p>
                                    By selecting YES, DELETE MY ACCOUNT below, your primary tree (@nik1234tanik) will be imediately terminated and your data will be lost. Visitors will no longer be able to access <a href="https://linktr.ee/nik1234tanik">your URL</a>.
                                </p>
                             
        </div>
        <div class="modal-footer">
       
          <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
          <button type="button" class="btn btn-default">
          <a href="<?php echo site_url('admin/account/delete_row'); ?>">Yes, Delete my Account</a>
          <!-- <button  type="button" class="btn" data-dismiss="modal" href="<?php echo site_url('admin/settings'); ?>">Yes, Delete my Account</button> -->
        </div>
                

                        </div>
                    </div>
                </div>
            </div>
  
 

<!-- basic modal -->
<div class="modal fade" id="basicModal" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="myModalLabel">Are you sure?</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
       <p>This action will sign you out of both your Linktree and Instagram accounts, and require you to sign into Instagram with another account.</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
        <button type="button" class="btn btn-default">
        <a href="<?php echo site_url('auth/logout'); ?>">Yes, Sign me out</a></button>
      </div>
    </div>
  </div>
</div>
        </form>
    </div>
</div>
