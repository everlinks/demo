<div class="container link-section">
    <div class="row align-items-center">
        <div class="col-md-6">
            <a href="javascript:void(0)" class="btn btn-lg btn-gradient add_button oh-8 text-white">Add New Link</a>
            <input type="hidden" name="store" id="store" value="<?php echo base_url('/admin/Dashboard/'); ?>">
        </div>
        <div class="col-md-6 text-center text-md-right">
            <p class="font-weight-bold mb-1">Total links #
                <b id="counter">
                    <?php 
                        if(isset($links) && !empty($links)){
                            echo count($links);
                        } else {
                            echo 0;
                        } 
                    ?>
                </b>
            </p>
        </div>
        <div class="col-md-12">
            <h4 class="font-weight-bold text-center mt-4">My Links</h4>
        </div>
        <div class="col-md-12">
            <div class="main-area grid" id="sortable">
                <?php if(isset($links) && !empty($links)): ?>
                    <?php foreach ($links as $key => $value): ?>
                        <div class="card flex-row" id="collapse-<?php echo $value['id']; ?>">
                            <div class="card-header border-0 dragger"><i class="fas fa-ellipsis-v"></i></div>
                            <div class="card-block p-1 form-section">
                                <input type="hidden" name="id" value="<?php echo $value['id']; ?>">
                                <div class="d-flex pb-1 justify-content-between align-items-center"><input type="text" name="title" class="title" placeholder="Title" value="<?php echo $value['title'] ?>">
                                    <div class="btn-toggle"><input type="checkbox" name="active" id="active<?php echo $value['id']; ?>" <?php if($value['active']) echo 'checked'; ?>><label class="oh-8" for="active<?php echo $value['id']; ?>">Toggle</label></div>
                                </div>
                                <div class="d-flex justify-content-between align-items-center"><input type="text" id="url" name="url" class="url" placeholder="http://url" value="<?php echo $value['url'] ?>"></div>
                                <ul class="nav nav-pills navtop d-flex justify-content-end">
                                    <li class="nav-item">
                                        <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#menu1-<?php echo $value['id']; ?>" aria-expanded="true">
                                          <i class="fas fa-images" aria-hidden="true"></i>
                                        </button>
                                    </li>
                                    <li class="nav-item">
                                        <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#menu2-<?php echo $value['id']; ?>" aria-expanded="true">
                                          <i class="fas fa-star" aria-hidden="true"></i>
                                        </button>
                                    </li>
                                    <li class="nav-item">
                                        <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#menu3-<?php echo $value['id']; ?>" aria-expanded="true">
                                          <i class="far fa-clock" aria-hidden="true"></i>
                                        </button>
                                    </li>

                                    <li class="nav-item">
                                        <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#menu4-<?php echo $value['id']; ?>" aria-expanded="true">
                                          <i class="fas fa-chart-line" aria-hidden="true"></i>
                                        </button>
                                    </li>
                                    <li class="nav-item">
                                        <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#menu5-<?php echo $value['id']; ?>" aria-expanded="true">
                                          <i class="far fa-trash-alt" aria-hidden="true"></i>
                                        </button>
                                    </li>
                                </ul>
                                <div class="tab-content text-center">
                                    <div id="menu1-<?php echo $value['id']; ?>" class="collapse" data-parent="#collapse-<?php echo $value['id']; ?>" class="panel-collapse collapse">
                                        <div class="box-part text-center"><i class="fas fa-images fa-2x" aria-hidden="true"></i>
                                            <div class="title"><h5>Thumbnail</h5></div>
                                            <div class="text">
                                                <span>Lorem ipsum dolor sit amet, id quo eruditi eloquentiam. Assum decore te sed. Elitr scripta ocurreret qui ad.</span>
                                            </div>
                                            <div class="action">
                                                <a href="javascript:void(0)" class="btn btn-green oh-8 text-white">Get Pro</a>
                                            </div>
                                        </div>
                                    </div>
                                    <div id="menu2-<?php echo $value['id']; ?>" class="collapse" data-parent="#collapse-<?php echo $value['id']; ?>" class="panel-collapse collapse">
                                        <div class="box-part text-center"><i class="fas fa-star fa-2x" aria-hidden="true"></i>
                                            <div class="title"><h5>Priority Link</h5></div>
                                            <div class="text">
                                                <span>Lorem ipsum dolor sit amet, id quo eruditi eloquentiam. Assum decore te sed. Elitr scripta ocurreret qui ad.</span>
                                            </div>
                                            <div class="action">
                                                <a href="javascript:void(0)" class="btn btn-green oh-8 text-white">Get Pro</a>
                                            </div>
                                        </div>
                                    </div>
                                    <div id="menu3-<?php echo $value['id']; ?>" class="collapse" data-parent="#collapse-<?php echo $value['id']; ?>" class="panel-collapse collapse">
                                        <div class="box-part text-center"><i class="far fa-clock fa-2x" aria-hidden="true"></i>
                                            <div class="title"><h5>Schedule Link</h5></div>
                                            <div class="text">
                                                <span>Lorem ipsum dolor sit amet, id quo eruditi eloquentiam. Assum decore te sed. Elitr scripta ocurreret qui ad.</span>
                                            </div>
                                            <div class="action">
                                                <a href="javascript:void(0)" class="btn btn-green oh-8 text-white">Get Pro</a>
                                            </div>
                                        </div>
                                    </div>
                                    <div id="menu4-<?php echo $value['id']; ?>" class="collapse" data-parent="#collapse-<?php echo $value['id']; ?>" class="panel-collapse collapse">
                                        <div class="box-part text-center"><i class="fas fa-chart-line fa-2x" aria-hidden="true"></i>
                                            <div class="title"><h5>Hits Counter</h5></div>
                                            <div class="text">
                                                <span>Lorem ipsum dolor sit amet, id quo eruditi eloquentiam. Assum decore te sed. Elitr scripta ocurreret qui ad.</span>
                                            </div>
                                            <div class="action">
                                                <a href="javascript:void(0)" class="btn btn-green oh-8 text-white">Get Pro</a>
                                            </div>
                                        </div>
                                    </div>
                                    <div id="menu5-<?php echo $value['id']; ?>" class="collapse" data-parent="#collapse-<?php echo $value['id']; ?>" class="panel-collapse collapse">
                                        <div class="box-part text-center"><i class="far fa-trash-alt fa-2x" aria-hidden="true"></i>
                                            <div class="title"><h5>Delete</h5></div>
                                            <div class="text">
                                                <span>Are you sure you want to delete this link?</span>
                                                <span>This action cannot be undone.</span>
                                            </div>
                                            <div class="action">
                                                <a href="javascript:void(0)" class="btn" data-toggle="collapse" data-target="#menu5-<?php echo $value['id']; ?>">Cancel</a>
                                                <a href="javascript:void(0)" class="btn btn-danger link-remove oh-8 text-white">Delete</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php endforeach; ?>
                <?php else: ?>
                    <div class="link-unavailabe">
                        <h5 class="text-center pt-5">Click on <b>Add New Link</b> button</h5>
                        <h5 class="text-center">To create your first Link</h5>
                    </div>
                <?php endif; ?>
            </div>
        </div>
    </div>
</div>