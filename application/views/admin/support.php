<body>
  
                                    <div class="container">
                                        <div class="content">
                                            <section class="section">
                                                <div class="bg">
                                                    <a href="#" class="pap">
                                                        <div class="collection">
                                                            <div class="collection__photo">
                                                                    <svg role="img" viewBox="0 0 48 48"><g id="book-bookmark" stroke-width="2" fill="none" fill-rule="evenodd" stroke-linecap="round"><path d="M35 31l-6-6-6 6V7h12v24z"></path><path d="M35 9h6v38H11a4 4 0 0 1-4-4V5" stroke-linejoin="round"></path><path d="M39 9V1H11a4 4 0 0 0 0 8h12" stroke-linejoin="round"></path></g></svg>
                                                                    </div>
                                                                    <div class="meta">
                                                                        <h2 class="t3">Features How To</h2>
                                                                        <div class="avatar">
                                                                                <div class="avatar__photo avatars__images o__ltr">
                                                                                      <img src="https://static.intercomassets.com/avatars/1952807/square_128/4eTy83ekQvKfdf0G3epUWA_thumb_604-1528252847.jpg?1528252847" alt="Cameron Mackinnon avatar" class="avatar__image">
                                                                              
                                                                                      <img src="https://static.intercomassets.com/avatars/417990/square_128/IMG_2270-1463315014.JPG?1463315014" alt="Alex Zaccaria avatar" class="avatar__image">
                                                                              
                                                                                </div>
                                                                                <div class="avatar__info">
                                                                                  <div>
                                                                                    <span class="cd">
                                                                                      11 articles in this collection
                                                                                    </span>
                                                                                    <br>
                                                                                    Written by <span class="c__darker"> Cameron Mackinnon</span> and <span class="c__darker"> Alex Zaccaria</span>
                                                                                  </div>
                                                                                </div>
                                                                              </div>
                                                                              </div>
                                                                              </div>
                                                                              </a>
                                                                              </div>
                                                                              <div class="gbg">
                                                                                    <a href="#" class="pap">
                                                                                            <div class="collection">
                                                                                                <div class="collection__photo">
                                                                                                        <svg role="img" viewBox="0 0 48 48"><g id="book-bookmark" stroke-width="2" fill="none" fill-rule="evenodd" stroke-linecap="round"><path d="M35 31l-6-6-6 6V7h12v24z"></path><path d="M35 9h6v38H11a4 4 0 0 1-4-4V5" stroke-linejoin="round"></path><path d="M39 9V1H11a4 4 0 0 0 0 8h12" stroke-linejoin="round"></path></g></svg>
                                                                                                        </div>
                                                                                                        <div class="meta">
                                                                                                            <h2 class="t3">Getting Started</h2>
                                                                                                            <div class="avatar">
                                                                                                                    <div class="avatar__photo avatars__images o__ltr">
                                                                                                                          <img src="https://static.intercomassets.com/avatars/1952807/square_128/4eTy83ekQvKfdf0G3epUWA_thumb_604-1528252847.jpg?1528252847" alt="Cameron Mackinnon avatar" class="avatar__image">
                                                                                                                  
                                                                                                                          <img src="https://static.intercomassets.com/avatars/417990/square_128/IMG_2270-1463315014.JPG?1463315014" alt="Alex Zaccaria avatar" class="avatar__image">
                                                                                                                  
                                                                                                                    </div>
                                                                                                                    <div class="avatar__info">
                                                                                                                      <div>
                                                                                                                        <span class="cd">
                                                                                                                          
                                                4 articles in this collection
                                              
                                                                                                                        </span>
                                                                                                                        <br>
                                                                                                                        Written by <span class="c__darker">  Alex Zaccaria</span> and <span class="c__darker">  Cameron Mackinnon</span>
                                                                                                                      </div>
                                                                                                                    </div>
                                                                                                                  </div>
                                                                                                            </div>
                                                                                                                  </div>
                                                                                                                  </a>
                                                                                                                  </div>
                                        
                                                                          
<div class="gb">
        <a href="#" class="pap">
                <div class="collection">
                    <div class="collection__photo">
                            <svg role="img" viewBox="0 0 48 48"><g id="book-bookmark" stroke-width="2" fill="none" fill-rule="evenodd" stroke-linecap="round"><path d="M35 31l-6-6-6 6V7h12v24z"></path><path d="M35 9h6v38H11a4 4 0 0 1-4-4V5" stroke-linejoin="round"></path><path d="M39 9V1H11a4 4 0 0 0 0 8h12" stroke-linejoin="round"></path></g></svg>
                            </div>
                            <div class="meta">
                                <h2 class="t3">FAQ</h2>
                                <div class="avatar">
                                        <div class="avatar__photo avatars__images o__ltr">
                                              <img src="https://static.intercomassets.com/avatars/1952807/square_128/4eTy83ekQvKfdf0G3epUWA_thumb_604-1528252847.jpg?1528252847" alt="Cameron Mackinnon avatar" class="avatar__image">
                                      
                                              <img src="https://static.intercomassets.com/avatars/417990/square_128/IMG_2270-1463315014.JPG?1463315014" alt="Alex Zaccaria avatar" class="avatar__image">
                                      
                                        </div>
                                        <div class="avatar__info">
                                          <div>
                                            <span class="cd">
                                              11 articles in this collection
                                            </span>
                                            <br>
                                            Written by <span class="c__darker"> Cameron Mackinnon</span> and <span class="c__darker"> Alex Zaccaria</span>
                                          </div>
                                        </div>
                                      </div>
                                      </div>
                                      </div>
                                      </a>
                                      </div>
                                      </section>
                                      </div>
                                      </div>
                                   
                                      <footer class="footer">
                                       <div class="log">
                                           <div class="con">
                                               <div class="cf">
                                                   <div class="foot">
                                                        <a href="/">
                                                            <img alt="Linktree Help Center" src="linktree_logo_white.png">
                                                        </a>
                                                        </div>
                                                        </div>
                                                        <div class="footer__advert logo">
                                                                <img src="https://intercom.help/_assets/intercom-a6a6ac0f033657af1aebe2e9e15b94a3cd5eabf6ae8b9916df6ea49099a894d8.png" alt="Intercom">
                                                                <a href="https://www.intercom.com/intercom-link?company=Linktree&amp;solution=customer-support&amp;utm_campaign=intercom-link&amp;utm_content=We+run+on+Intercom&amp;utm_medium=help-center&amp;utm_referrer=https%3A%2F%2Fsupport.linktr.ee%2F&amp;utm_source=desktop-web">We run on Intercom</a>
                                                              </div>
                                                              </div>
                                                              </div>
                                                              </div>
                                                              </footer>
                                             <style>
.content{
    max-width: 100%;
    width: 900px;
    margin-left: auto;
    margin-right: auto;

}
#leftPanel{
    height:100%;
}
.section{
    padding-top: 26px;
}
.pap{
    background-color: white;
    border-radius: 4px;
    padding: 30px;
    position: relative;
    z-index: 3;
    text-decoration: none;
    overflow: hidden;
    width: 100%;
    display: block;
    outline: none;
    box-shadow: 0 3px 8px 0 rgba(0,0,0,0.03);
    border: 1px solid #d4dadf;
}
.btn{
    color: white;

}
.collection{
    position: relative;
    padding-left: 150px;
}
.collection__photo{
    position: absolute;
    left: 0;
    top: 0;
    width: 120px;
    height: 100%;
}
.collection__photo svg {
    stroke: #818a97;
    position: relative;
    width: 48px;
    height: 48px;
    transition: stroke .1s linear;
    top: 50%;
    left: 50%;
    margin: -24px 0 0 -24px;
}
.t3{
   
    margin: -5px 0 2px;
    font-size: 18px;
    line-height: 1.24;
    font-weight: 400;
    text-decoration: none;
}
.t3:hover{
    color:#4c98c3;
}
.avatar {
    font-size: 13px;
    color: #8f919d;
    margin-top: 4px;
}
.avatar__info, .avatar__photo {
    line-height: 1.4;
}
.avatar__info, .avatar__photo {
    line-height: 1.4;
}
.avatar__image {
    width: 32px;
    height: 32px;
    vertical-align: bottom;
    border-radius: 50%;
    box-shadow: 0 0 0 2px white;
    position: relative;
    z-index: 4;
}
.gb{
    margin-top: 16px;
}
.gbg{
    margin-top: 16px;
}
body{
    background-color: #f3f5f7;
}

.footer {
    margin-top: 35px;
    padding: 40px 0;
    text-align: left;
    font-size: 14px;
    -webkit-box-flex: 0;
    flex: none;
    background: #ffffff;
    color: #a3abb5;
}
.log{
    padding-left: 40px;
    padding-right: 40px;

}
.con{
    max-width: 100%;
    width: 900px;
    margin-left: auto;
    margin-right: auto;
}
.foot{
    height: 50px;
    line-height: 30px;
    text-align: center;
    vertical-align: middle;
}
.foot a{
    color: #909aa5;
    text-decoration: none;
}
.foot img {
    max-height: 32px;
    filter: grayscale(100%) contrast(80%);
}
.footer__advert {
    text-align: center;
    font-size: 14px;
}
.footer__advert img {
    height: 14px;
    vertical-align: middle;
}
.footer__advert a {
    padding-left: 5px;
    color: #a8b6c2;
    vertical-align: middle;
}
.cd{
    color: #4f5e6b;
}
.t3{
    color: #367fa9;
}
</style>                 

    
