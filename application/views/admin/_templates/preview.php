<div class="col" id="previewPanel">
	<nav class="navbar-expand-md navbar justify-content-center">
		<ul class="navbar-nav">
		 	<li class="nav-item">
		 		<h6 class="text-white d-inline">My Bio Link:</h6>
		 		<span class="bg-dark w-30 mx-2" id="copyLink">
			 		<a href="<?php echo site_url($this->session->userdata['username']); ?>" target="_blank"><?php echo site_url($this->session->userdata['username']); ?></a>
			 	</span>
			 	<button type="button" class="btn-copy js-tooltip js-copy oh-8" data-toggle="tooltip" data-placement="bottom" data-copy="<?php echo site_url($this->session->userdata['username']); ?>" title="Copy Link"><i class="fa fa-copy" aria-hidden="true"></i></button>
		 	</li>
		</ul>
	</nav>
    <div class="container d-flex justify-content-center">
    	<input type="hidden" id="preview" value="<?php echo site_url($this->session->userdata['username']); ?>">
		<div class="marvel-device iphone-x">
		    <div class="notch">
		        <div class="speaker"></div>
		    </div>
		    <div class="top-bar"></div>
		    <div class="sleep"></div>
		    <div class="bottom-bar"></div>
		    <div class="volume"></div>
		    <div class="overflow">
		        <div class="shadow shadow--tr"></div>
		        <div class="shadow shadow--tl"></div>
		        <div class="shadow shadow--br"></div>
		        <div class="shadow shadow--bl"></div>
		    </div>
		    <div class="inner-shadow"></div>
		    <div class="screen">
		    	<div class="mobile-content customScroll h-100"></div>
		    </div>
		</div>
    </div>
</div>
<script type="text/javascript">
	
function copyToClipboard(text, el) {
  var copyTest = document.queryCommandSupported('copy');
  var elOriginalText = el.attr('data-original-title');

  if (copyTest === true) {
    var copyTextArea = document.createElement("textarea");
    copyTextArea.value = text;
    document.body.appendChild(copyTextArea);
    copyTextArea.select();
    try {
      var successful = document.execCommand('copy');
      var msg = successful ? 'Copied!' : 'Whoops, not copied!';
      el.attr('data-original-title', msg).tooltip('show');
    } catch (err) {
      console.log('Oops, unable to copy');
    }
    document.body.removeChild(copyTextArea);
    el.attr('data-original-title', elOriginalText);
  } else {
    // Fallback if browser doesn't support .execCommand('copy')
    window.prompt("Copy to clipboard: Ctrl+C or Command+C, Enter", text);
  }
}

function previewUpdate() {
	$.post($('#preview').val(), function( data ) {
	    $('.mobile-content').html(data)
	});
}

window.onload = function() {
	$('.js-tooltip').tooltip();
	$('.js-copy').click(function() {
		var text = $(this).attr('data-copy');
		var el = $(this);
		copyToClipboard(text, el);
	});
	previewUpdate();
	new PerfectScrollbar('.customScroll');
}
</script>
<link rel="stylesheet" href="<?php echo base_url($frameworks_dir . '/everlink-admin/css/devices.min.css'); ?>">
<link rel="stylesheet" href="<?php echo base_url($frameworks_dir . '/everlink/css/theme.css'); ?>">