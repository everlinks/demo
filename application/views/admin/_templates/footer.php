	</div>
</div>

<!--SCRIPTS -->
<script src="<?php echo base_url($frameworks_dir . '/everlink/js/jquery-1.12.4.min.js'); ?>"></script>
<script src="<?php echo base_url($frameworks_dir . '/bootstrap/js/popper.min.js'); ?>"></script>
<script src="<?php echo base_url($frameworks_dir . '/bootstrap/js/bootstrap.min.js'); ?>"></script>
<script src="<?php echo base_url($plugins_dir . '/perfectscrollbar/perfect-scrollbar.js'); ?>"></script>
<?php $active = strtolower($this->uri->segment(2)); ?>
<?php if($active == '' || $active == 'dashboard'): ?>
    <script src="<?php echo base_url($plugins_dir . '/sortable/sortable.js'); ?>"></script>
    <script src="<?php echo base_url($frameworks_dir . '/everlink-admin/js/links.js'); ?>"></script>
<?php endif; ?>
<?php if('get_started'): ?>
    <script src="<?php echo base_url($frameworks_dir . '/jquery/jquery.validate.min.js'); ?>"></script>
<?php endif; ?>
<?php if('upgrade'): ?>
	<script src="https://js.stripe.com/v3/" type="text/javascript" ></script>
<?php endif; ?>
<script type="text/javascript">
    $(document).ready(function () {
        $('#sidebarCollapse').on('click', function () {
            $('#sidebar').toggleClass('active');
        });
    });
    var modalConfirm = function(callback) {

	    $("#btn-confirm").on("click", function() {
	        $("#mi-modal").modal('show');
	    });

	    $("#modal-btn-yes").on("click", function() {
	        callback(true);
	        $("#mi-modal").modal('hide');
	    });

	    $("#modal-btn-no").on("click", function() {
	        callback(false);
	        $("#mi-modal").modal('hide');
	    });
	};

	modalConfirm(function(confirm) {
	    if (confirm) {
	        window.location = "<?php echo site_url('auth/logout'); ?>";
	    }
	});
</script>
</body>
</html>
