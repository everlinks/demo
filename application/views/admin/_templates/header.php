<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="<?php echo $charset; ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="msapplication-tap-highlight" content="no"/>
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="Everlinks Dashboard">
    <meta name="author" content="">
    <title>Everlinks - Boost your instagam Bio Link</title>

<?php if ($mobile === FALSE): ?>
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<?php else: ?>
    <meta name="HandheldFriendly" content="true">
<?php endif; ?>
<?php if ($mobile == TRUE && $mobile_ie == TRUE): ?>
    <meta http-equiv="cleartype" content="on">
<?php endif; ?>

    <!--- STYLESHEETS -->
    <link rel="icon" href="<?php echo base_url($frameworks_dir . '/everlink/images/logo.png'); ?>">
    <link href="<?php echo base_url($frameworks_dir . '/bootstrap/css/bootstrap.min.css'); ?>" rel="stylesheet">
    <link href="<?php echo base_url($frameworks_dir . '/bootstrap/css/all.min.css'); ?>" rel="stylesheet">
    <link href="<?php echo base_url($frameworks_dir . '/everlink-admin/css/everlink.css'); ?>" rel="stylesheet">
    <link href="<?php echo base_url($plugins_dir . '/perfectscrollbar/perfect-scrollbar.css'); ?>" rel="stylesheet">
    <?php if('upgrade'): ?>
        <link href="<?php echo base_url($frameworks_dir . '/everlink-admin/css/upgrade.css'); ?>" rel="stylesheet">
    <?php endif; ?>
</head>
<body>
  <div class="wrapper">
