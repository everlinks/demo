<?php $active = strtolower($this->uri->segment(2)); ?>
<nav class="navbar-expand-md navbar justify-content-center">
	<ul class="navbar-nav">
	 	<li class="nav-item <?php if($active == '' || $active == 'dashboard') echo 'active'; ?>">
	 		<a class="nav-link" href="<?php echo site_url('admin'); ?>">Links</a>
	 	</li>
	 	<li class="nav-item <?php if($active == 'settings') echo 'active'; ?>">
		 	<a class="nav-link" href="<?php echo site_url('admin/settings'); ?>">Setting</a>
	 	</li>
	 	<li class="nav-item <?php if($active == 'pro') echo 'active'; ?>">
		 	<a class="nav-link" href="<?php echo site_url('admin/pro'); ?>">Pro</a>
	 	</li>
	</ul>
</nav>