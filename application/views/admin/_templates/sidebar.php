<!-- Sidebar  -->
<?php $active = strtolower($this->uri->segment(2)); ?>
<nav id="sidebar" class="active">
    <div class="sidebar-header">
        <h3><a href="<?php echo site_url(); ?>">EverLinks</a></h3>
        <strong><a href="<?php echo site_url(); ?>">EL</a></strong>
        <button type="button" id="sidebarCollapse" class="btn btn-info">
            <i class="fas fa-align-left"></i>
        </button>
    </div>
    <ul class="list-unstyled components">
        <li <?php if($active == 'account') echo 'class="active"'; ?>>
            <a href="<?php echo site_url('admin/account'); ?>">
                <i class="fas fa-user-circle"></i>
                My Account
            </a>
        </li>
        <li>
            <a href="javascript:void(0)" id="btn-confirm">
                <i class="fas fa-clock"></i>
                Sign in as a different user
            </a>
        </li>
        <li <?php if($active == 'support') echo 'class="active"'; ?>>
            <a href="<?php echo site_url('admin/support'); ?>">
                <i class="fas fa-question-circle"></i>
                Support
            </a>
        </li>
        <li>
            <a href="<?php echo site_url('auth/logout'); ?>">
                <i class="fas fa-sign-out-alt"></i>
                Logout
            </a>
        </li>
    </ul>
</nav>
<div class="modal fade" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true" id="mi-modal">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel">Are You Sure?</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-footer">
                <div class="dialog__body">
                    <p>
                        This action will sign you out of both your Everlink and Instagram accounts, and
                        require you to sign into Instagram with another account.
                    </p>
                    <button type="button" class="btn btn-default" id="modal-btn-no">Cancel</button>
                    <button type="button" class="btn btn-danger" id="modal-btn-yes">Yes,Sign me out</button>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="content">
    <div class="row no-gutters">
        <div class="col" id="leftPanel">