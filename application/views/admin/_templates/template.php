<?php
defined('BASEPATH') OR exit('No direct script access allowed');

if (isset($header))
{
    echo $header;
}

if (isset($sidebar))
{
    echo $sidebar;
}

if (isset($main_header))
{
    echo $main_header;
}

if (isset($content))
{
    echo $content;
    echo '</div>';
}

if (isset($preview))
{
    echo $preview;
}

if (isset($footer))
{
    echo $footer;
}
