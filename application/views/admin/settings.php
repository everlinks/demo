<input type="hidden" name="store" id="store" value="<?php echo base_url('/admin/settings/'); ?>">
<div class="action">
    <div class="main">
        <div class="settings-wrap">
            <input type="hidden" id="user_id" value="<?php echo $user ?>">
            <h3>Appearance</h3>
            <div class="section__inner">
                <div class="setting theme">
                    <h6>THEMES</h6>
                    
                    <div class="theme-options" id="setting-theme" data-active-highlight>
                
                  
                  <label class="theme-option theme-option--leaf" name="leaf" for="leaf"  data-active-highlight-for="#colourway-radio-leaf">
                  <div class="theme__icon" data-height-padder>
                  <div class="theme__icon__inner" style="
    background-color: #3d3b3c;
"></div>
                  </div>
                  <input type="radio" id="leaf"  name="color" data-settings-setting="" data-settings-setting-key="account[colourway]" data-settings-setting-value="leaf" value="leaf" setting="" style="
    visibility: hidden;
"  data-id="1">
                  </label>
                     
<label class="theme-option theme-option--snow" name="snow" for="snow" data-active-highlight-for="#colourway-radio-snow" >
<div class="theme__icon" data-height-padder>
<div class="theme__icon__inner" style="
    background-color: #39e09b;
"></div></div>
<input type="radio" id="snow" name="color" data-settings-setting=""  data-settings-setting-key="account[colourway]" data-settings-setting-value="snow" value="snow" setting="" style="
    visibility: hidden;
" data-id="2">
</label>

<label class="theme-option theme-option--miami" name="miami" for="miami" data-active-highlight-for="#colourway-radio-snow" >
<div class="theme__icon" data-height-padder>
<div class="theme__icon__inner" style="
    background-color: #d463a3;
"></div></div>
<input type="radio" id="miami" name="color" data-settings-setting=""  data-settings-setting-key="account[colourway]" data-settings-setting-value="miami" value="miami" setting="" style="
    visibility: hidden;
"  data-id="3"></label>

<label class="theme-option theme-option--bloom" name="bloom" for="bloom" data-active-highlight-for="#colourway-radio-bloom" >
<div class="theme__icon" data-height-padder>
<div class="theme__icon__inner" style="
    background-color: #694ff6;
"></div></div>
<input type="radio" id="bloom" name="color" data-settings-setting=""  data-settings-setting-key="account[colourway]" data-settings-setting-value="bloom" value="bloom" setting="" style="
    visibility: hidden;"
 data-id="4"></label>
                       
                       
                    
                        <div class="dropdown">
                        <button type="submit" class="dropdown" >
                            <div class="theme__icon" data-height-padder="">
                               <div class="theme__icon__inner"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 29 29"><path fill="none" stroke="#404040" stroke-linecap="round" stroke-linejoin="round" d="M18 18c-1 1-2.1 1.5-3.5 1.5S11.9 19 11 18c-1-1-1.5-2.1-1.5-3.5S10 12 11 11s2.1-1.5 3.5-1.5S17 10 18 11s1.5 2.1 1.5 3.5S19 17 18 18zm6.9-.8c1.8-.7 3-1.2 3.6-1.5v-2.5c-.7-.3-1.9-.8-3.6-1.4l-.3-.1-1-2.3.1-.3c.8-1.8 1.3-3 1.5-3.6l-1.8-1.8c-.7.3-1.9.8-3.6 1.5l-.3.1-2.3-.9-.1-.3c-.7-1.8-1.2-3-1.5-3.6h-2.5c-.3.6-.8 1.8-1.4 3.6l-.1.3-2.3 1-.2-.2c-1.8-.7-3-1.2-3.7-1.4L3.7 5.5c.2.7.7 1.9 1.5 3.5l.1.3-1 2.3-.3.1c-1.8.7-3 1.2-3.6 1.5v2.5c.6.3 1.8.8 3.6 1.4l.3.1 1 2.3-.1.3c-.8 1.7-1.2 3-1.5 3.6l1.8 1.8c.8-.3 1.9-.8 3.6-1.5l.3-.1 2.3 1 .1.3c.7 1.7 1.2 2.9 1.5 3.6h2.5c.4-.7.8-1.9 1.4-3.6l.1-.3 2.3-1 .3.1c1.7.7 2.9 1.2 3.6 1.5l1.8-1.8c-.2-.7-.8-1.9-1.5-3.5l-.1-.3 1-2.3c-.1 0 .2-.1.2-.1z"></path></svg></div>
                            </div></button>
                            <div class="dropdown-content">
    <a class="it" href="#">   5 extra themes + Custom theme settings available with Everlink PRO.
                                                        </a>
  </div>
  </label>
                            </div>
                            </div>
                            </div>
                        </a>
                        <h6 style="margin-top: 26px;">CUSTOM THEMES</h6>
                        <p style="font-size: 0.90em;">More theme options and custom branding available on Everlink PRO.</p>
                </div>
            </div>
        
    </div>
</div>
<script type="text/javascript">
document.addEventListener("DOMContentLoaded", function(event) { 
    $("input[name=color]").click(function(){
        var theme_id=$(this).data("id");
        var store = $('#store').val();
        var dataString = 'theme_id=' + theme_id;
        
         $.ajax({
            url: store+'theme_id_update',
            type: "POST",
            data: dataString,
            success: function (data) {
                previewUpdate();
            },
            error: function (result) {
                alert("error");                                                               
            }
        });
    });
});
</script>
<style>
.theme-option .theme__icon__inner {
    width: 30px;
    height: 30px;
    margin-top: 31px;
    margin-left: 23px;
  
}
label{
    display: inline-grid;
    margin-bottom: 0px;

}
.theme-option:hover{
    border: 1px solid;
}
.dropbtn {
    /* background-color: #4CAF50; */
    color: white;
    cursor: pointer;
    /* padding: 16px;
    font-size: 16px; */
    border: none;
}
.dropdown {
    position: relative;
    display: grid;
    border: 2px dotted;
    background-color: white;
    cursor: pointer;
}
.dropdown-content {
    display: none;
    position: absolute;
    background-color: black;
    min-width: 160px;
    box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
    z-index: 1;
}
.dropdown-content .it {
    color: white;
    padding: 12px 16px;
    text-decoration: none;
    display: block;
}


.action {
    margin-top: 75px;
}

.theme-options {
    text-align: left;
    position: relative;
    margin: 0 auto;
    display: inline-flex;
}

.it .btn .dropdown-content {
    display: none;
    position: absolute;
    background-color: black;
    min-width: 160px;
    box-shadow: 0px 8px 16px 0px rgba(0, 0, 0, 0.2);
    z-index: 1;
}


.dropdown-content .it {
    color: white;
    padding: 12px 16px;
    text-decoration: none;
    display: block;
}

.dropdown-content .it:hover {
    background-color: #1d8a8a;
}

.dropdown:hover .dropdown-content {
    display: block;
}

.dropdown:hover .btn {
    background-color: black;
}


.active-highlight {
    width: 76px;
    height: 76px;
    top: -8px;
    left: -8px;
}

.theme-option {
    height: 97px;
    width: 78px;
    cursor: pointer;
}

.theme__icon {
    position: relative;
    width: 60px;
    height: 60px;
}

.theme__icon__inner {
    width: 50%;
    height: 50%;
    margin-top: 46%;
    margin-left: 25%;
    border-radius: 100%;
    position: relative;
}

.main {
    width: 100%;
}

.main h3 {
    font-weight: 500;
    font-size: 30px;
    text-align: center;
}

div.settings-wrap .section__inner {
    background-color: rgba(255, 255, 255, .5);
    border: 1px solid #e0e0e0;
    border-top: 0;
    border-left: 0;
    padding: 20px;
    border-radius: 3px;
    position: relative;
    margin: 83px;
     margin-top: 16px;
}

body.admin h5 {
    text-transform: uppercase;
    font-size: 13px;
    font-weight: 600;
    margin-bottom: 16px;
}
</style>