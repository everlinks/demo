    <section class="pricing py-5">
        <div class="container">
            <div class="row">
                <!-- Free Tier -->
                <div class="col-lg">
                    <div class="card mb-5 mb-lg-0">
                        <div class="card-body">
                            <h5 class="card-title text-muted text-uppercase text-center">Free</h5>
                            <h6 class="text-center">All the basics, for as long as you like.</h6>
                            <h6 class="card-price text-center">$0<span class="period">/month</span></h6>
                            <hr>
                            <h>Free includes all of the basic features to get you started:</h>
                            <ul class="fa-ul">
                                <li><span class="fa-li"><i class="fas fa-check"></i></span>Get unlimited links on your everlink.</li>
                                <li><span class="fa-li"><i class="fas fa-check"></i></span>See the total number of
                                    times each link has been clicked.</li>
                                <li><span class="fa-li"><i class="fas fa-check"></i></span> Pick from a selection of
                                    everlink themes.</li>

                            </ul>

                        </div>
                    </div>
                </div>
                <div class="col-lg">
                    <div class="card mb-6 mb-lg-0">
                        <div class="card-body">
                            <h5 class="card-title text-muted text-uppercase text-center">Pro</h5>
                            <h6 class="text-center">For users who want their links to work harder.</h6>

                            <h6 class="card-price text-center">$6<span class="period">/month</span></h6>
                            <hr>
                            <h>Includes everything in Free plus all this:</h>
                            <ul class="fa-ul">
                                <li><span class="fa-li"><i class="fas fa-check"></i></span>Get help quickly with
                                    priority support.</li>
                                <li><span class="fa-li"><i class="fas fa-check"></i></span>Capture your visitors email
                                    addresses with the email / newsletter signup integration.</li>
                                <li><span class="fa-li"><i class="fas fa-check"></i></span>More everlink themes.</li>
                                <li><span class="fa-li"><i class="fas fa-check"></i></span>Remove everlink branding.</li>
                                <li><span class="fa-li"><i class="fas fa-check"></i></span>See a day-by-day breakdown
                                    of link traffic.</li>
                                <li><span class="fa-li"><i class="fas fa-check"></i></span>Complete customization of
                                    your everlink colors, button styles and fonts.</li>
                                <li><span class="fa-li"><i class="fas fa-check"></i></span>Highlight your most
                                    important links with priority links.</li>

                            </ul>
                            <a href="<?php echo site_url('admin/pro/subscribe'); ?>" class="btn btn-block btn-gradient text-white text-uppercase">Get Everlink Pro</a>

                        </div>

                    </div>
                </div>
            </div>
</div>
    </section>
</div>
</div>

