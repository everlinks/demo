<div class="main">
    <div class="container">
        <input type="hidden" name="store" id="store" value="<?php echo base_url('/admin/upgrade/'); ?>" />
        <h2 id="who_message" class="card-title">Your subscription</h2>
        <p class="text-center">You're nearly there, complete the fields below to get access to the
            full suite of Everlink PRO features.</p>
        <div class="card person-card">
            <div class="card-body">
                <h5 class="text">Plan</h5>
                <!-- First row (on medium screen) -->
                <div class="row">
                    <div class="form-group col-sm-12">
                        <div class="text-1">
                            <h5 class="text-dark">
                                    <?php echo $this->session->userdata['real_name']; ?>
                                </h5>
                            <h6 class="text-warning">@
                                    <?php echo $this->session->userdata['username']; ?>
                                </h6>
                        </div>
                        <hr />
                        <div class="text-2">
                            <b class="text-dark">Plan</b>
                            <h6 class="text-warning">$6/month (USD)</h6>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body d-flex p-0">
                        <div class="col-md-6">
                            <div class="px-3 py-2">
                                <h5 class="text">Billing Details</h5>
                                <h6>CONTACT INFORMATION</h6>
                                <div class="text-1">
                                    <h3>Full Name</h3>
                                    <input id="real_name" type="text" name="real_name" class="form-control" placeholder="Real name" value="<?php echo $this->session->userdata['real_name']; ?>" required readonly>
                                    <div id="real_name_feedback" class="invalid-feedback">
                                        <?php echo isset($message) ? $message : NULL ; ?>
                                    </div>
                                </div>
                                <h3>Email</h3>
                                <input id="real_name" type="text" name="real_name" class="form-control" placeholder="your@email.com" value="<?php echo $this->session->userdata['email']; ?>" required readonly>
                                <div id="real_name_feedback" class="invalid-feedback">
                                    <?php echo isset($message) ? $message : NULL ; ?>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="px-3 py-2">
                                <h6>BILLING ADDRESS (OPTIONAL)</h6>
                                <div class="text-2">
                                    <input id="add1" type="text" name="addo" class="form-control" placeholder="Address Line 1">
                                    <div id="real_name_feedback" class="invalid-feedback">
                                        <?php echo isset($message) ? $message : NULL ; ?>
                                    </div>
                                    <input id="add2" type="text" name="addt" class="form-control" placeholder="Address Line 2">
                                    <div id="real_name_feedback" class="invalid-feedback">
                                        <?php echo isset($message) ? $message : NULL ; ?>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <input type="text" name="city" class="form-control" id="City" placeholder="Your City" data-rule="minlen:4">
                                                <div class="validation"></div>
                                            </div>
                                        </div>
                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <input type="text" name="state" class="form-control" id="State" placeholder="Your State" data-rule="minlen:4">
                                                <div class="validation"></div>
                                            </div>
                                        </div>
                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <input type="text" name="code" class="form-control" id="Postcode" placeholder="Postcode" data-rule="minlen:4">
                                                <div class="validation"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-9">
                    <div class="card">
                        <div class="card-body">
                            <h5 class="text">Payment</h5>
                            <h>Add a new card
                            </h>
                            <div class="col-sm-12">
                                <form action="<?= site_url('admin/upgrade'); ?>" method="POST">
                                    <input type="hidden" name="stripeToken" />
                                    <div class="group">
                                        <label>
                                          <span>Name</span>
                                          <input name="cardholder-name" class="field" placeholder="Jane Doe" />
                                        </label>
                                        <label>
                                          <span>Card</span>
                                          <div id="card-element" class="field"></div>
                                        </label>
                                        <button type="submit" class="btn btn-gradient oh-8">Pay $25</button>
                                        <div class="outcome">
                                          <div class="error"></div>
                                          <div class="success">
                                            Success! Your Stripe token is <span class="token"></span>
                                          </div>
                                        </div>
                                    </div>
                                </form>
                               <!--  <div class="form-check">
                                    <label class="form-check-label">
                                        <input type="checkbox" class="form-check-input" value="" name="remember">I agree to the Everlink PRO <a href="/pro/terms" target="_blank">Terms &amp; Conditions</a>
                                    </label>
                                </div> -->
                                <!-- <button type="button" id="say" class="btn btn-gradient text-white btn-block" data-toggle="modal" data-target="#basicModal">Change my card and SignUp</button> -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- basic modal -->
<div class="modal fade" id="basicModal" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel">Are you sure?</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p>This action will change your card and then signup with new card detail.</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                <button type="submit" class="btn" href="<?php echo site_url('admin/upgrade'); ?>">OK</button>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">

    window.onload =(function(){

jQuery.validator.addMethod("lettersonly", function(value, element) {
  return this.optional(element) || /^[a-z]+$/i.test(value);
}, "Letters only please"); 

jQuery.validator.addMethod("regex", function(value, element) {
  return this.optional(element) || /^[a-zA-Z0-9]+$/.test(value);
}, "Alphanumeric character only please"); 
$("#form1").validate({
// Specify the validation rules
rules:
    {
        addo: {
            
            regex:true
            
        },
        addt: {
            regex:true
        },

        city : {
            
            lettersonly: true
            
        },
         cvv : {
            
            number: true,
            required:true
            
        },
        exp_date_m : {
            
            number: true,
            required:true
            
        },
        exp_date_y : {
            
            number: true,
            required:true
            
        },
        
       

        state : {
           // required: true,
            lettersonly: true
            
        },
        code : {
            //required: true,
            number: true
        },

         cardname  : {
            required: true,
            lettersonly: true
            
        },
        cardeitals :{
         required: true,
         creditcard: true
         //number:true,


         

        },

        remember:
         "required" 
    },

    messages: {
   // email: "Please enter your Email", 
    remember: "Please accept our policy",
    regex: "Please enter only numbers"
}, 
errorElement:'div',
errorPlacement: function (error, element) {
    if (element.attr("type") == "checkbox") {
        //error.appendTo ( element.next());
         error.insertAfter(element.next());
         $('#basicModal').modal('hide');
    } 
    else
    {
       error.appendTo( element.next() );
      $('#basicModal').modal('hide');
       
    }
},
submitHandler: function(form) 
{   
    var email=$("#email").val();
    var store = $('#store').val();
    // var dataString = 'email=' + email+'&addo='+addo+'&addt='+'addt'+'&city' +city+ '&state=' +state + '&code=' +code+ '&cardname=' + cardname+ '&cardeitals='+ cardeitals;
    var dataString = $('#form1').serialize();
    $.ajax({
          type: 'POST',
          dataType: 'json',
          data: dataString,
         url: store+"savedata",
          success: function(data) {
              console.log("success");
              //location.reload(true);
              $('#basicModal').modal('hide');
              
          },
          error: function(data){
             console.log(data);
             
          }
      }); }




})
});

    window.onload = function () {

        var stripe = Stripe("<?= $this->config->item('stripe_publishable_key'); ?>");
        var elements = stripe.elements();

        var card = elements.create('card', {
          hidePostalCode: false,
          style: {
            base: {
              iconColor: '#F99A52',
              color: '#32315E',
              lineHeight: '48px',
              fontWeight: 400,
              fontFamily: '"Poppins", sans-serif',
              fontSize: '15px',

              '::placeholder': {
                color: '#CFD7DF',
              }
            },
          }
        });
        card.mount('#card-element');

        function setOutcome(result) {
          var successElement = document.querySelector('.success');
          var errorElement = document.querySelector('.error');
          successElement.classList.remove('visible');
          errorElement.classList.remove('visible');

          if (result.token) {
            // Use the token to create a charge or a customer
            // https://stripe.com/docs/charges
            // successElement.querySelector('.token').textContent = result.token.id;
            // successElement.classList.add('visible');
            
            var form = document.querySelector('form');
            form.querySelector('input[name="stripeToken"]').setAttribute('value', result.token.id);
            form.submit();

          } else if (result.error) {
            errorElement.textContent = result.error.message;
            errorElement.classList.add('visible');
          }
        }

        card.on('change', function(event) {
          setOutcome(event);
        });

        document.querySelector('form').addEventListener('submit', function(e) {
          e.preventDefault();
          var form = document.querySelector('form');
          var extraDetails = {
            name: form.querySelector('input[name=cardholder-name]').value,
          };
          stripe.createToken(card, extraDetails).then(setOutcome);
        });

    }
</script>
