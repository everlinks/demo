<div class="container">
    <div class="row">   

        <div class="col-md-12">
            <form action="<?= site_url('admin/pro/subscribe'); ?>" method="POST">
                <input type="hidden" name="stripeToken" />
                <div class="group">
                  <label>
                     <span>Name</span>
                     <input name="cardholder-name" class="field" placeholder="Jane Doe" />
                  </label>
                  <label>
                    <span>Card number</span>
                    <div id="card-number-element" class="field"></div>
                    <span class="brand"><i class="pf pf-credit-card" id="brand-icon"></i></span>
                  </label>
                  <label>
                    <span>Expiry date</span>
                    <div id="card-expiry-element" class="field"></div>
                  </label>
                  <label>
                    <span>CVC</span>
                    <div id="card-cvc-element" class="field"></div>
                  </label>
                  <label>
                    <span>Postal code</span>
                    <input id="postal-code" name="postal_code" class="field" placeholder="90210" />
                  </label>
                </div>
                <button type="submit">Pay $25</button>
                <div class="outcome">
                  <div class="error"></div>
                  <div class="success">
                    Success! Your Stripe token is <span class="token"></span>
                  </div>
                </div>
              </form>

        </div>

        <div class="col-md-4">
            
            <div class="card">
                <div class="card-header bg-success text-white">Product Information</div>
                <div class="card-body bg-light">
                    <?php if (validation_errors()): ?>
                        <div class="alert alert-danger" role="alert">
                            <strong>Oops!</strong>
                            <?php echo validation_errors() ;?> 
                        </div>  
                    <?php endif ?>
                    <div id="payment-errors"></div>  
                     <form method="post" id="paymentFrm" enctype="multipart/form-data" action="<?php echo base_url(); ?>admin/stripe">
                        <div class="form-group">
                            <input type="text" name="name" class="form-control" placeholder="Name" value="<?php echo set_value('name'); ?>" required>
                        </div>  

                        <div class="form-group">
                            <input type="email" name="email" class="form-control" placeholder="email@you.com" value="<?php echo set_value('email'); ?>" required />
                        </div>

                         <div class="form-group">
                            <input type="number" name="card_num" id="card_num" class="form-control" placeholder="Card Number" autocomplete="off" value="<?php echo set_value('card_num'); ?>" required>
                        </div>
                       
                        
                        <div class="row">

                            <div class="col-sm-8">
                                 <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <input type="text" name="exp_month" maxlength="2" class="form-control" id="card-expiry-month" placeholder="MM" value="<?php echo set_value('exp_month'); ?>" required>
                                        </div>
                                    </div>

                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <input type="text" name="exp_year" class="form-control" maxlength="4" id="card-expiry-year" placeholder="YYYY" required="" value="<?php echo set_value('exp_year'); ?>">
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-sm-4">
                                <div class="form-group">
                                    <input type="text" name="cvc" id="card-cvc" maxlength="3" class="form-control" autocomplete="off" placeholder="CVC" value="<?php echo set_value('cvc'); ?>" required>
                                </div>
                            </div>
                        </div>
                        

                       

                        <div class="form-group text-right">
                          <button class="btn btn-secondary" type="reset">Reset</button>
                          <button type="submit" id="payBtn" class="btn btn-success">Submit Payment</button>
                        </div>
                    </form>     
                </div>
            </div>
                 
        </div>

        <div class="col-md-4">
            <div class="card">
                <div class="card-header bg-info text-white">
                    Test credentials
                </div>
                <div class="card-body">
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th>Card </th>
                                <th>Brand</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td class="card-number"> 4242 4242 4242 4242 </td>
                                <td>Visa</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>    
        </div>

        <div class="col-md-4">
            <div class="card">
                <h6 class="card-header bg-primary text-white">
                    Some Help?
                </h6>
                <div class="card-body">
                    <p>Get some real help by browsing these guide from offical source.</p>
                    <ol>
                        <li> <a href="https://stripe.com/docs" target="_blank">Stripe Docs</a> </li>
                        <li> <a href="https://stripe.com/docs/checkout" target="_blank">Stripe Checkout</a></li>

                    </ol>
                </div>
            </div>
        </div>

    </div>
</div> 
</div>
<style type="text/css">
.main {
  padding: 20px 0;
}
.card {
    margin-top: 1em;
}
.person-card {
    margin-top: 5em;
    padding-top: 5em;
}
.person-card .card-title{
    text-align: center;
}
.person-card .person-img{
    width: 10em;
    position: absolute;
    top: -5em;
    left: 50%;
    margin-left: -5em;
    border-radius: 100%;
    overflow: hidden;
    background-color: white;
}
.invalid-feedback {
    display: block;
}
.group {
  background: white;
  box-shadow: 0 7px 14px 0 rgba(49, 49, 93, 0.10), 0 3px 6px 0 rgba(0, 0, 0, 0.08);
  border-radius: 4px;
  margin-bottom: 20px;
}

label {
  position: relative;
  color: #8898AA;
  font-weight: 300;
  height: 40px;
  line-height: 40px;
  margin-left: 20px;
  display: flex;
  flex-direction: row;
}

.group label:not(:last-child) {
  border-bottom: 1px solid #F0F5FA;
}

label > span {
  width: 120px;
  text-align: right;
  margin-right: 30px;
}

label > span.brand {
  width: 30px;
}

.field {
  background: transparent;
  font-weight: 300;
  border: 0;
  color: #31325F;
  outline: none;
  flex: 1;
  padding-right: 10px;
  padding-left: 10px;
  cursor: text;
}

.field::-webkit-input-placeholder {
  color: #CFD7E0;
}

.field::-moz-placeholder {
  color: #CFD7E0;
}

.outcome {
  float: left;
  width: 100%;
  padding-top: 8px;
  min-height: 24px;
  text-align: center;
}

.success,
.error {
  display: none;
  font-size: 13px;
}

.success.visible,
.error.visible {
  display: inline;
}

.error {
  color: #E4584C;
}

.success {
  color: #666EE8;
}

.success .token {
  font-weight: 500;
  font-size: 13px;
}

</style>
<link rel="stylesheet" href="http://localhost/codeigniter/everlink/assets/frameworks/paymentfont/css/paymentfont.min.css">
<!-- Stripe JavaScript library -->
<script type="text/javascript" src="https://js.stripe.com/v3/"></script>   
    
    <script type="text/javascript">
        //set your publishable key
        // Stripe.setPublishableKey("<?= $this->config->item('stripe_publishable_key'); ?>");
        
        // //callback to handle the response from stripe
        // function stripeResponseHandler(status, response) {
        //     if (response.error) {
        //         //enable the submit button
        //         $('#payBtn').removeAttr("disabled");
        //         //display the errors on the form
        //         // $('#payment-errors').attr('hidden', 'false');
        //         $('#payment-errors').addClass('alert alert-danger');
        //         $("#payment-errors").html(response.error.message);
        //     } else {
        //         var form$ = $("#paymentFrm");
        //         //get token id
        //         var token = response['id'];
        //         //insert the token into the form
        //         form$.append("<input type='hidden' name='stripeToken' value='" + token + "' />");
        //         //submit form to the server
        //         form$.get(0).submit();
        //     }
        // }
        window.onload = function () {

            var stripe = Stripe("<?= $this->config->item('stripe_publishable_key'); ?>");
            var elements = stripe.elements();

            var style = {
              base: {
                iconColor: '#666EE8',
                color: '#31325F',
                lineHeight: '40px',
                fontWeight: 300,
                fontFamily: 'Helvetica Neue',
                fontSize: '15px',

                '::placeholder': {
                  color: '#CFD7E0',
                },
              },
            };

            var cardNumberElement = elements.create('cardNumber', {
              style: style
            });
            cardNumberElement.mount('#card-number-element');

            var cardExpiryElement = elements.create('cardExpiry', {
              style: style
            });
            cardExpiryElement.mount('#card-expiry-element');

            var cardCvcElement = elements.create('cardCvc', {
              style: style
            });
            cardCvcElement.mount('#card-cvc-element');


            function setOutcome(result) {
              var successElement = document.querySelector('.success');
              var errorElement = document.querySelector('.error');
              successElement.classList.remove('visible');
              errorElement.classList.remove('visible');

              if (result.token) {
                // In this example, we're simply displaying the token
                // successElement.querySelector('.token').textContent = result.token.id;
                // successElement.classList.add('visible');

                // In a real integration, you'd submit the form with the token to your backend server
                var form = document.querySelector('form');
                form.querySelector('input[name="stripeToken"]').setAttribute('value', result.token.id);
                form.submit();
              } else if (result.error) {
                errorElement.textContent = result.error.message;
                errorElement.classList.add('visible');
              }
            }

            var cardBrandToPfClass = {
                'visa': 'pf-visa',
                  'mastercard': 'pf-mastercard',
                  'amex': 'pf-american-express',
                  'discover': 'pf-discover',
                  'diners': 'pf-diners',
                  'jcb': 'pf-jcb',
                  'unknown': 'pf-credit-card',
            }

            function setBrandIcon(brand) {
                var brandIconElement = document.getElementById('brand-icon');
              var pfClass = 'pf-credit-card';
              if (brand in cardBrandToPfClass) {
                pfClass = cardBrandToPfClass[brand];
              }
              for (var i = brandIconElement.classList.length - 1; i >= 0; i--) {
                brandIconElement.classList.remove(brandIconElement.classList[i]);
              }
              brandIconElement.classList.add('pf');
              brandIconElement.classList.add(pfClass);
            }

            cardNumberElement.on('change', function(event) {
                // Switch brand logo
                if (event.brand) {
                setBrandIcon(event.brand);
              }

                setOutcome(event);
            });

            document.querySelector('form').addEventListener('submit', function(e) {
              e.preventDefault();
              var options = {
                card_num: 'adasd',
                address_zip: document.getElementById('postal-code').value,
              };
              stripe.createToken(cardNumberElement, options).then(setOutcome);
            });


            // $(document).ready(function() {
            //     //on form submit
            //     $("#paymentFrm").submit(function(event) {
            //         //disable the submit button to prevent repeated clicks
            //         $('#payBtn').attr("disabled", "disabled");
                    
            //         //create single-use token to charge the user
            //         Stripe.createToken({
            //             number: $('#card_num').val(),
            //             cvc: $('#card-cvc').val(),
            //             exp_month: $('#card-expiry-month').val(),
            //             exp_year: $('#card-expiry-year').val()
            //         }, stripeResponseHandler);
                    
            //         //submit from callback
            //         return false;
            //     });
            // });
        }
    </script>
