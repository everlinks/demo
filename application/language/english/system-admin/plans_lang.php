<?php
/**
 * @package	CodeIgniter
 * @author	domProjects Dev Team
 * @copyright   Copyright (c) 2015, domProjects, Inc. (http://domProjects.com/)
 * @license http://opensource.org/licenses/MIT	MIT License
 * @link    http://domProjects.com
 * @since	Version 1.0.0
 * @filesource
 */
defined('BASEPATH') OR exit('No direct script access allowed');

$lang['plans_name']        = 'Plan Name';
$lang['plans_amount'] = 'Amount';
$lang['plans_interval']      = 'Interval';
// $lang['support_create']      = 'Create Category';
// $lang['support_edit_support']  = 'Edit Category';
// $lang['support_edit_group']  = 'Edit group';
 $lang['plans_create']      = 'Create Plans';
// $lang['support_color']       = 'Color';
