<?php
/**
 * @package	CodeIgniter
 * @author	domProjects Dev Team
 * @copyright   Copyright (c) 2015, domProjects, Inc. (http://domProjects.com/)
 * @license http://opensource.org/licenses/MIT	MIT License
 * @link    http://domProjects.com
 * @since	Version 1.0.0
 * @filesource
 */
defined('BASEPATH') OR exit('No direct script access allowed');

$lang['article_name']        = 'Name';
$lang['article_description'] = 'Short Description';
$lang['article_action']      = 'Action';
$lang['article_edit_article']  = 'Edit article';
$lang['article_create']      = 'Create article';
$lang['article_color']       = 'Color';
