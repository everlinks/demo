<?php
/**
 * @package	CodeIgniter
 * @author	domProjects Dev Team
 * @copyright   Copyright (c) 2015, domProjects, Inc. (http://domProjects.com/)
 * @license http://opensource.org/licenses/MIT	MIT License
 * @link    http://domProjects.com
 * @since	Version 1.0.0
 * @filesource
 */
defined('BASEPATH') OR exit('No direct script access allowed');

$lang['features_category']        = 'category';
$lang['features_name']        = 'title';
$lang['features_description'] = 'short description';
$lang['features_descript'] = 'full description';
$lang['features_answers']      = 'answers';
$lang['features_edit_article']  = 'Edit features';
$lang['features_create']      = 'Create features';
$lang['features_color']       = 'Color';
$lang['features_options']       = 'options';
