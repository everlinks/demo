<?php
/**
 * @package	CodeIgniter
 * @author	domProjects Dev Team
 * @copyright   Copyright (c) 2015, domProjects, Inc. (http://domProjects.com/)
 * @license http://opensource.org/licenses/MIT	MIT License
 * @link    http://domProjects.com
 * @since	Version 1.0.0
 * @filesource
 */
defined('BASEPATH') OR exit('No direct script access allowed');

$lang['support_name']        = 'Name';
$lang['support_description'] = 'Description';
$lang['support_action']      = 'FAQ';
$lang['support_create']      = 'Create Category';
$lang['support_edit_support']  = 'Edit Category';
// $lang['support_edit_group']  = 'Edit group';
// $lang['support_create']      = 'Create group';
// $lang['support_color']       = 'Color';
