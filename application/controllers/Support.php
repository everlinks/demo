<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Support extends Public_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->data['footer'] = true;
    }

  	public function index()
  	{

            $this->template->support_render('support/support', $this->data);
            // $this->load->view('public/home', $this->data);
           
    }

    public function feature($id=null)
    {
        if($id!=null){
            $this->template->support_render('support/priority', $this->data);
         
        }else{
            $this->template->support_render('support/feature', $this->data);
            // $this->load->view('public/home', $this->data);
        }
    }
    
    public function started($id=null)
    {
        if($id!=null){
            $this->template->support_render('support/started', $this->data);
         
        }else{
            $this->template->support_render('support/started', $this->data);
            // $this->load->view('public/home', $this->data);
        }
    }

    public function email($id=null)
    {
        if($id!=null){
            $this->template->support_render('support/email', $this->data);
         
        }else{
            $this->template->support_render('support/feature', $this->data);
            // $this->load->view('public/home', $this->data);
        }
    }
        
    public function statistics($id=null)
    {
        if($id!=null){
            $this->template->support_render('support/statistics', $this->data);
         
        }else{
            $this->template->support_render('support/feature', $this->data);
            // $this->load->view('public/home', $this->data);
        }
    }
}  

