<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends Public_Controller {

    public function __construct()
    {
        parent::__construct();
    }


  	public function index()
    {
        /* Load Template */
        $this->template->public_render('public/home', $this->data);
    }

    public function user($uname = null)
  	{
        // 
        if($uname==null){
            redirect('/');
        } else {
            $data = array();
            $this->load->model('public/user_model');
            $data['user'] = $this->user_model->get_user_record($uname);
            //user theme
            $this->load->model('admin/settings_model');
            $data['user_theme'] = $this->settings_model->get_user_theme($data['user']->id);


            if($data['user']===false){
                $this->output->set_status_header('404');
                $data['heading'] = 'Everlink';
                $data['message'] = 'The page you’re looking for doesn’t exist';
                $this->load->view('errors/html/error_404', $data);
            } else {
                if($this->input->method() === 'get')
                    $this->load->view('public/_templates/user_header', $data);

                //theme_name
                $data['links'] = $this->user_model->get_links($data['user']->id);
                //$data['theme'] = $this->settings_model->get_theme($data['user_theme']->theme_id);

                /* Load Template */
                $this->load->view('public/user', $data);

                if($this->input->method() === 'get')
                    $this->load->view('public/_templates/user_footer', $data);
            }
        }
    }

    public function instagram()
    {
        if($this->ion_auth->logged_in()){
            if($this->ion_auth->is_admin()){
                redirect('/system-admin');
            } else {
                redirect('/admin');
            }
        } else {
            redirect($this->instagram_api->instagram_login());
        }
    }

    public function verify(){

        if($this->ion_auth->logged_in()){
            if($this->ion_auth->is_admin()){
                redirect('/system-admin');
            } else {
                redirect('/admin');
            }
            return true;
        }

        // Make sure that there is a GET variable of code
        if(isset($_GET['code']) && $_GET['code'] != '') {
            
            $auth_response = $this->instagram_api->authorize($_GET['code']);

            if (isset($auth_response->code) && $auth_response->code === 400 ){

                $this->session->set_flashdata('message', $auth_response->error_message);
                redirect('/', 'refresh');

            } else {

              $instagram_id = $auth_response->user->id;

              $data = array(
                  'username' => $auth_response->user->username,
                  'real_name'  => $auth_response->user->full_name,
                  'profile_picture' => $auth_response->user->profile_picture,
                  'access_token'=> $auth_response->access_token
              );

              if($this->ion_auth->identity_check($instagram_id))
              {
                $this->ion_auth->login($instagram_id, $data);
              } 
              else 
              {
                if($this->ion_auth->register($instagram_id, $data, array())){
                  $this->ion_auth->login($instagram_id);
                }
              }
              if ($this->session->userdata['email'] && !empty($this->session->userdata['email'])) {
                   redirect('/admin');
              }
              else
              {
                 redirect('/admin/get_started');
              }
              

            }
                       
        }
    }

    public function showimage()
    {
        echo $targetfolder = base_url()."/upload/";


        echo base_url();
        $string='https://scontent.cdninstagram.com/vp/1b287e92c4760d85ead5b560fe443730/5C1D8016/t51.2885-19/s150x150/13636135_1591339594496783_678856338_a.jpg';
        //   $this->save_image($string,$targetfolder.'image.jpg');
        $uploadfile = $_SERVER['DOCUMENT_ROOT'] . $string;//'/sites/bestinfo/images/news/CodeCogsEqn.gif';
        //  exit;
        // $imgurl = 'http://www.foodtest.ru/images/big_img/sausage_3.jpg';
        //$imagename= basename($string);
        ////if(file_exists('./tmp/'.$imagename)){continue;}
        //$image = $this->getimg($imgurl);
        //print_r(file_put_contents('tmp/'.$imagename,$image));
        //             $image_array=explode("/", $string);
        //
        //          //  print_r($image_array);
        //             $image_name_with_extention= $image_array[count($image_array)-1];
        //             copy($string, $targetfolder.'/'.$image_name_with_extention);
        //          //  print_r("image Name=> ");
        //           $image_name_array= explode(".", $image_name_with_extention);
        ////           $image = new SimpleImage();
        ////$image->load($string);
        ////$image->resize(250,400);
        ////$image->save('filename.jpg');
        //         //  echo $image_name_array[0];
        //           echo   $targetfolder = $targetfolder . $image_name_with_extention;//$image_name_array[0];

        move_uploaded_file($uploadfile, $targetfolder);
        echo "Upload";
        // }
        exit;
    }

    function getimg($url)
    {
        $headers[] = 'Accept: image/gif, image/x-bitmap, image/jpeg, image/pjpeg';
        $headers[] = 'Connection: Keep-Alive';
        $headers[] = 'Content-type: application/x-www-form-urlencoded;charset=UTF-8';
        $user_agent = 'php';
        $process = curl_init($url);
        curl_setopt($process, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($process, CURLOPT_HEADER, 0);
        curl_setopt($process, CURLOPT_USERAGENT, $user_agent); //check here
        curl_setopt($process, CURLOPT_TIMEOUT, 30);
        curl_setopt($process, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($process, CURLOPT_FOLLOWLOCATION, 1);
        $return = curl_exec($process);
        curl_close($process);
        return $return;
    }


//======================================//

    public function save_image($inPath,$outPath)
    { //Download images from remote server
        $in=    fopen($inPath, "rb");
        $out=   fopen($outPath, "wb");
        while ($chunk = fread($in,8192))
        {
            fwrite($out, $chunk, 8192);
        }
        fclose($in);
        fclose($out);
    }

}
