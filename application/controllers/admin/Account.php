<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Account extends Public_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->data['main_header'] = true;
    }

    public function index()
    {
        if ( ! $this->ion_auth->logged_in() OR ! $this->ion_auth->is_user())
        {
            redirect('/', 'refresh');
        }
        else
        {
            $this->load->model('admin/Upd_Model'); // First load the model
            if (isset($_POST) && !empty($_POST))
            {
                $data = array
                (
                  'real_name' => $this->input->post('real_name'),
                  'email' => $this->input->post('email'),
                  'instagram_id' => $this->session->userdata['instagram_id']
                );

                if($this->Upd_Model->update_title($data))
                {   
                    $email=$this->input->post('email');
                    $this->session->unset_userdata('email');
                    $this->session->set_userdata('email',$email);
                    $this->data['message'] = 'Updated successfully';
                }
                else
                {
                    $this->data['message'] = 'Error message';
                }
            
            }

            /* Load Template */
            $this->template->user_render('admin/account', $this->data);
            // $this->load->view('public/home', $this->data);
        }
    }
}