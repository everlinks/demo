<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends Public_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->data['main_header'] = true;
        $this->data['preview'] = true;
        $this->load->model('admin/dashboard_model');
    }

  	public function index()
  	{
        if ( ! $this->ion_auth->logged_in() OR ! $this->ion_auth->is_user())
        {
            redirect('/', 'refresh');
        }
        else
        {
            $user = $this->session->userdata['user_id'];
            $this->data['links'] = $this->dashboard_model->get_links($user);

            /* Load Template */
            $this->template->user_render('admin/main', $this->data);
            // $this->load->view('public/home', $this->data);
        }
    }

    public function storeLink()
    {
        $user = $this->session->userdata['user_id'];
        if ($user) {
            $response = array();
            
            $data = array(
                'user_id' => $user,
                'created_on' => time(),
                'position'  => 0,
                'active'  => 0
            );

            $result = $this->dashboard_model->insert_link($data);
            if($result) {
                $response['status'] = true;
                $response['id'] = $result;
            } else {
                $response['status'] = false;
                $response['message'] = 'Error';
            }
            echo json_encode($response);
        }
        return;
    }

    public function removeLink()
    {
        $user = $this->session->userdata['user_id'];
        if ($user) {
            $response = array();

            
            $data = array(
                'user_id' => $user,
                'id' => $this->input->post('id'),
                'deleted' => 1
            );

            $result = $this->dashboard_model->remove_link($data);
            if($result) {
                $response['status'] = true;
            } else {
                $response['status'] = false;
                $response['message'] = 'Error';
            }
            echo json_encode($response);
        }
        return;
    }

    // input-data-store
    public function storeData()
    {
        $user = $this->session->userdata['user_id'];
        if ($user) {
            $response = array();

            $data = array(
                $this->input->post('type') => $this->input->post('value')
            );

            $id = $this->input->post('id');

            $result = $this->dashboard_model->insert_link_data($data, $id);
            if($result) {
                $response['status'] = true;
            } else {
                $response['status'] = false;
                $response['message'] = 'Error';
            }
            echo json_encode($response);
        }
        return;
    }

     public function updatePosition()
     {
        $user = $this->session->userdata['user_id'];
        if ($user) {
            $data = array();
            foreach ($this->input->post('position') as $key => $value) {
                $data[] = array('id' => $value['id'], 'position' => $key);
            }
            $result = $this->dashboard_model->update_link_position($data);
            if($result) {
                $response['status'] = true;
            } else {
                $response['status'] = false;
                $response['message'] = 'Error';
            }
            echo json_encode($response);
        }
        return;

     }
}