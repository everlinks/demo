<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Get_started extends Public_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->data['main_header'] = true;
    }

  	public function index()
  	{
        if ( ! $this->ion_auth->logged_in() OR ! $this->ion_auth->is_user())
        {
            redirect('/', 'refresh');
        }
        else
        {
            $this->data['user'] = $this->session->userdata['username'];

            /* Load Template */
            $this->template->user_render('admin/get_started', $this->data);
            //$this->load->view('admin/get_started', $this->data);
        }
    }

    public function update_mail()
    {
        $id=$this->session->userdata['user_id'];
        $email = $this->input->post('email');
       
        

       
        $data = array(
                'email' => $email,
               
            );

        $response = array();

        
            $this->load->model('admin/Get_started_model');
            $result=$this->Get_started_model->update_mail($id,$data);

            if($result) {
                $response['status'] = true;
                $response['redirect'] = site_url('/admin');
            } else {
                $response['status'] = false;
                $response['message'] = 'Error';
            }
            echo json_encode($response);
           

        }

            
    }

  
