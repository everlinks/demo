<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pro extends Public_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->data['main_header'] = true;
    }

  	public function index()
  	{
        if ( ! $this->ion_auth->logged_in() OR ! $this->ion_auth->is_user())
        {
            redirect('/', 'refresh');
        }
        else
        {
            /* Load Template */
            $this->template->user_render('admin/pro', $this->data);
            // $this->load->view('public/home', $this->data);
        }
    }

    public function subscribe()
    {
        if ( ! $this->ion_auth->logged_in() OR ! $this->ion_auth->is_user())
        {
            redirect('/', 'refresh');
        }
        else
        {
            $this->load->config('stripe_api');
            if (isset($_POST) && ! empty($_POST))
            {
                echo "<pre>";
                print_r($_POST);
                exit;
                if(!empty($_POST['stripeToken'])){
                    //get token, card and user info from the form
                    $token  = $_POST['stripeToken'];
                    // $name = $_POST['name'];
                    // $email = $_POST['email'];
                    // $card_num = $_POST['card_num'];
                    // $card_cvc = $_POST['cvc'];
                    // $card_exp_month = $_POST['exp_month'];
                    // $card_exp_year = $_POST['exp_year'];
                    
                    //include Stripe PHP library
                    require_once(APPPATH . 'third_party/stripe/init.php');

                    
                    
                    //set api key
                    $stripe = array(
                      "secret_key"      => $this->config->item('stripe_secret_key'),
                      "publishable_key" => $this->config->item('stripe_publishable_key')
                    );
                    
                    \Stripe\Stripe::setApiKey($stripe['secret_key']);
                    
                    //add customer to stripe
                    $customer = \Stripe\Customer::create(array(
                        'email' => $email,
                        'source'  => $token
                    ));
                    
                    //item information
                    $itemName = "Premium Script CodexWorld";
                    $itemNumber = "PS123456";
                    $itemPrice = 55;
                    $currency = "usd";
                    $orderID = "SKA92712382139";
                    
                    //charge a credit or a debit card
                    $charge = \Stripe\Charge::create(array(
                        'customer' => $customer->id,
                        'amount'   => $itemPrice,
                        'currency' => $currency,
                        'description' => $itemName,
                        'metadata' => array(
                            'order_id' => $orderID
                        )
                    ));

                    echo "<pre>";
                    print_r($charge);
                    exit;
                    
                    //retrieve charge details
                    $chargeJson = $charge->jsonSerialize();

                    //check whether the charge is successful
                    if($chargeJson['amount_refunded'] == 0 && empty($chargeJson['failure_code']) && $chargeJson['paid'] == 1 && $chargeJson['captured'] == 1){

                        //order details 
                        $amount = $chargeJson['amount'];
                        $balance_transaction = $chargeJson['balance_transaction'];
                        $currency = $chargeJson['currency'];
                        $status = $chargeJson['status'];
                        $date = date("Y-m-d H:i:s");

                        //insert tansaction data into the database
                        $dataDB = array(
                            'name' => $name,
                            'email' => $email, 
                            'card_num' => $card_num, 
                            'card_cvc' => $card_cvc, 
                            'card_exp_month' => $card_exp_month, 
                            'card_exp_year' => $card_exp_year, 
                            'item_name' => $itemName, 
                            'item_number' => $itemNumber, 
                            'item_price' => $itemPrice, 
                            'item_price_currency' => $currency, 
                            'paid_amount' => $amount, 
                            'paid_amount_currency' => $currency, 
                            'txn_id' => $balance_transaction, 
                            'payment_status' => $status,
                            'created' => $date,
                            'modified' => $date
                        );

                        if ($this->db->insert('payments', $dataDB)) {
                            if($this->db->insert_id() && $status == 'succeeded'){
                                $data['insertID'] = $this->db->insert_id();
                                $this->template->user_render('admin/stripe_success', $data);
                                // redirect('Welcome/payment_success','refresh');
                            }else{
                                echo "Transaction has been failed";
                            }
                        }
                        else
                        {
                            echo "not inserted. Transaction has been failed";
                        }




                        
                        //insert tansaction data into the database
                        // $sql = "INSERT INTO orders(name,email,card_num,card_cvc,card_exp_month,card_exp_year,item_name,item_number,item_price,item_price_currency,paid_amount,paid_amount_currency,txn_id,payment_status,created,modified) VALUES('".$name."','".$email."','".$card_num."','".$card_cvc."','".$card_exp_month."','".$card_exp_year."','".$itemName."','".$itemNumber."','".$itemPrice."','".$currency."','".$amount."','".$currency."','".$balance_transaction."','".$status."','".$date."','".$date."')";
                        // $insert = $db->query($sql);
                        // $last_insert_id = $db->insert_id;
                        
                        // //if order inserted successfully
                        // if($last_insert_id && $status == 'succeeded'){
                        //     $statusMsg = "<h2>The transaction was successful.</h2><h4>Order ID: {$last_insert_id}</h4>";
                        // }else{
                        //     $statusMsg = "Transaction has been failed";
                        // }
                    }else{
                        $statusMsg = "Transaction has been failed";
                    }
                }else{
                    $statusMsg = "Form submission error.......";
                }

                //show success or error message
                echo $statusMsg;
            }

            /* Load Template */
            $this->template->user_render('admin/subscribe', $this->data);
            // $this->load->view('public/home', $this->data);
        }
    }
    public function payment_success()
    {
        $this->load->view('payment_success');
    }
    public function payment_error()
    {
        $this->load->view('payment_error');
    }
}