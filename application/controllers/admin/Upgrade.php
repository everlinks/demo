<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Upgrade extends MY_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->data['main_header'] = true;
        $this->load->database();
    }

  	public function index()
  	{
        if ( ! $this->ion_auth->logged_in() OR ! $this->ion_auth->is_user())
        {
            redirect('/', 'refresh');
        }
        else
        {
            $this->load->config('stripe_api');
            if (isset($_POST) && ! empty($_POST))
            {
                if(!empty($_POST['stripeToken'])){
                    //get token, card and user info from the form
                    $token  = $_POST['stripeToken'];
                    // $name = $_POST['name'];
                    // $email = $_POST['email'];
                    // $card_num = $_POST['card_num'];
                    // $card_cvc = $_POST['cvc'];
                    // $card_exp_month = $_POST['exp_month'];
                    // $card_exp_year = $_POST['exp_year'];
                    
                    //include Stripe PHP library
                    require_once(APPPATH . 'third_party/stripe/init.php');

                    
                    
                    //set api key
                    $stripe = array(
                      "secret_key"      => $this->config->item('stripe_secret_key'),
                      "publishable_key" => $this->config->item('stripe_publishable_key')
                    );
                    
                    \Stripe\Stripe::setApiKey($stripe['secret_key']);
                    
                    // //add customer to stripe
                    // $customer = \Stripe\Customer::create(array(
                    //     'email' => $email,
                    //     'source'  => $token
                    // ));
                    
                    //item information
                    $itemName = "Premium Script CodexWorld";
                    $itemNumber = "PS123456";
                    $itemPrice = 600;
                    $currency = "usd";
                    $orderID = "SKA92712382139";
                    
                    //charge a credit or a debit card
                    $charge = \Stripe\Charge::create(array(
                        'amount'   => $itemPrice,
                        'currency' => $currency,
                        'description' => $itemName,
                        'source' => $token,
                        'metadata' => array(
                            'order_id' => $orderID
                        ),
                    ));

                    echo "<pre>";
                    print_r($charge);
                    exit;
                    
                    //retrieve charge details
                    $chargeJson = $charge->jsonSerialize();

                    //check whether the charge is successful
                    if($chargeJson['amount_refunded'] == 0 && empty($chargeJson['failure_code']) && $chargeJson['paid'] == 1 && $chargeJson['captured'] == 1){

                        //order details 
                        $amount = $chargeJson['amount'];
                        $balance_transaction = $chargeJson['balance_transaction'];
                        $currency = $chargeJson['currency'];
                        $status = $chargeJson['status'];
                        $date = date("Y-m-d H:i:s");

                        //insert tansaction data into the database
                        $dataDB = array(
                            'name' => $name,
                            'email' => $email, 
                            'card_num' => $card_num, 
                            'card_cvc' => $card_cvc, 
                            'card_exp_month' => $card_exp_month, 
                            'card_exp_year' => $card_exp_year, 
                            'item_name' => $itemName, 
                            'item_number' => $itemNumber, 
                            'item_price' => $itemPrice, 
                            'item_price_currency' => $currency, 
                            'paid_amount' => $amount, 
                            'paid_amount_currency' => $currency, 
                            'txn_id' => $balance_transaction, 
                            'payment_status' => $status,
                            'created' => $date,
                            'modified' => $date
                        );

                        if ($this->db->insert('payments', $dataDB)) {
                            if($this->db->insert_id() && $status == 'succeeded'){
                                $data['insertID'] = $this->db->insert_id();
                                $this->template->user_render('admin/stripe_success', $data);
                                // redirect('Welcome/payment_success','refresh');
                            }else{
                                echo "Transaction has been failed";
                            }
                        }
                        else
                        {
                            echo "not inserted. Transaction has been failed";
                        }

                    }else{
                        $statusMsg = "Transaction has been failed";
                    }
                }else{
                    $statusMsg = "Form submission error.......";
                }

                //show success or error message
                echo $statusMsg;
            }

            /* Load Template */
            $this->template->user_render('admin/upgrade', $this->data);
            // $this->load->view('public/home', $this->data);
        }
        
    }

    public function payment_success()
    {
        $this->load->view('payment_success');
    }
    public function payment_error()
    {
        $this->load->view('payment_error');
    }

    public function savedata(){

      
        if ( ! $this->ion_auth->logged_in() OR ! $this->ion_auth->is_admin()){
        }

        if (!empty($_POST)) {  
            $addo = $this->input->post('addo');
            $addt = $this->input->post('addt');
            $city = $this->input->post('city');
            $state = $this->input->post('state');
            $code = $this->input->post('code');        
            $cardname = $this->input->post('cardname');
            $cardeitals = $this->input->post('cardeitals');
        
        // if ($addo != NULL && $addt != NULL  && $city != NULL  && $state != NULL  && $code != NULL  && $cardname != NULL && $cardeitals != NULL)  {
          
            $data = array(
              
                'cardname' => $this->input->post('cardname'),
                'cardeitals' => $this->input->post('cardeitals')
            );
            $data1 = array(
                'addo' => $this->input->post('addo'),
                'addt' => $this->input->post('addt'),
                'city' => $this->input->post('city'),
                'state' => $this->input->post('state'),
              
                
            );
            $data2 = array(
                'code' => $this->input->post('code'),
            );
          
            // $this->load->model('admin/upgrade_model');
          
            // if($this->upgrade_model->add_letter($data)){
              
            // } 
            // if($this->upgrade_model->addition($data2)){
              
            // } if($this->upgrade_model->lets_add($data1)){
            //     redirect('admin/upgrade', 'refresh');
            // }
            //     else{
            //     echo 'false';
            // }

                $this->load->model('admin/upgrade_model');
               if($this->upgrade_model->add_user_details())
               {
                 //redirect('admin/upgrade', 'refresh');

                

               }


        }
        else {
            print_r("error");
        }
        exit;
	}
}

    
