<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Settings extends Public_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->data['main_header'] = true;
        $this->data['preview'] = true;
        $this->data['user'] = $this->session->userdata['user_id'];
    }

  	public function index()
  	{
        if ( ! $this->ion_auth->logged_in() OR ! $this->ion_auth->is_user())
        {
            redirect('/', 'refresh');
        }
        else
        {
            /* Load Template */
            $this->template->user_render('admin/settings', $this->data);
            // $this->load->view('public/home', $this->data);
        }
    }

    public function theme_id_update()
    {
     $id=$this->session->userdata['user_id'];
      $theme_id = $this->input->post('theme_id');

     $data = array(
                'theme_id' => $theme_id
               
            );
               
            $this->load->model('admin/Settings_model');
            $result=$this->Settings_model->theme_id_update($id,$data);


    }


}