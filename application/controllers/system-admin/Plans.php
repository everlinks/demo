<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Plans extends Admin_Controller {

    public function __construct()
    {
        parent::__construct();

        $this->lang->load('system-admin/plans');

        $this->load->model('system-admin/plans_model');

        /* Title Page :: Common */
        $this->page_title->push(lang('menu_plans_create'));
        $this->data['pagetitle'] = $this->page_title->show();

        /* Breadcrumbs :: Common */
        $this->breadcrumbs->unshift(1, lang('menu_plans_list'), 'system-admin/plans');
    }


	public function index()
	{
        if ( ! $this->ion_auth->logged_in() OR ! $this->ion_auth->is_admin())
        {
            redirect('auth/login', 'refresh');
        }
        else
        {
            /* Breadcrumbs */
         $this->data['breadcrumb'] = $this->breadcrumbs->show();

         $this->data['categories'] = $this->plans_model->plans();

            // print_r($this->data['categories']);
            // exit;

            /* Load Template */
            $this->template->admin_render('system-admin/plans/plans', $this->data);
        }
    }


	public function create()
	{
		if ( ! $this->ion_auth->logged_in() OR ! $this->ion_auth->is_admin())
		{
			redirect('auth', 'refresh');
		}

        /* Breadcrumbs */
        $this->breadcrumbs->unshift(2, lang('menu_groups_create'), 'system-admin/plans/create');
        $this->data['breadcrumb'] = $this->breadcrumbs->show();



		/* Validate form input */
		$this->form_validation->set_rules('plans_name', 'plans_name', 'required|alpha_dash');
		$this->form_validation->set_rules('amount', 'amount', 'required|alpha_dash');
		$this->form_validation->set_rules('intervals', 'intervals', 'required|alpha_dash');



		if ($this->form_validation->run() == TRUE)
		{



			$new_group_id = $this->plans_model->create_plans($this->input->post('plans_name'), $this->input->post('amount'),$this->input->post('intervals'));
			if ($new_group_id)
			{
				$this->session->set_flashdata('message', $this->ion_auth->messages());
			
            }
            redirect('system-admin/plans', 'refresh');
		}
		else
		{
            $this->data['message'] = (validation_errors() ? validation_errors() : ($this->ion_auth->errors() ? $this->ion_auth->errors() : $this->session->flashdata('message')));

			$this->data['plans_name'] = array(
				'name'  => 'plans_name',
				'id'    => 'plans_name',
				'type'  => 'text',
                'class' => 'form-control',
				'value' => $this->form_validation->set_value('plans_name')
			);
			$this->data['amount'] = array(
				'name'  => 'amount',
				'id'    => 'amount',
				'type'  => 'text',
                'class' => 'form-control',
				'value' => $this->form_validation->set_value('amount')
			);

			$this->data['intervals'] = array(
				'name'  => 'intervals',
				'id'    => 'intervals',
				'type'  => 'text',
                'class' => 'form-control',
				'value' => $this->form_validation->set_value('intervals')
			);

            /* Load Template */
            $this->template->admin_render('system-admin/plans/create', $this->data);
		}
    }

    
	public function edit_plan($id)
	{
		if ( ! $this->ion_auth->logged_in() OR ! $this->ion_auth->is_admin() OR ! $id OR empty($id))
		{
			redirect('auth', 'refresh');
		}

        /* Breadcrumbs */
        $this->breadcrumbs->unshift(2, lang('menu_plans_edit'), 'system-admin/plans/edit_plan');
        $this->data['breadcrumb'] = $this->breadcrumbs->show();

        /* Variables */
		$plans = $this->plans_model->get_plans($id);

		/* Validate form input */
        $this->form_validation->set_rules('plans_name', 'plans_name', 'required|alpha_dash');
        $this->form_validation->set_rules('amount', 'amount', 'required|alpha_dash');
        $this->form_validation->set_rules('intervals', 'intervals', 'required|alpha_dash');

		if (isset($_POST) && !empty($_POST))
		{
            if ($this->form_validation->run() == TRUE)
			{
                $data = array(
                    'id' => $id,
                    'plan_name' => $this->input->post('plans_name'),
                    'amount' => $this->input->post('amount'), 
                    'intervals' => $this->input->post('intervals')
                );
                
				$plans_update = $this->plans_model->update_plans($id, $data);

				if ($plans_update)
				{
					$this->session->set_flashdata('message', $this->lang->line('edit_group_saved'));
				}
				else
				{
					$this->session->set_flashdata('message', $this->ion_auth->errors());
				}

				redirect('system-admin/plans', 'refresh');
			}
		}

        $this->data['message'] = (validation_errors() ? validation_errors() : ($this->ion_auth->errors() ? $this->ion_auth->errors() : $this->session->flashdata('message')));
        $this->data['group']   = $plans;


		//$readonly = 'miami' === $plans->name ? 'readonly' : '';

		$this->data['plans_name'] = array(
			'type'    => 'text',
			'name'    => 'plans_name',
			'id'      => 'plans_name',
			'value'   => $this->form_validation->set_value('plans_name', $plans->plan_name),
            'class'   => 'form-control',
			//$readonly => $readonly
		);

		$this->data['amount'] = array(
			'type'  => 'text',
			'name'  => 'amount',
			'id'    => 'amount',
			'value' => $this->form_validation->set_value('amount', $plans->amount),
            'class' => 'form-control'
        );

        $this->data['intervals'] = array(
			'type'  => 'text',
			'name'  => 'intervals',
			'id'    => 'intervals',
			'value' => $this->form_validation->set_value('intervals', $plans->intervals),
            'class' => 'form-control'
        );
	

        /* Load Template */
        $this->template->admin_render('system-admin/plans/edit_plan', $this->data);
	}


	public function delete_plan($id)
	{
	
		if($this->db->delete('plans', array('id' => $id)))
		{

			redirect('system-admin/plans', 'refresh');
		}

	}



}




