<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Support extends Admin_Controller {

    public function __construct()
    {
        parent::__construct();

        $this->lang->load('system-admin/support');

        $this->load->model('system-admin/support_model');

        /* Title Page :: Common */
        $this->page_title->push(lang('menu_security_support'));
        $this->data['pagetitle'] = $this->page_title->show();

        /* Breadcrumbs :: Common */
        $this->breadcrumbs->unshift(1, lang('menu_security_support'), 'system-admin/support');
    }


	public function index()
	{
        if ( ! $this->ion_auth->logged_in() OR ! $this->ion_auth->is_admin())
        {
            redirect('auth/login', 'refresh');
        }
        else
        {
            /* Breadcrumbs */
            $this->data['breadcrumb'] = $this->breadcrumbs->show();

            $this->data['categories'] = $this->support_model->support();

            // print_r($this->data['categories']);
            // exit;

            /* Load Template */
            $this->template->admin_render('system-admin/support/index', $this->data);
        }
    }


	public function create()
	{
		if ( ! $this->ion_auth->logged_in() OR ! $this->ion_auth->is_admin())
		{
			redirect('auth', 'refresh');
		}

        /* Breadcrumbs */
        $this->breadcrumbs->unshift(2, lang('menu_groups_create'), 'system-admin/support/create');
        $this->data['breadcrumb'] = $this->breadcrumbs->show();

		/* Validate form input */
		$this->form_validation->set_rules('support_name', 'lang:create_group_validation_name_label', 'required|alpha_dash');

		if ($this->form_validation->run() == TRUE)
		{
			$new_group_id = $this->support_model->create_support($this->input->post('support_name'), $this->input->post('support_description'));
			if ($new_group_id)
			{
				$this->session->set_flashdata('message', $this->ion_auth->messages());
			
            }
            redirect('system-admin/support', 'refresh');
		}
		else
		{
            $this->data['message'] = (validation_errors() ? validation_errors() : ($this->ion_auth->errors() ? $this->ion_auth->errors() : $this->session->flashdata('message')));

			$this->data['support_name'] = array(
				'name'  => 'support_name',
				'id'    => 'support_name',
				'type'  => 'text',
                'class' => 'form-control',
				'value' => $this->form_validation->set_value('support_name')
			);
			$this->data['support_description'] = array(
				'name'  => 'support_description',
				'id'    => 'support_description',
				'type'  => 'text',
                'class' => 'form-control',
				'value' => $this->form_validation->set_value('support_description')
			);

            /* Load Template */
            $this->template->admin_render('system-admin/support/create', $this->data);
		}
    }

    
	public function edit($id)
	{
		if ( ! $this->ion_auth->logged_in() OR ! $this->ion_auth->is_admin() OR ! $id OR empty($id))
		{
			redirect('auth', 'refresh');
		}

        /* Breadcrumbs */
        $this->breadcrumbs->unshift(2, lang('menu_groups_edit'), 'system-admin/groups/edit');
        $this->data['breadcrumb'] = $this->breadcrumbs->show();

        /* Variables */
		$support = $this->support_model->get_category($id);

		/* Validate form input */
        $this->form_validation->set_rules('support_name', $this->lang->line('edit_group_validation_name_label'), 'required|alpha_dash');

		if (isset($_POST) && !empty($_POST))
		{
            if ($this->form_validation->run() == TRUE)
			{
                $data = array(
                    'id' => $id,
                    'name' => $this->input->post('support_name'), 
                    'description' => $this->input->post('support_description')
                );
                
				$support_update = $this->support_model->update_support($id, $data);

				if ($support_update)
				{
					$this->session->set_flashdata('message', $this->lang->line('edit_group_saved'));
				}
				else
				{
					$this->session->set_flashdata('message', $this->ion_auth->errors());
				}

				redirect('system-admin/support', 'refresh');
			}
		}

        $this->data['message'] = (validation_errors() ? validation_errors() : ($this->ion_auth->errors() ? $this->ion_auth->errors() : $this->session->flashdata('message')));
        $this->data['group']   = $support;


		$readonly = 'miami' === $support->name ? 'readonly' : '';

		$this->data['support_name'] = array(
			'type'    => 'text',
			'name'    => 'support_name',
			'id'      => 'support_name',
			'value'   => $this->form_validation->set_value('support_name', $support->name),
            'class'   => 'form-control',
			$readonly => $readonly
		);
		$this->data['support_description'] = array(
			'type'  => 'text',
			'name'  => 'support_description',
			'id'    => 'support_description',
			'value' => $this->form_validation->set_value('support_description', $support->description),
            'class' => 'form-control'
        );
	

        /* Load Template */
        $this->template->admin_render('system-admin/support/edit', $this->data);
	}
}




