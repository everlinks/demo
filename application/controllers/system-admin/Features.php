<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Features extends Admin_Controller {

    public function __construct()
    {
        parent::__construct();

        $this->lang->load('system-admin/features');

        $this->load->model('system-admin/Features_model');
     

        /* Title Page :: Common */
        $this->page_title->push(lang('menu_security_article'));
        $this->data['pagetitle'] = $this->page_title->show();

        /* Breadcrumbs :: Common */
        $this->breadcrumbs->unshift(1, lang('menu_security_support'), 'system-admin/features');
    }


	public function index()
	{
        if ( ! $this->ion_auth->logged_in() OR ! $this->ion_auth->is_admin())
        {
            redirect('auth/login', 'refresh');
        }
        else
        {
            /* Breadcrumbs */
            $this->data['breadcrumb'] = $this->breadcrumbs->show();

            $this->data['features'] = $this->Features_model->features();

    

            /* Load Template */
            $this->template->admin_render('system-admin/features/index', $this->data);
        }
    }

	public function create()
	{
		if ( ! $this->ion_auth->logged_in() OR ! $this->ion_auth->is_admin())
		{
			redirect('auth', 'refresh');
		}

        /* Breadcrumbs */
        $this->breadcrumbs->unshift(2, lang('menu_groups_create'), 'system-admin/features/create');
        $this->data['breadcrumb'] = $this->breadcrumbs->show();

		/* Validate form input */
		$this->form_validation->set_rules('features_name', 'lang:create_group_validation_name_label', 'required|alpha_dash');

		if ($this->form_validation->run() == TRUE)
		{
			$new_group_category = $this->Features_model->create_features($this->input->post('features_name'), $this->input->post('features_description'), $this->input->post('features_descript'),$this->input->post('features_category'),$this->input->post('features_answers'));
			if ($new_group_category)
			{
				$this->session->set_flashdata('message', $this->ion_auth->messages());
			
            }
            redirect('system-admin/features', 'refresh');
		}
		else
		{
            $this->data['message'] = (validation_errors() ? validation_errors() : ($this->ion_auth->errors() ? $this->ion_auth->errors() : $this->session->flashdata('message')));

			$this->data['features_name'] = array(
				'name'  => 'features_name',
				'id'    => 'features_name',
				'type'  => 'text',
                'class' => 'form-control',
				'value' => $this->form_validation->set_value('features_name')
            );
            

            $this->data['features_title'] = array(
				'name'  => 'features_title',
				'id'    => 'features_title',
				'type'  => 'text',
                'class' => 'form-control',
				'value' => $this->form_validation->set_value('features_title')
			);

			
            $this->data['features_options'] = array(
				'name'  => 'features_options',
				'id'    => 'features_options',
				'type'  => 'text',
                'class' => 'form-control',
				'value' => $this->form_validation->set_value('features_options')
			);
			$this->data['features_description'] = array(
				'name'  => 'features_description',
				'id'    => 'features_description',
				'type'  => 'text',
                'class' => 'form-control',
				'value' => $this->form_validation->set_value('features_description')
            );
            
            $this->data['features_category'] = array(
				'name'  => 'features_category',
				'type'  => 'text',
				'id'    => 'features_category',
                'class' => 'form-control',
                'options'=> $this->Features_model->getCategory()
            );
            
            $this->data['features_answers'] = array(
				'name'  => 'features_answers',
				'id'    => 'features_answers',
				'type'  => 'text',
                'class' => 'form-control',
				'value' => $this->form_validation->set_value('features_answers')
			);

            /* Load Template */
            $this->template->admin_render('system-admin/features/create', $this->data);
		}
    }
    public function youControllerAction($list) {
   
            $list['category'] = $this->input->post('name');
       
  
            $res = $this->Features_model->insert_student($list);
  
            if($res)
            {
                header('location:'.base_url()."index.php/student/".$this->index());
            }
         }
    

    public function delete($id)
    {
        $this->load->model('Features_model');
        $this->Features_model->row_delete($id);

        redirect('system-admin/features', 'refresh');
    }

        
	public function edit($id)
	{
		if ( ! $this->ion_auth->logged_in() OR ! $this->ion_auth->is_admin() OR ! $id OR empty($id))
		{
			redirect('auth', 'refresh');
		}

        /* Breadcrumbs */
        $this->breadcrumbs->unshift(2, lang('menu_groups_edit'), 'system-admin/features/edit');
        $this->data['breadcrumb'] = $this->breadcrumbs->show();

        /* Variables */
		$features = $this->Features_model->edit_category($id);

		/* Validate form input */
        $this->form_validation->set_rules('features_options', $this->lang->line('edit_group_validation_name_label'), 'required|alpha_dash');

		if (isset($_POST) && !empty($_POST))
		{
            if ($this->form_validation->run() == TRUE)
			{
                $data = array(
					'id' => $id,
					'title' => $this->input->post('features_name'),
                    'options' => $this->input->post('features_options')
                    
                );
                
				$features_update = $this->Features_model->update_features($id, $data);

				if ($features_update)
				{
					$this->session->set_flashdata('message', $this->lang->line('edit_group_saved'));
				}
				else
				{
					$this->session->set_flashdata('message', $this->ion_auth->errors());
				}

				redirect('system-admin/features', 'refresh');
			}
		}

        $this->data['message'] = (validation_errors() ? validation_errors() : ($this->ion_auth->errors() ? $this->ion_auth->errors() : $this->session->flashdata('message')));
        $this->data['group']   = $features;


		$readonly = 'miami' === $features->title ? 'readonly' : '';


		$this->data['features_name'] = array(
			'type'  => 'text',
			'name'  => 'features_name',
			'id'    => 'features_name',
			'value' => $this->form_validation->set_value('features_name', $features->title),
            'class' => 'form-control'
        );
		$this->data['features_options'] = array(
			'type'    => 'text',
			'name'    => 'features_options',
			'id'      => 'features_options',
			'value'   => $this->form_validation->set_value('features_options', $features->options),
            'class'   => 'form-control',
			$readonly => $readonly
		);
	
	

        /* Load Template */
        $this->template->admin_render('system-admin/features/edit', $this->data);
	}
}

     
    