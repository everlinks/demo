//Once add button is clicked
$('.add_button').click(function() {
    
    var store = $("#store").val();

    $.ajax({
        type: 'POST',
        dataType: 'json',
        url: store + "storelink",
        success: function(data) {
            if (data['status']) {
              $('.link-unavailabe').remove();
              $('#counter').html(function(i, val) { return +val+1 });
              var id = data['id'];
              var fieldHTML = 
                  '<div class="card flex-row" id="collapse-'+id+'">'+
                      '<div class="card-header border-0 dragger"><i class="fas fa-ellipsis-v"></i></div>'+
                      '<div class="card-block p-1 form-section">'+
                          '<input type="hidden" name="id" value="'+id+'">'+
                          '<div class="d-flex pb-1 justify-content-between align-items-center"><input type="text" name="title" class="title" placeholder="Title" value="">'+
                              '<div class="btn-toggle"><input type="checkbox" name="active" id="active'+id+'"><label class="oh-8" '+'for="active'+id+'">Toggle</label></div>'+
                          '</div>'+
                          '<div class="d-flex justify-content-between align-items-center"><input type="text" id="url" name="url" '+'class="url" placeholder="http://url" value=""></div>'+
                          '<ul class="nav nav-pills navtop d-flex justify-content-end">'+
                              '<li class="nav-item">'+
                                  '<button class="btn btn-link collapsed" data-toggle="collapse" data-target="#menu1-'+id+'" '+'aria-expanded="true">'+
                                      '<i class="fas fa-images" aria-hidden="true"></i>'+
                                  '</button>'+
                              '</li>'+
                              '<li class="nav-item">'+
                                  '<button class="btn btn-link collapsed" data-toggle="collapse" data-target="#menu2-'+id+'" '+'aria-expanded="true">'+
                                      '<i class="fas fa-star" aria-hidden="true"></i>'+
                                  '</button>'+
                              '</li>'+
                              '<li class="nav-item">'+
                                  '<button class="btn btn-link collapsed" data-toggle="collapse" data-target="#menu3-'+id+'" '+'aria-expanded="true">'+
                                      '<i class="far fa-clock" aria-hidden="true"></i>'+
                                  '</button>'+
                              '</li>'+
                              '<li class="nav-item">'+
                                  '<button class="btn btn-link collapsed" data-toggle="collapse" data-target="#menu4-'+id+'" '+'aria-expanded="true">'+
                                      '<i class="fas fa-chart-line" aria-hidden="true"></i>'+
                                  '</button>'+
                              '</li>'+
                              '<li class="nav-item">'+
                                  '<button class="btn btn-link collapsed" data-toggle="collapse" data-target="#menu5-'+id+'" '+'aria-expanded="true">'+
                                      '<i class="far fa-trash-alt" aria-hidden="true"></i>'+
                                  '</button>'+
                              '</li>'+
                          '</ul>'+
                          '<div class="tab-content text-center">'+
                              '<div id="menu1-'+id+'" class="collapse" data-parent="#collapse-'+id+'" class="panel-collapse collapse">'+
                                  '<div class="box-part text-center"><i class="fas fa-images fa-2x" aria-hidden="true"></i>'+
                                      '<div class="title"><h5>Thumbnail</h5>'+
                                      '</div>'+
                                      '<div class="text">'+
                                          '<span>Lorem ipsum dolor sit amet, id quo eruditi eloquentiam. Assum decore te sed. Elitr '+'scripta ocurreret qui ad.</span>'+
                                      '</div>'+
                                      '<div class="action">'+
                                          '<a href="javascript:void(0)" class="btn btn-green oh-8 text-white">Get Pro</a>'+
                                      '</div>'+
                                  '</div>'+
                              '</div>'+
                              '<div id="menu2-'+id+'" class="collapse" data-parent="#collapse-'+id+'" class="panel-collapse collapse">'+
                                  '<div class="box-part text-center"><i class="fas fa-star fa-2x" aria-hidden="true"></i>'+
                                      '<div class="title"><h5>Priority Link</h5>'+
                                      '</div>'+
                                      '<div class="text">'+
                                          '<span>Lorem ipsum dolor sit amet, id quo eruditi eloquentiam. Assum decore te sed. Elitr '+'scripta ocurreret qui ad.</span>'+
                                      '</div>'+
                                      '<div class="action">'+
                                          '<a href="javascript:void(0)" class="btn btn-green oh-8 text-white">Get Pro</a>'+
                                      '</div>'+
                                  '</div>'+
                              '</div>'+
                              '<div id="menu3-'+id+'" class="collapse" data-parent="#collapse-'+id+'" class="panel-collapse collapse">'+
                                  '<div class="box-part text-center"><i class="far fa-clock fa-2x" aria-hidden="true"></i>'+
                                      '<div class="title"><h5>Schedule Link</h5>'+
                                      '</div>'+
                                      '<div class="text">'+
                                          '<span>Lorem ipsum dolor sit amet, id quo eruditi eloquentiam. Assum decore te sed. Elitr '+'scripta ocurreret qui ad.</span>'+
                                      '</div>'+
                                      '<div class="action">'+
                                          '<a href="javascript:void(0)" class="btn btn-green oh-8 text-white">Get Pro</a>'+
                                      '</div>'+
                                  '</div>'+
                              '</div>'+
                              '<div id="menu4-'+id+'" class="collapse" data-parent="#collapse-'+id+'" class="panel-collapse collapse">'+
                                  '<div class="box-part text-center"><i class="fas fa-chart-line fa-2x" aria-hidden="true"></i>'+
                                      '<div class="title"><h5>Hits Counter</h5>'+
                                      '</div>'+
                                      '<div class="text">'+
                                          '<span>Lorem ipsum dolor sit amet, id quo eruditi eloquentiam. Assum decore te sed. Elitr '+'scripta ocurreret qui ad.</span>'+
                                      '</div>'+
                                      '<div class="action">'+
                                          '<a href="javascript:void(0)" class="btn btn-green oh-8 text-white">Get Pro</a>'+
                                      '</div>'+
                                  '</div>'+
                              '</div>'+
                              '<div id="menu5-'+id+'" class="collapse" data-parent="#collapse-'+id+'" class="panel-collapse collapse">'+
                                  '<div class="box-part text-center"><i class="far fa-trash-alt fa-2x" aria-hidden="true"></i>'+
                                      '<div class="title"><h5>Delete</h5>'+
                                      '</div>'+
                                      '<div class="text">'+
                                          '<span>Are you sure you want to delete this link?</span>'+
                                          '<span>This action cannot be undone.</span>'+
                                      '</div>'+
                                      '<div class="action">'+
                                          '<a href="javascript:void(0)" class="btn" data-toggle="collapse" data-target="#menu5-'+id+'">Cancel</a>'+
                                          '<a href="javascript:void(0)" class="btn btn-danger link-remove oh-8 text-white">Delete</a>'+
                                      '</div>'+
                                  '</div>'+
                              '</div>'+
                          '</div>'+
                      '</div>'+
                  '</div>';
              $('#sortable').prepend(fieldHTML);
            }
          },
        error: function(data){
           console.log(data)
        }
    });
});


$('body').on('change', 'input', function() {
    var store = $('#store').val();
    var id = $(this).parents('.form-section').find('input[name="id"]').val();
    var type = $(this).attr('name');
    if (type === 'active') {
        var value = +$(this).is(':checked');
    } else if (type === 'url') {
        var value = $(this).val();
        var regex = /^(http[s]?:\/\/){0,1}(www\.){0,1}[a-zA-Z0-9\.\-]+\.[a-zA-Z]{2,5}[\.]{0,1}/;
        if (!regex.test(value)) {
            $(this).css("color", "red");
            return false;
        } else {
            $(this).css("color", "");
        }
    } else {
        var value = $(this).val();
    }

    var dataString = 'id=' + id + '&type=' + type + '&value=' + value;

    $.ajax({
        type: 'POST',
        dataType: 'json',
        data: dataString,
        url: store + "storedata",
        success: function(data) {
            console.log(data);
            previewUpdate();
        },
        error: function(data){
           console.log(data)
        }
    });
});

// List with handle
Sortable.create(sortable, {
    handle: '.dragger',
    animation: 150,
    // onAdd: function (evt){ console.log('onAdd.bar:', evt.item); },
    // onUpdate: function (evt){ console.log('onUpdate.bar:', evt.item); },
    // onRemove: function (evt){ console.log('onRemove.bar:', evt.item); },
    // onStart:function(evt){ console.log('onStart.foo:', evt.item);},
    // onEnd: function(evt){ 
    //   var to = evt.to;    // target list
    //   var from = evt.from;  // previous list
    //   var oldIndex = evt.oldIndex;  // element's old index within old parent
    //   var newIndex = evt.newIndex;  // element's new index within new parent
    //   console.log('onEnd.foo:', to, from, oldIndex, newIndex);
    // },
    filter: '.link-remove',
    onFilter: function(evt) {
        var store = $("#store").val();
        var id = $(evt.item).find('input[name="id"]').val();
        $.ajax({
          type: 'POST',
          dataType: 'json',
          data: {id: id},
          url: store + "removelink",
          success: function(data) {
              evt.item.parentNode.removeChild(evt.item);
              previewUpdate();
          },
          error: function(data){
             console.log(data)
          }
      });
    },
    onUpdate: function(evt) {
      var store = $("#store").val();
      var values = $('input[name="id"]').map(function(){
                return {
                  'id' : $(this).val()
                };
              }).get();
      $.ajax({
          type: 'POST',
          dataType: 'json',
          data: {position: values},
          url: store + "updateposition",
          success: function(data) {
              previewUpdate();
          },
          error: function(data){
             console.log(data)
          }
      });
    }
});

new PerfectScrollbar('.link-section');